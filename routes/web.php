<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('a/{personnel_no}/', 'Auth\LoginController@programaticallyEmployeeLogin')->name('login.a');
    

Route::get('/config-cache', function() {
    Artisan::call('config:cache');
    return "Cache is cleared";
});
Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    return "Cache is cleared";
});
Route::get('/', function () {
    if (!Auth::user()) {
        // untuk virutal host
        // return redirect()->route('login');
        return redirect()->away('https://sso.krakatausteel.com');

        // jika tidak menggukalan virtual host
        // return redirect()->guest('/public/login');
    } else {
        
        return redirect()->route('dashboard');
    }
});



Route::group(['middleware' => ['auth','role:administrator']], function () {
    Route::resource('user', 'UserController')->parameters(['user' => 'id']);
});

Route::group(['middleware' => ['auth','role:administrator|admin']], function () {
    Route::resource('materi', 'MateriRakoorController')->parameters(['materi' => 'id']);
    
    Route::get('partisipan/user', 'PartisipanController@user')->name('partisipan.user');
    Route::get('ambil_unit/performance', 'UnitController@ambil_unit');
    Route::resource('partisipan', 'PartisipanController')->parameters(['partisipan' => 'id']);
    
    Route::get('notulis/user', 'NotulisController@user')->name('notulis.user');
    Route::resource('notulis', 'NotulisController')->parameters(['notulis' => 'id']);
});

Route::group(['middleware' => ['auth','role:notulis|administrator|admin']], function () {
    Route::get('notulen/user/{q?}', 'NotulenController@user')->name('notulen.user');
    Route::resource('notulen', 'NotulenController')->parameters(['notulen' => 'id']);
});

Route::group(['middleware' => ['auth']], function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('/unit', 'UnitController');

    // master kpi
    // Route::resource('/kpi', 'KpiController');
    Route::get('/dashboard/{kode?}/{nama?}/{tahun?}/{bulan?}', 'HomeController@index');
    Route::get('/kpi', 'KpiController@index')
        ->name('kpi.index');
    Route::get('import/kpi', 'KpiController@formImport')
        ->name('kpi.import');
    Route::get('kpi/{id}', 'KpiController@edit')
        ->name('kpi.edit');
    Route::put('kpi/{id}', 'KpiController@update')
        ->name('kpi.update');
    Route::post('/import/kpi/process','KpiController@importProcess')
        ->name('kpi.import.process');

    // master unit
    Route::resource('/unit', 'UnitController');
    Route::get('pic/unit/{id}', 'UnitController@formImport');
    Route::post('pic/unit/process', 'UnitController@importProcess')
        ->name('unit.pic.process');

    // deployment
    // Route::resource('/deployment', 'DeploymentController');
    Route::get('/deployment', 'DeploymentController@index')
        ->name('deployment.index');
    Route::get('import/deployment', 'DeploymentController@formImport')
        ->name('deployment.import');
    Route::get('/deployment/edit/{id}', 'DeploymentController@edit');
    Route::get('/deployment/delete/{id}', 'DeploymentController@remove');
    Route::post('/deployment/update/', 'DeploymentController@update');
    Route::get('/deployment/carikodeunit/', 'DeploymentController@carikodeunit');
    Route::post('import/deployment/process', 'DeploymentController@importProcess')
        ->name('deployment.import.process');

    // deployment target
    Route::get('target/deployment', 'DeploymentTransactionController@index')
        ->name('deployment.target.index');
    Route::get('target/{id}/deployments', 'DeploymentTransactionController@create')
        ->name('deployment.target.create');
	Route::get('validasitarget/{id}/deployments', 'DeploymentTransactionController@validasitarget')
        ->name('deployment.target.validasitarget');
    Route::post('target/deployment','DeploymentTransactionController@store')
        ->name('deployment.store');
    Route::get('target/deployment/{id}/show','DeploymentTransactionController@show')
        ->name('deployment.target.show');
    Route::get('target/deployment/{id}/validasi/admin','DeploymentTransactionController@formValidasiAdmin')
        ->name('deployment.target.formvalidasiadmin');
    Route::get('deployments/unvalid/{id}','DeploymentTransactionController@Unvalidasi');
    Route::put('target/deployment/{id}/validasi/admin','DeploymentTransactionController@update')
        ->name('deployment.target.validasiadmin');
    Route::get('target/deployment/{id}/validasi/atasan','DeploymentTransactionController@formValidasiAtasan')
        ->name('deployment.target.formvalidasiatasan');
    Route::get('target/deployment/allvalidasitarget','DeploymentTransactionController@allvalidasitarget');
    Route::get('target/deployment/onevalidasitarget','DeploymentTransactionController@onevalidasitarget');
    Route::put('target/deployment/{id}/validasi/atasan','DeploymentTransactionController@validasiAtasan')
        ->name('deployment.target.validasiatasan');

    // deployment realization
    Route::get('realisasi/deployment','DeploymentRealizationController@index')
        ->name('deployment.realisasi.index');  
    Route::get('realisasi/hapusfile/','DeploymentRealizationController@hapusfile');
    Route::get('realisasi/deployment/{id}/show','DeploymentRealizationController@show')
        ->name('deployment.realisasi.show');  
	Route::post('realisasi/deployment','DeploymentRealizationController@store')
        ->name('deployment.realisasi.store');
	Route::post('realisasi/insrealisasi','DeploymentRealizationController@insrealisasi');
	Route::post('realisasi/insalasannya','DeploymentRealizationController@insalasan');
    Route::get('realisasi/validasi','DeploymentRealizationController@validasirealisasi');
    Route::get('realisasi/kembalikan','DeploymentRealizationController@kembalikanrealisasi');
    Route::get('realisasi/deployment/{id}/validasi','DeploymentRealizationController@formValidasi')
        ->name('deployment.realisasi.formvalidasi');
    Route::put('realisasi/deployment/{id}/validasi','DeploymentRealizationController@processValidasi')
        ->name('deployment.realisasi.validasi');
    Route::get('realisasi/validasiperbulan','DeploymentRealizationController@validasiperbulan');
     // deployment capaian
     Route::get('capaian/deployment','CapaianController@index')
     ->name('deployment.capaian.index');  
     Route::get('capaian/deployment/atasan','CapaianController@indexatasan');
     Route::get('capaian/deployment/laporancapaian','CapaianController@laporancapaian');
     Route::get('capaian/deployment/laporancapaianadmin/{kode?}/{nama?}/{tahun?}','CapaianController@laporancapaianadmin');
     Route::get('capaian/deployment/laporan/{kode?}/{tahun?}','CapaianController@laporan');
     Route::get('capaian/deployment/validasi/{kode?}/{tahun?}','CapaianController@laporan_validasi');
     Route::get('capaian/deployment/view_laporan/{kode?}/{tahun?}','CapaianController@view_laporan');
     Route::get('capaian/deployment/view_laporan_acc/{kode?}/{tahun?}','CapaianController@view_laporan_acc');
     
});


Route::get('/debug', 'DebugController@index');
Auth::routes();