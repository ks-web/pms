<?php

namespace App\Imports;

use App\Kpi;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class KpisImport implements ToModel, WithStartRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        
            return new Kpi([
                'kode_kpi'              => $row[0],
                'kpi'                   => $row[1],
                'satuan'                => $row[3], 
                'rumus_capaian'         => $row[4],
                'rumus_akumulasi'       => $row[5],
                'deskripsi'             => $row[2],
                'keterangan'            => $row[2]
                
                
            ]);
            
    }

     /**
     * @return int
     */
    public function startRow(): int
    {
        return 2;
    }
}
