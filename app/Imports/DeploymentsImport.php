<?php

namespace App\Imports;

use App\Deployment;
use App\Kpi;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class DeploymentsImport implements ToModel, WithStartRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $cek=Deployment::where('kode_unit',$row[0])->where('kode_kpi',$row[1])->where('tahun',$row[4])->count();
        $kpii=Kpi::where('kode_kpi',$row[1])->first();

        if($cek>0){
            $deploy                     =   Deployment::where('kode_unit',$row[0])->where('kode_kpi',$row[1])->where('tahun',$row[4])->first();
            $deploy->target_tahunan     = $row[2];
            $deploy->bobot_tahunan      = $row[3];
            $deploy->rumus_capaian      = $kpii['rumus_capaian'];
            $deploy->rumus_akumulasi    = $kpii['rumus_akumulasi'];
            $deploy->kode_unit_tingkat  = $row[5];
            $deploy->save();
        }else{
           
            return new Deployment([
                'kode_unit'         => $row[0],
                'kode_kpi'          => $row[1],
                'target_tahunan'    => $row[2],
                'bobot_tahunan'     => $row[3],
                'tahun'             => $row[4],
                'rumus_akumulasi'   => $kpii->rumus_akumulasi,
                'rumus_capaian'     => $kpii->rumus_capaian,
                'id_kpi_unix'       => $row[1].$row[4],
                'kode_unit_tingkat' => $row[5],
                'level' => $row[6],
                'sts' => $row[7],
            ]);
        }
        
    }

    /**
     * @return int
     */
    public function startRow(): int
    {
        return 2;
    }
}
