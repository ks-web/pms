<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    protected $table = 'unit_kerja';
    public $timestamps = false;

    public function deployments() 
    {
        return $this->hasMany('App\Deployments', 'kode', 'kode_unit');
    }
    public function deploymentdetail() 
    {
        return $this->hasMany('App\DeploymentTransaction', 'kode', 'kode_unit');
    }
    public function deploymentatasan() 
    {
        return $this->hasMany('App\Deployments', 'kode', 'kode_unit_tingkat');
    }
    public function user()
    {
        return $this->belongsTo('App\User','nik_atasan','username');
    }
}
