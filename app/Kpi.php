<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kpi extends Model
{
    protected $fillable = ['kode_kpi','kpi','satuan','rumus_capaian','rumus_akumulasi','max_capaian','tahun','target','kode_unix','deskripsi'];

    public function deployments() {
        return $this->hasMany('App\Deployment', 'kode_kpi', 'kode_kpi');
    }

    public function rumuscapaian() {
        return $this->belongsTo('App\Rumuscapaian', 'rumus_capaian', 'id');
    }

    public function rumusakumulasi() {
        return $this->belongsTo('App\Accumulation', 'rumus_akumulasi', 'id');
    }
}
