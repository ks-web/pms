<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeploymentTransaction extends Model
{
    protected $fillable = ['id_deployment','tahun','bulan','target','selisih','realisasi','kode_unit','sts'];
    protected $table = 'deployment_targets';

    public function deployment() 
    {
        return $this->belongsTo('App\Deployment', 'id_deployment', 'id');
    }

    public function status()
    {
        return $this->belongsTo('App\Status');
    }

    public function unit()
    {
        return $this->belongsTo('App\Unit','kode_unit','kode');
    }
}
