<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    protected $table = 'level';
    public $timestamps = false;

    public function deployment() 
    {
        return $this->hasMany('App\Deployment', 'id', 'level');
    }
}
