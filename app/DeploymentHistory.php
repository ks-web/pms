<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeploymentHistory extends Model
{
    protected $table = 'deployment_history';
    public $timestamps = true;
    protected $fillable = ['kode_unit','kode_kpi','target_tahunan','bobot_tahunan','tahun','rumus_akumulasi','rumus_capaian','id_kpi_unix','kode_unit_tingkat'];
    
    public function kpi()
    {
        return $this->belongsTo('App\Kpi', 'kode_kpi', 'kode_kpi');
    }

    public function status()
    {
        return $this->belongsTo('App\Status');
    }

    public function deploymentTargets()
    {
        return $this->hasMany('App\DeploymentTransaction', 'id_deployment', 'id');
    }

    public function unit()
    {
        return $this->belongsTo('App\Unit','kode_unit','kode');
    }

    public function unitatasan()
    {
        return $this->belongsTo('App\Unit','kode_unit_tingkat','kode');
    }

    public function rumuscapaian() {
        return $this->belongsTo('App\Rumuscapaian', 'rumus_capaian', 'id');
    }

    public function rumusakumulasi() {
        return $this->belongsTo('App\Accumulation', 'rumus_akumulasi', 'id');
    }
}
