<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProgresRealisasi extends Model
{
    Protected $table = 'progres_realisasi';
    public $timestamps = false;
}
