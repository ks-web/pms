<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeploymentRealization extends Model
{
    public function deployment() 
    {
        return $this->belongsTo('App\Deployment', 'id_deployment', 'id');
    }

}
