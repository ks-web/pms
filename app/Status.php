<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $fillable = ['name'];

    public function deployments() 
    {
        return $this->hasMany('App\Deployments');
    }

    public function deploymentTargets() 
    {
        return $this->hasMany('App\DeploymentTransaction');
    }
}
