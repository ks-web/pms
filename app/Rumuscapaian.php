<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rumuscapaian extends Model
{
    protected $table = 'rumus_capaian';
    public $timestamps = false;

    public function kpi() 
    {
        return $this->hasMany('App\Kpi', 'id', 'rumus_capaian');
    }

    public function deployment() 
    {
        return $this->hasMany('App\Deployment', 'id', 'rumus_capaian');
    }
}
