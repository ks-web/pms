<?php

function unit(){
    $data =App\Unit::orderBy('nama','DESC')->where('pimpinan','!=','none')->where('unit_id','!=',4)->where('unit_id','!=',6)->get();
    return $data;
 }
function cek_unit($kode){
    $data =App\Unit::where('kode',$kode)->first();
    return $data;
 }
function deskripsi($kode){
    $data =App\Kpi::where('kode_kpi',$kode)->first();
    return $data['deskripsi'];
 }

 function progres_target($id){
    $data=App\DeploymentTransaction::where('id_deployment', $id)->where('target','!=',0)->max('id');
    $datta=App\DeploymentTransaction::where('id', $data)->first();
    return $datta['target'];
 }
 function progres_realisasi($id){
    $data=App\DeploymentTransaction::where('id_deployment', $id)->where('realisasi','!=',0)->max('id');
    $datta=App\DeploymentTransaction::where('id', $data)->first();
    return $datta['realisasi'];
 }
 function Tglvalidasi($kode,$bulan,$tahun){
    $cek=App\DeploymentTransaction::where('kode_unit', $kode)->where('bulan',$bulan)->where('tahun',$tahun)->whereNotNull('tgl_validasi_atasan')->count();
    $data=App\DeploymentTransaction::where('kode_unit', $kode)->where('bulan',$bulan)->where('tahun',$tahun)->whereNotNull('tgl_validasi_atasan')->first();
    if($cek>0){
        return date('d/m/y',strtotime($data['tgl_validasi_atasan'])); 
    }else{
        return $data['tgl_validasi_atasan']; 
    }
    
    return $cek;
}
 function view_Tglvalidasi($kode,$bulan,$tahun){
    $cek=App\DeploymentTransaction::where('kode_unit', $kode)->where('bulan',$bulan)->where('tahun',$tahun)->whereNotNull('tgl_validasi_atasan')->count();
    $data=App\DeploymentTransaction::where('kode_unit', $kode)->where('bulan',$bulan)->where('tahun',$tahun)->whereNotNull('tgl_validasi_atasan')->first();
    if($cek>0){
        return date('d/m/y',strtotime($data['tgl_validasi_atasan'])); 
    }else{
        return $data['tgl_validasi_atasan']; 
    }
    
    
}

function num_tgl($date){
    $d=explode('-',$date);
    if($date==''){
        $data=0;
    }else{
        $data=$d[0].$d[1].$d[2];
    }
    
   

    return $data;
}
function text_tgl($date){
    if($date==''){
        $data=0;
    }else{
        $data=date('d/m/y',strtotime($date));
    }
    
   

    return $data;
}

function target($capaian,$target){
    if($capaian==3){
        if($target==0){
            $data=0;
        }else{
            
            $data=date('d/m/y',strtotime($target));
        }
        
    }else{
        $data=$target;
    }

    return $data;
}

function rumus_capaian($capaian,$bobot,$target,$realisasi){

    if($capaian==3){
        if(is_null($target) || $target==0){
            $cap=100;
        }else{
            if($realisasi==0){
                $cap=0;
            }else{
                $tanggal1 = date_create(date('Y-m-d h:i:s',strtotime($realisasi)));
                $tanggal2 = date_create(date('Y-m-d h:i:s',strtotime($target)));
                $perbedaan = $tanggal1->diff($tanggal2);
                $cap=(1+($perbedaan->days/30))*100;
            }
            
        }
        
        if($cap>120){
            $nicap=120;
        }else{
            $nicap=$cap;
        }

        $nil= round($nicap);

    }elseif($capaian==4){
        if($target==0){
            $cap=100;
        }else{
            $expld=explode("-",$target);
            if($expld[0]>$realisasi){
                $cap=(1-(($expld[0]-$realisasi)/$expld[0]))*100;
            }else{
                $cap=(1-(($realisasi-$expld[1])/$expld[1]))*100;
            }
        }
        
        if($cap>120){
            $nicap=120;
        }else{
            $nicap=$cap;
        }

        $nil= round($nicap);

    }elseif($capaian==1){
        if($target==0){
            $cap=100;
        }else{
            if($realisasi==0){
                $cap=0;
            }else{
                 $cap=(1-(($target-$realisasi)/$target))*100;
            }
        }
        
        if($cap>120){
            $nicap=120;
        }else{
            $nicap=$cap;
        }
        $nil= round($nicap);

    }else{
        if($target==0){
            $cap=100;
        }else{
            if($realisasi==0){
                $cap=0;
            }else{
                 $cap=(1+(($target-$realisasi)/$target))*100;
            }
        }
        
        if($cap>120){
            $nicap=120;
        }else{
            $nicap=$cap;
        }
        $nil= round($nicap);
    }
    
    if($nil<0){
        $nils=0;
    }else{
        $nils=$nil;
    }
    return $nils;
    
}
function tervalidasi($kode,$tahun,$bulan){
    $data =App\DeploymentTransaction::where('kode_unit',$kode)->where('tahun',$tahun)->where('bulan',$bulan)->where('target','!=',0)->count();
    return $data;
}
function cek_validasi($kode,$tahun,$bulan){
    $data =App\DeploymentTransaction::where('kode_unit',$kode)->where('tahun',$tahun)->where('bulan',$bulan)->where('realisasi','!=',0)->count();
    return $data;
}
function cek_tgl_validasi($kode,$tahun,$bulan){
    $data =App\DeploymentTransaction::where('kode_unit',$kode)->where('tahun',$tahun)->where('bulan',$bulan)->where('tgl_validasi_atasan','!=','')->count();
    return $data;
}
function total_kpi($kode,$tahun,$bulan){
    $data =App\DeploymentTransaction::where('kode_unit',$kode)->where('tahun',$tahun)->where('bulan',$bulan)->count();
    return $data;
 }
function tot_kpi($kode,$tahun,$bulan){
    $data =App\DeploymentTransaction::where('kode_unit',$kode)->where('tahun',$tahun)->where('bulan',$bulan)->count();
    return $data;
 }

function validation($kode,$tahun,$bulan){
    $data =App\DeploymentTransaction::where('kode_unit',$kode)->where('tahun',$tahun)->where('bulan',$bulan)->first();
    return $data['tgl_validasi_atasan'];
 }
function potongan($kode,$tahun,$bulan){
    $data =App\DeploymentTransaction::where('kode_unit',$kode)->where('tahun',$tahun)->where('bulan',$bulan)->first();
    $mulai=$data['tgl_validasi_atasan'];
    if($bulan>11){
        $bln=1;
        $thn=$tahun+1;
    }else{
        $bln=$bulan+1;
        $thn=$tahun;
    }
    $sampai=$thn.'-'.$bln.'-05';
    $tanggal1 = date_create(date('Y-m-d h:i:s',strtotime($mulai)));
    $tanggal2 = date_create(date('Y-m-d h:i:s',strtotime($sampai)));
    $perbedaan = $tanggal1->diff($tanggal2);
    if(num_tgl($sampai)-num_tgl($mulai)>1){
        $potongannya=0;
    }else{
        $potongannya=$perbedaan->days;
    }
    if($potongannya*0.05>50){
        $poto=50;
    }else{
        $poto=$potongannya*0.05;
    }
    return $poto;
 }

 function hasil_capaian($capaian,$kpi){
     if($kpi>0){
        $data=round($capaian/$kpi);
       
     }else{
        $data=0;
     }
     return $data;
 }

 function selisih($date,$bulan){
    $mulai=$date;
    $sampai=$tgll[0].'-'.$bul.'-05';
    $tanggal1 = date_create(date('Y-m-d h:i:s',strtotime($mulai)));
    $tanggal2 = date_create(date('Y-m-d h:i:s',strtotime($sampai)));
    $perbedaan = $tanggal1->diff($tanggal2);
    if(num_tgl($sampai)-num_tgl($mulai)>1){
        $potongannya=0;
    }else{
        $potongannya=$perbedaan->days;
    }
 }


