<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kpi;
use App\Imports\KpisImport;
use Maatwebsite\Excel\Facades\Excel;

class KpiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kpis = Kpi::all();
        return view('kpis.index', compact('kpis'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kpi = Kpi::find($id);
        return view('kpis.edit', compact('kpi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $kpi = Kpi::where('id',$id)->update([
                'kpi' => $request->input('kpi'),
                'deskripsi' => $request->input('deskripsi'),
                'periode' => $request->input('periode'),
                'satuan' => $request->input('satuan'),
                'kode_catalog' => $request->input('kode_catalog')
            ]);
        } catch(Exception $ex) {
            return redirect()
                ->route('kpi.index')
                ->with('success','Error'.$ex);
        }
        
        return redirect()
            ->route('kpi.index')
            ->with('success', 'Kpi berhasil diedit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function formImport()
    {
        return view('kpis.create');
    }

    public function importProcess(Request $request)
    {
        $delete=Kpi::truncate();
		// menangkap file excel
        $file = $request->file('files');
        
        //dd($file);
 
		// membuat nama file unik
        $nama_file = rand().$file->getClientOriginalName();
        
 
		// upload ke folder file_siswa di dalam folder public
		$file->move('file_import',$nama_file);
 
		// import data
		Excel::import(new KpisImport, public_path('/file_import/'.$nama_file));
        return redirect()
            ->route('kpi.index')
            ->with('success', 'Data berhasil di upload.');
    }
}
