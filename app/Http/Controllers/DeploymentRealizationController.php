<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Deployment;
use App\ProgresRealisasi;
use App\User;
use App\Unit;
use App\Accumulation;
use App\DeploymentTransaction as DeploymentTarget;
use App\DeploymentRealization;

class DeploymentRealizationController extends Controller
{
    /**
     * menampilkan data deployment realization
     */	
	 public function index(Request $request)
    {
        // control proses untuk keypersone
        
        
        if(Auth::user()->hasRole('keyperson'))
        {
            $units = Unit::where('nik', Auth::user()->username)->get();
            $unitakhir = Unit::where('nik', Auth::user()->username)->take(1)->first();
            
                if($request->input('unit') == 'all' || $request->input('unit') == null)
                {
                    $deployments = Deployment::where('kode_unit', $request->input('unit'))
                        ->where('status_id',4)
                        ->get();
                }
                else 
                {
                    if($request->input('unit') == null && $request->input('tahun') == null)
                    {
                        $deployments = Deployment::where('kode_unit', $request->input('unit'))
                            ->where('status_id',4)
                            ->get();
                    }
                    else if($request->input('unit') != null && $request->input('tahun') == null)     
                    {
                        $deployments = Deployment::where('status_id',4)
                            ->where('kode_unit', $request->input('unit'))->get();
                    }  
                    else if($request->input('unit') == null && $request->input('tahun') != null)     
                    {
                        $deployments = Deployment::where('status_id',4)
                            ->where('tahun', $request->input('tahun'))->get();
                    }   
                    else if($request->input('unit') != null && $request->input('tahun') != null)
                    {
                        $deployments = Deployment::where('status_id',4)
                            ->where('kode_unit', $request->input('unit'))
                            ->where('tahun', $request->input('tahun'))
                            ->get();
                    }        
                }
        }

        if( Auth::user()->hasRole('boss'))
        {
            $units = Unit::where('nik_atasan', Auth::user()->username)->get();
            $unitakhir = Unit::where('nik_atasan', Auth::user()->username)->first();
            
                // if($request->input('tahun') == null)
                // {
                    
                //     $deployments = Deployment::where('kode_unit', $unitakhir->kode)
                //         ->where('status_id',4)
                //         ->get();
                // }
                // else 
                // {
                //     $deployments = Deployment::where('kode_unit', $request->input('unit'))
                //         ->where('status_id',4)->where('tahun', $request->input('tahun'))
                //         ->get();
                // }
            
            

                if($request->input('unit') == 'all' || $request->input('unit') == null)
                {
                    $deployments = Deployment::where('kode_unit', $request->input('unit'))
                        ->where('status_id',4)
                        ->get();
                }
                else 
                {
                    if($request->input('unit') == null && $request->input('tahun') == null)
                    {
                        $deployments = Deployment::where('kode_unit', $request->input('unit'))
                            ->where('status_id',4)
                            ->get();
                    }
                    else if($request->input('unit') != null && $request->input('tahun') == null)     
                    {
                        $deployments = Deployment::where('status_id',4)
                            ->where('kode_unit', $request->input('unit'))->get();
                    }  
                    else if($request->input('unit') == null && $request->input('tahun') != null)     
                    {
                        $deployments = Deployment::where('status_id',4)
                            ->where('tahun', $request->input('tahun'))->get();
                    }   
                    else if($request->input('unit') != null && $request->input('tahun') != null)
                    {
                        $deployments = Deployment::where('status_id',4)
                            ->where('kode_unit', $request->input('unit'))
                            ->where('tahun', $request->input('tahun'))
                            ->get();
                    }        
                }
        }



        // control proses untuk selain keypersone

        if(Auth::user()->hasRole('administrator'))
        {
            $units = Unit::where('pimpinan','!=','none')->where('unit_id','!=',4)->where('unit_id','!=',6)->orderBy('nama','Asc')->get();
            // if user administrator
            if($request->input('unit') == 'all' || $request->input('unit') == null)
            {
                $deployments = Deployment::where('status_id',4)
                    ->get();
            }
            else 
            {
                if($request->input('unit') == null && $request->input('tahun') == null)
                {
                    $deployments = Deployment::where('status_id',4)
                        ->get();
                }
                else if($request->input('unit') != null && $request->input('tahun') == null)     
                {
                    $deployments = Deployment::where('status_id',4)
                        ->where('kode_unit', $request->input('unit'));
                }  
                else if($request->input('unit') == null && $request->input('tahun') != null)     
                {
                    $deployments = Deployment::where('status_id',4)
                        ->where('tahun', $request->input('tahun'));
                }   
                else if($request->input('unit') != null && $request->input('tahun') != null)
                {
                    $deployments = Deployment::where('status_id',4)
                        ->where('kode_unit', $request->input('unit'))
                        ->where('tahun', $request->input('tahun'))
                        ->get();
                }        
            }
        }
            $tampilunit = Unit::where('kode', $request->input('unit'))->first();
            $namaunit=$tampilunit['nama'];
            return view('deployment_realizations.index', compact('namaunit','deployments','units'));
    }

    public function show($id)
    {
        try {
            // return view to file views/deployement_realisasi/validasi-realisasi.blade.php
            // getdata deployment
            $deployment = Deployment::find($id);
            
            // get data deployments target
            $targetdeployment = DeploymentTarget::where('id_deployment', $id)
                ->get();

            // jumlah target
            $jumlah = $targetdeployment->count();

            // set to variabel view
            $view   = view('deployment_realizations._detail', compact('deployment','targetdeployment','jumlah'))->render();

            // response json
            return response()->json(['html' => $view]);
        }catch(Exception $ex) {
            return redirect()
                ->route('deployment.realisasi.index')
                ->with('success','Error'.$ex);
        }
    }
	
	public function store(Request $request)
    {
        try {
            // update deployment
            $deploy = Deployment::where('id',$request->input('id_deployment'))->update(
                [
                    // 'akumulasi' => $request->input('totalbulanan'),
                    // 'accumulations_id' => $request->input('accumulations_id'),
                    'status_id' => $request->input('status_id'),
                ]
            );

            // update deployment target
            for($i=1; $i<=12; $i++)
            {
                $cekrealisasi=DeploymentTarget::where('id',$request->input('id_targetok'.$i))->first();
                if(is_null($cekrealisasi->tgl_realisasi)){
                    $tanggal=date('Y-m-d');
                }else{
                    $tanggal=$cekrealisasi->tgl_realisasi;
                }
                $flight = DeploymentTarget::where('id',$request->input('id_targetok'.$i))->update(
                    [
                        'realisasi' => $request->input('realisasi'.$i),
                        'tgl_realisasi' =>$tanggal,
                        'status_id' => '2',
                    ]
                );

                
            }
                // return redirect('/target/'.$request->input('id_deployment').'/deployments');
                return redirect()
                    ->route('deployment.realisasi.index')
                    ->with('success','Realisasi berhasil diinput.');
        }
		catch(Exception $ex) {
            return redirect()
                ->route('deployment.realisasi.index')
                ->with('success','Error'.$ex);
        }
    }
    
    public function insrealisasi(request $request){
        if (trim($request->realisasi) == '') {$error[] = '- Isi Nilai Realisasi';}
        if($request->selisih<95)
        {
            if (trim($request->masalah) == '') {$error[] = '- Isi Kolom masalah yang dihadapi';}
            if (trim($request->rencana) == '') {$error[] = '- Isi Kolom rencana yang dihadapi';}
        }else{
            
        }
        if (isset($error)) {echo '<b>Error</b>: <br />'.implode('<br />', $error);} 
        else {
                if($request->selisih>120)
                {
                    $sell=120;
                }else{
                    $sell=$request->selisih;
                }
                // echo $sell;
                if(is_null($request->editfile)){
                    if(is_null($request->file)){
                        echo'Upload file terlebih dahulu';
                    }else{
                        $type = $request->file->extension();
                        if($type=='pdf'){
                            $file = $request->file->store('images');

                            $real                   =DeploymentTarget::where('id',$request->id_target)->first();
                            $real->realisasi        =$request->realisasi;
                            $real->selisih          =$sell;
                            $real->tgl_target_penyelesaian       =$request->tgl_target_penyelesaian;
                            $real->masalah          =$request->masalah;
                            $real->rencana          =$request->rencana;
                            $real->progres          =$request->progres;
                            $real->file             =$file;
                            $real->tgl_realisasi    =date('Y-m-d');
                            $real->status_realisasi =1;
                            $real->save();

                            echo'ok';
                        }else{
                            echo'Format file harus Pdf';
                        }
                    }
                        
                }else{
                    $real                   =DeploymentTarget::where('id',$request->id_target)->first();
                    $real->realisasi        =$request->realisasi;
                    $real->selisih          =$sell;
                    $real->keterangan       =$request->keterangan;
                    $real->tgl_target_penyelesaian       =$request->tgl_target_penyelesaian;
                    $real->masalah          =$request->masalah;
                    $real->progres          =$request->progres;
                    $real->rencana          =$request->rencana;
                    $real->tgl_realisasi    =date('Y-m-d');
                    $real->status_realisasi =1;
                    $real->save();

                    echo'ok';
                }
                    
            
        }
        
        
    }

    public function insalasan(request $request){
        if (trim($request->progres) == '') {$error[] = '- Isi Kolom Progres';}
        if (isset($error)) {echo '<b>Error</b>: <br />'.implode('<br />', $error);} 
        else {
                
                    $real                       =DeploymentTarget::where('id',$request->id_target)->first();
                    $real->progres              =$request->progres;
                    $real->save();

                    echo 'ok';
              
        }
        
        
    }

    public function hapusfile(){
        $real               =DeploymentTarget::where('id',$_GET['id'])->first();
        $real->file         =null;
        $real->save();
    } 

    public function validasiperbulan(){
        $deploy                    =DeploymentTarget::where('bulan',$_GET['bulan'])->where('kode_unit',$_GET['kode'])->where('tahun',$_GET['tahun'])->where('status_realisasi',1)->get();
        
        foreach($deploy as $o){
            $real                    =DeploymentTarget::where('id',$o->id)->first();
            $real->status_realisasi  =2;
            $real->tgl_validasi_atasan  =date('Y-m-d');
            $real->save();
        }
        
        
    } 
    public function validasirealisasi(){
        $real                    =DeploymentTarget::where('id',$_GET['id'])->first();
        $real->status_realisasi  =2;
        $real->tgl_validasi_atasan  =date('Y-m-d');
        $real->save();
    } 

    public function kembalikanrealisasi(){
        $real                    =DeploymentTarget::where('id',$_GET['id'])->first();
        $real->status_realisasi  =null;
        $real->save();
    } 
    public function formValidasi($id) 
    {
        try {
            // return view to file views/deployement_realisasi/validasi-realisasi.blade.php
            // getdata deployment
            $deployment = Deployment::find($id);
            
            // get data deployments target
            $targetdeployment = DeploymentTarget::where('id_deployment', $id)
                ->get();

            // jumlah target
            $jumlah = $targetdeployment->count();

            // set to variabel view
            $view   = view('deployment_realizations.boss.validasi-realisasi', compact('deployment','targetdeployment','jumlah'))->render();

            // response json
            return response()->json(['html' => $view]);
        }catch(Exception $ex) {
            return redirect()
                ->route('deployment.realisasi.index')
                ->with('success','Error'.$ex);
        }
    }

    public function processValidasi(Request $request, $id) 
    {
        try {
            if($request->input('status_id') == 2)
            {
                $update = DeploymentTarget::where('id',$id)->update(
                    [
                        'tgl_validasi_admin' =>date('Y-m-d'),
                        'status_id' => '3'
                    ]
                );

                return response()->json([
                    'success' => 'Validasi oleh administrasi berhasil',
                ]);
            }
            else if($request->input('status_id') == 3) 
            {
                $update = DeploymentTarget::where('id',$id)->update(
                    [
                        'tgl_validasi_atasan' =>date('Y-m-d'),
                        'status_id' => '4'
                    ]
                );

                return response()->json([
                    'success' => 'Validasi oleh atasan berhasil',
                ]);
            }

            
        } catch(Exception $ex) {
            return redirect()
                ->route('deployment.realisasi.index')
                ->with('success','Error'.$ex);
        }
    }
}
