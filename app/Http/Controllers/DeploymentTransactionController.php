<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Deployment;
use App\User;
use App\ProgresRealisasi;
use App\Unit;
use App\Accumulation;
use App\DeploymentTransaction as DeploymentTarget;

class DeploymentTransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // control untuk keypersone
        

        if(Auth::user()->hasRole('keyperson'))
        {
                $units = Unit::where('nik', Auth::user()->username)->get();
                $unitakhir = Unit::where('nik', Auth::user()->username)->take(1)->first();
               
                    if($request->input('tahun') == null  && $request->input('unit') == null )
                    {
                        $deployments = Deployment::where('kode_unit',$unitakhir->kode)->where('sts',0)->get();
                    }

                    else if($request->input('unit') != null && $request->input('tahun') == null)
                    {
                        $deployments = Deployment::where('kode_unit', $request->input('unit'))->where('sts',0)           
                            ->get();

                            //dd($deployments);
                    }
                    
                    else if($request->input('unit') == null && $request->input('tahun') != null)
                    {
                        $deployments = Deployment::where('tahun', $request->input('tahun'))->where('sts',0)           
                            ->get();
                    }
                    
                    else if($request->input('unit') != null && $request->input('tahun') != null)
                    {
                        $deployments = Deployment::where('tahun',$request->input('tahun'))    
                        ->where('kode_unit', $request->input('unit'))->where('sts',0)           
                        ->get();
                    }

                    $tampilunit = Unit::where('kode', $request->input('unit'))->first();
                    $namaunit=$tampilunit['nama'];    
        }

        if(Auth::user()->hasRole('adminkpi'))
        {
                $units = Unit::where('nik', Auth::user()->username)->get();
                $unitakhir = Unit::where('nik', Auth::user()->username)->take(1)->first();
               
                    if($request->input('tahun') == null)
                    {
                        $deployments = Deployment::where('sts',1)->get();
                    }

                    else
                    {
                        $deployments = Deployment::where('sts',1)->where('tahun', $request->input('tahun'))           
                            ->get();
                    }
                    
                    $tampilunit = Unit::where('kode', $request->input('unit'))->first();
                    $namaunit=$tampilunit['nama'];    
        }


        // control untuk selain Bos
        if(Auth::user()->hasRole('boss'))
        {
                $units = Unit::where('nik_atasan', Auth::user()->username)->get();
                $unitakhir = Unit::where('nik_atasan', Auth::user()->username)->first();
                //dd($unitakhir->kode);
                    if($request->input('tahun') == null  && $request->input('unit') == null )
                    {
                        $deployments = Deployment::where('kode_unit',$unitakhir->kode)->get();
                    }
                    else if($request->input('unit') != null && $request->input('tahun') == null)
                    {
                        $deployments = Deployment::where('kode_unit', $request->input('unit'))           
                            ->get();
                    }
                    else if($request->input('unit') == null && $request->input('tahun') != null)
                    {
                        $deployments = Deployment::where('tahun', $request->input('tahun'))           
                            ->get();
                    }
                    else if($request->input('unit') != null && $request->input('tahun') != null)
                    {
                        $deployments = Deployment::where('tahun',$request->input('tahun'))    
                        ->where('kode_unit', $request->input('unit'))           
                        ->get();
                    }

                    if($request->input('unit') == null){
                        $namaunit=$unitakhir['nama'];
                    }else{
                        $namaunit=$request->input('unit');
                    }
                    
                    
        }
        // control untuk selain keypersone

        if(Auth::user()->hasRole('administrator'))
        {
                $units = Unit::where('pimpinan','!=','none')->orderBy('nama','asc')->get();
                if($request->input('unit') == 'all' || $request->input('unit') == null)
                {
                    $deployments = Deployment::all();
                        
                }
                else 
                {
                    if($request->input('tahun') == null  && $request->input('unit') == null )
                    {
                        $deployments = Deployment::all();
                    }
                    else if($request->input('unit') != null && $request->input('tahun') == null)
                    {
                        $deployments = Deployment::where('kode_unit', $request->input('unit'))           
                            ->get();
                    }
                    else if($request->input('unit') == null && $request->input('tahun') != null)
                    {
                        $deployments = Deployment::where('tahun', $request->input('tahun'))           
                            ->get();
                    }
                    else if($request->input('unit') != null && $request->input('tahun') != null)
                    {
                        $deployments = Deployment::where('tahun',$request->input('tahun'))    
                        ->where('kode_unit', $request->input('unit'))           
                        ->get();
                    }
                        
                }
                if($request->input('unit')==null){
                    if($request->input('tahun')==null){
                        $cekda=Deployment::where('status_id',3)->where('tahun',date('Y'))->count();
                        if($cekda>0){
                            $tombol='<span class="btn btn-primary btn-sm" onclick="allvalidasi('.date('Y').')">
                                        <i class="fa fa-check" ></i> All Validasi 
                                    </span>';
                        }else{
                            $tombol='';
                        }
                    }else{
                        $cekda=Deployment::where('status_id',3)->where('tahun',$request->input('tahun'))->count();
                        if($cekda>0){
                            $tombol='<span class="btn btn-primary btn-sm" onclick="allvalidasi('.$request->input('tahun').')">
                                        <i class="fa fa-check" ></i> All Validasi 
                                    </span>';
                        }else{
                            $tombol='';
                        }
                    }
                }else{
                    $cekda=Deployment::where('status_id',3)->where('tahun',date('Y'))->where('kode_unit',$request->input('unit'))->count();
                    if($cekda>0){
                        $tombol='<span class="btn btn-primary btn-sm" onclick="onevalidasi('.date('Y').','.$request->input('unit').')">
                                    <i class="fa fa-check" ></i> Validasi 
                                </span>';
                    }else{
                        $tombol='';
                    }
                }
                
                $tampilunit = Unit::where('kode', $request->input('unit'))->first();
                $namaunit=$tampilunit['nama'];
                if($request->input('tahun')==null){
                    $tahundeploy=date('Y');
                }else{
                    $tahundeploy=$request->input('tahun');
                }
                
                
        }
            
            return view('deployment_targets.index', compact('deployments','units','namaunit','tahundeploy','tombol'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {

        try{
            // get data rumus akumulasi
            $accumulations = Accumulation::all();

            // get data deployments
            $deployment = Deployment::where('id', $id)
                ->first();
            
            // get data deployments target
            $targetdeployment = DeploymentTarget::where('id_deployment', $id)
                ->get();

            // jumlah target
            $jumlah = $targetdeployment->count();

            $capaian = collect([]);
            $capaian2 = collect([]);
            $capaian3 = collect([]);
            // $nilairealisasi = collect([]);
            $target = collect([]);
            
                for($x=1;$x<=12;$x++){
                    foreach($targetdeployment->where('bulan',$x) as $tar)
                    {
                        $target->push([
                            'id' => $tar->id,
                            'file' => $tar->file,
                            'status_realisasi' => $tar->status_realisasi,
                            'tgl_validasi_atasan' => $tar->tgl_validasi_atasan,
                            'target' => $tar->target,
                            'realisasi' => $tar->realisasi,
                        ]);

                        if($deployment['rumus_capaian']==1){
                            if($tar['target']==0){
                                $cap=100;
                            }else{
                                $cap=(1-(($tar['target']-$tar['realisasi'])/$tar['target']))*100;
                                
                            }

                            if($cap>120){
                                $nicap=120;
                            }else{
                                $nicap=$cap;
                            }
                            $capaian->push([
                                'capaian' => round($nicap)
                            ]);
                            $capaian2->push([
                                'capaian' => round($nicap)
                            ]);
                            $capaian3->push([
                                'capaian' => round($nicap)
                            ]); 
                        }


                        if($deployment['rumus_capaian']==2){
                            if($tar['target']==0){
                                $cap=100;
                            }else{
                                $cap=(1+(($tar['target']-$tar['realisasi'])/$tar['target']))*100;
                                
                            }

                            if($cap>120){
                                $nicap=120;
                            }else{
                                $nicap=$cap;
                            }
                            $capaian->push([
                                'capaian' => round($nicap)
                            ]);
                            $capaian2->push([
                                'capaian' => round($nicap)
                            ]);   
                            $capaian3->push([
                                'capaian' => round($nicap)
                            ]);   
                            
                        }

                        if($deployment['rumus_capaian']==3){
                            
                            if(is_null($tar['target']) || $tar['target']==0){
                                $cap=100;
                            }else{
                                $tanggal1 = date_create(date('Y-m-d h:i:s',strtotime($tar['realisasi'])));
                                $tanggal2 = date_create(date('Y-m-d h:i:s',strtotime($tar['target'])));
                                $perbedaan = $tanggal1->diff($tanggal2);
                                $cap=(1+($perbedaan->days/30))*100;
                            }

                            if($cap>120){
                                $nicap=120;
                            }else{
                                $nicap=$cap;
                            }
                            $capaian->push([
                                'capaian' => round($nicap)
                            ]);
                            $capaian2->push([
                                'capaian' => round($nicap)
                            ]);
                            $capaian3->push([
                                'capaian' => round($nicap)
                            ]); 
                            
                        }
                        if($deployment['rumus_capaian']==4){
                            if($tar['target']==0){
                                $nicap=55;
                            }else{
                                $expld=explode("-",$tar['target']);
                                if($expld[0]>$tar['realisasi']){
                                    $nicap=(1-(($expld[0]-$tar['realisasi'])/$expld[0]))*100;
                                }else{
                                    $nicap=(1-(($tar['realisasi']-$expld[1])/$expld[1]))*100;
                                }
                                
                            }
                            $capaian->push([
                                'capaian' => round($nicap)
                            ]);

                            $capaian2->push([
                                'capaian' => round($nicap)
                            ]);
                            $capaian3->push([
                                'capaian' => round($nicap)
                            ]); 

                            
                        }

                        if($deployment['rumus_capaian']==5){
                            if($tar['target']==0){
                                $nicap=100;
                            }else{
                                
                                if($tar['realisasi']>=120){
                                    $nicap=100;
                                }else if($tar['realisasi']>=110){
                                    $nicap=98;
                                }else if($tar['realisasi']>=100){
                                    $nicap=95;
                                }else{
                                    $nicap=0;
                                }
                                
                            }
                            $capaian->push([
                                'capaian' => round($nicap)
                            ]);
    
                            $capaian2->push([
                                'capaian' => round($nicap)
                            ]);
                            $capaian3->push([
                                'capaian' => round($nicap)
                            ]); 
    
                            
                        }

                        
                    }
                    
                }
           

            // dd($jumlah);
            $progres = ProgresRealisasi::orderBy('id','Desc')->get();
            return view('deployment_targets.create', compact('progres','deployment','accumulations','targetdeployment','jumlah','target','capaian','capaian2','capaian3'));
        }
        catch(Exception $ex)
        {
            return redirect()
                ->route('deployment_targets.index')
                ->with('success','Error'.$ex);
        }
       
    }
	
	public function validasitarget($id)
    {
        try{
            // get data rumus akumulasi
            $accumulations = Accumulation::all();

            // get data deployments
            $deployment = Deployment::where('id', $id)
                ->first();
            
            // get data deployments target
            $targetdeployment = DeploymentTarget::where('id_deployment', $id)
                ->get();

            // jumlah target
            $jumlah = $targetdeployment->count();

            $progres = ProgresRealisasi::orderBy('id','Desc')->get();
            $capaian = collect([]);
            $capaian2 = collect([]);
            $capaian3 = collect([]);
            // $nilairealisasi = collect([]);
            $target = collect([]);
            
            for($x=1;$x<=12;$x++){
                foreach($targetdeployment->where('bulan',$x) as $tar)
                {
                    $target->push([
                        'id' => $tar->id,
                        'file' => $tar->file,
                        'status_realisasi' => $tar->status_realisasi,
                        'tgl_validasi_atasan' => $tar->tgl_validasi_atasan,
                        'target' => $tar->target,
                        'realisasi' => $tar->realisasi,
                    ]);

                    if($deployment['rumus_capaian']==1){
                        if($tar['target']==0){
                            $cap=100;
                        }else{
                            $cap=(1-(($tar['target']-$tar['realisasi'])/$tar['target']))*100;
                            
                        }

                        if($cap>120){
                            $nicap=120;
                        }else{
                            $nicap=$cap;
                        }
                        $capaian->push([
                            'capaian' => round($nicap)
                        ]);
                        $capaian2->push([
                            'capaian' => round($nicap)
                        ]);
                        $capaian3->push([
                            'capaian' => round($nicap)
                        ]); 
                    }

                    
                    if($deployment['rumus_capaian']==2){
                        if($tar['target']==0){
                            $cap=100;
                        }else{
                            $cap=(1+(($tar['target']-$tar['realisasi'])/$tar['target']))*100;
                            
                        }

                        if($cap>120){
                            $nicap=120;
                        }else{
                            $nicap=$cap;
                        }
                        $capaian->push([
                            'capaian' => round($nicap)
                        ]);
                        $capaian2->push([
                            'capaian' => round($nicap)
                        ]);  
                        $capaian3->push([
                            'capaian' => round($nicap)
                        ]); 
                        
                        
                    }

                    if($deployment['rumus_capaian']==3){
                        
                        if(is_null($tar['target']) || $tar['target']==0){
                            $cap=100;
                        }else{
                            $tanggal1 = date_create(date('Y-m-d h:i:s',strtotime($tar['realisasi'])));
                            $tanggal2 = date_create(date('Y-m-d h:i:s',strtotime($tar['target'])));
                            $perbedaan = $tanggal1->diff($tanggal2);
                            $cap=(1+($perbedaan->days/30))*100;
                        }

                        if($cap>120){
                            $nicap=120;
                        }else{
                            $nicap=$cap;
                        }
                        $capaian->push([
                            'capaian' => round($nicap)
                        ]);
                        $capaian2->push([
                            'capaian' => round($nicap)
                        ]);
                        $capaian3->push([
                            'capaian' => round($nicap)
                        ]); 
                        
                    }

                    if($deployment['rumus_capaian']==4){
                        if($tar['target']==0){
                            $nicap=55;
                        }else{
                            $expld=explode("-",$tar['target']);
                            $nicap=$expld[1];
                        }
                        $capaian->push([
                            'capaian' => round($nicap)
                        ]);

                        $capaian2->push([
                            'capaian' => round($nicap)
                        ]);
                        $capaian3->push([
                            'capaian' => round($nicap)
                        ]); 

                        
                    }

                    
                }
                
            }
            // dd($jumlah);
            if(Auth::user()->hasRole('boss')){
                return view('deployment_targets.validasipimpinan', compact('progres','deployment','accumulations','targetdeployment','jumlah','target','capaian','capaian2','capaian3'));
            }else{
                return view('deployment_targets.validasi', compact('progres','deployment','accumulations','targetdeployment','jumlah','target','capaian','capaian2','capaian3'));
            }
           
           
        }
        catch(Exception $ex)
        {
            return redirect()
                ->route('deployment_targets.index')
                ->with('success','Error'.$ex);
        }
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->input('status_id'));
        if(Auth::user()->hasRole('boss'))
        {
            try {
                
                $deploy = Deployment::where('id',$request->input('id_deployment'))->update(
                    [
                       'status_id' => $request->input('status_id'),
                    ]
                );


            
                    // return redirect('/target/'.$request->input('id_deployment').'/deployments');
                return redirect()
                    ->route('deployment.target.index')
                    ->with('success','Target berhasil diinput.');
            }
            catch(Exception $ex)
            {
                return redirect()
                    ->route('deployment.target.index')
                    ->with('success','Error'.$ex);
            }
        }else{
            try {
                
                $deploy = Deployment::where('id',$request->input('id_deployment'))->update(
                    [
                        'akumulasi' => $request->input('totalbulanan'),
                        'accumulations_id' => $request->input('accumulations_id'),
                        'rumus_akumulasi' => $request->input('rumus_akumulasi'),
                        'rumus_capaian' => $request->input('rumus_capaian'),
                        'status_id' => $request->input('status_id'),
                    ]
                );


                for($i=1; $i<=12; $i++)
                {
                    if(is_null($request->input('bulan'.$i)) || $request->input('bulan'.$i)==0 || $request->input('bulan'.$i)=='')
                        {$target=0;$realisasi=100;$stsreal='1';}
                    else{$target=$request->input('bulan'.$i);$realisasi=0;$stsreal=null;}
                    if(is_null($request->input('id_target'.$i))){
                        
                        $flight = DeploymentTarget::create(
                            [
                                'id_deployment' => $request->input('id_deployment'),
                                'tahun' => $request->input('tahun'),
                                'bulan' => $i,
                                'target' => $target,
                                'realisasi' => 0,
                                'selisih' => $realisasi,
                                'status_realisasi' => $stsreal,
                                'kode_unit' => $request->input('kode_unit'),
                                'kode_kpi' => $request->input('kode_kpi'),
                                'tgl_target' => $request->input('tahun').'-'.$request->input('bulan'.$i).'-15',
                            ]
                        );
                    }else{
                        $flight = DeploymentTarget::where('id',$request->input('id_target'.$i))->update(
                            [
                                'id_deployment' => $request->input('id_deployment'),
                                'tahun' => $request->input('tahun'),
                                'bulan' => $i,
                                'target' => $target,
                                'status_realisasi' => $stsreal,
                                'kode_unit' => $request->input('kode_unit'),
                                'kode_kpi' => $request->input('kode_kpi'),
                                'tgl_target' => $request->input('tahun').'-'.$request->input('bulan'.$i).'-15',
                            ]
                        );

                    
                    }
                    
                }

            
                    // return redirect('/target/'.$request->input('id_deployment').'/deployments');
                    // return redirect()
                    //     ->route('deployment.target.index')
                    //     ->with('success','Target berhasil diinput.');
                    echo'<script> ;window.history.go(-2);</script>';
            }

            catch(Exception $ex)
            {
                return redirect()
                    ->route('deployment.target.index')
                    ->with('success','Error'.$ex);
            }
        }
            
        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // getdata deployment
        $deployment = Deployment::find($id);
        
        // get data deployments target
        $targetdeployment = DeploymentTarget::where('id_deployment', $id)
            ->get();

        // jumlah target
        $jumlah = $targetdeployment->count();

        // set to variabel view
        $view   = view('deployment_targets._detail', compact('deployment','targetdeployment','jumlah'))->render();

        // response json
        return response()->json(['html' => $view]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function Unvalidasi($id)
    {
        $Deployment = Deployment::where('id',$id)->update(
            [
                'status_id' => '2'
            ]
        );

        echo $id;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $Deployment = Deployment::where('id',$id)->update(
            [
                'status_id' => '3'
            ]
        );

        return response()->json([
            'success' => 'Validasi berhasil',
        ]);
    }

    public function formValidasiAdmin($id)
    {
        // getdata deployment
        $deployment = Deployment::find($id);
        
        // get data deployments target
        $targetdeployment = DeploymentTarget::where('id_deployment', $id)
            ->get();

        // jumlah target
        $jumlah = $targetdeployment->count();

        // set to variabel view
        $view   = view('deployment_targets.admin.validasi-target', compact('deployment','targetdeployment','jumlah'))->render();

        // response json
        return response()->json(['html' => $view]);
    }

    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * menamplkan modal / form validasi atasan
     */

    public function formValidasiAtasan($id)
    {
        // getdata deployment
        $deployment = Deployment::find($id);
        
        // get data deployments target
        $targetdeployment = DeploymentTarget::where('id_deployment', $id)
            ->get();

        // jumlah target
        $jumlah = $targetdeployment->count();

        // set to variabel view
        $view   = view('deployment_targets.boss.validasi-target', compact('deployment','targetdeployment','jumlah'))->render();

        // response json
        return response()->json(['html' => $view]);
    }
    

    /**
     * validasi pimpinan
     * @param int $id
     */
    public function validasiAtasan($id) 
    {
         //
         $Deployment = Deployment::where('id',$id)->update(
            [
                'status_id' => '4'
            ]
        );

        return response()->json([
            'success' => 'Validasi berhasil',
        ]);
    }

    public function allvalidasitarget() 
    {
         //
         $Deployment = Deployment::where('tahun',$_GET['tahun'])->where('status_id',3)->get();
         foreach($Deployment as $o){
            $dep                = Deployment::where('id',$o['id'])->first();
            $dep->status_id     =4;
            $dep->save();
         }
         
    }
    public function onevalidasitarget() 
    {
         //
         $Deployment = Deployment::where('tahun',$_GET['tahun'])->where('kode_unit',$_GET['kode'])->where('status_id',3)->get();
         foreach($Deployment as $o){
            $dep                = Deployment::where('id',$o['id'])->first();
            $dep->status_id     = 4;
            $dep->save();
         }
    }
}
