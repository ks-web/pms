<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\User;


class LoginController extends Controller
{
    

    use AuthenticatesUsers{
        logout as performLogout;
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function logout(Request $request)
    {
    // do the normal logout
    $this->performLogout($request);
    
    // redirecto to sso
    return redirect()->away('https://sso.krakatausteel.com');
    }
    
    public function programaticallyEmployeeLogin(Request $request, $personnel_no)
    {
        $personnel_no = base64_decode($personnel_no);
        try {
        
        $userlogin = User::where('username', $personnel_no)->first();
        //dd($userlogin);
        if(is_null($userlogin)){
            return redirect('https://sso.krakatausteel.com');
        }else{
            Auth::loginUsingId($userlogin->id);
            return redirect()
            ->route('home');
        }
        
        } catch (ModelNotFoundException $e) {
    
        return redirect('https://sso.krakatausteel.com');
        }

        return $this->sendLoginResponse($request);
    }
}
