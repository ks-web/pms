<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Unit;
use App\Deployment;
use App\DeploymentTransaction as DeploymentTarget;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(request $request,$kode=null,$nama=null,$tahun=null,$bulan=null)
    {
        if(is_null($kode)){
            $kodeun='';
            $nama='';
        }else{
            $kodeun=$kode;
            $nama=$nama;
        }
        
        
       
        if(Auth::user()->hasRole('boss'))
        {
            $unitakhir = array_column(Unit::where('nik_atasan',Auth::user()['username'])
            ->get()->toArray(), 'kode');
            if($request->tahun==''){
                $unit = Deployment::where('tahun',date('Y'))->whereIn('kode_unit',$unitakhir)->groupBy('kode_unit')->get();
            }else{
                $unit = Deployment::where('tahun',$request->tahun)->whereIn('kode_unit',$unitakhir)->groupBy('kode_unit')->get();
            }
            $tahuun=$request->tahun;
        }

        if(Auth::user()->hasRole('keyperson') || Auth::user()->hasRole('adminkpi'))
        {
           
                $unit = Deployment::where('tahun',date('Y'))->where('kode_unit',$kode)->groupBy('kode_unit')->get();
                $namaunit = Unit::where('nik', Auth::user()->username)->get();

                $tahuun=date('Y');
        }
        $kode=$kode;
        if(Auth::user()->hasRole('administrator'))
        {
            if($request->tahun==''){
                $unit = Deployment::where('tahun',date('Y'))->groupBy('kode_unit')->get();
            }else{
                $unit = Deployment::where('tahun',$request->tahun)->groupBy('kode_unit')->get();
            }
            $namaunit = Unit::all();
            $tahuun=$request->tahun;
        }
        
       //dd($unit);
        $jumvalidasi = collect([]);
        $tglvalidasi = collect([]);
        $tervalid = collect([]);
        $tervalid2 = collect([]);
        $isitervalid = collect([]);
        $isitervalid2 = collect([]);
        foreach($unit as $nn=>$o){
            //$tot=DeploymentTarget::where('kode_unit',$o->kode_unit)->where('tahun',2019)->where('status_realisasi',2)->count();
            $not=0;
            for($x=1;$x<=12;$x++){
                $validasi=DeploymentTarget::where('kode_unit',$o->kode_unit)->where('tahun',2019)->where('bulan',$x)->where('status_realisasi',2)->count();
                $ket=DeploymentTarget::where('kode_unit',$o->kode_unit)->where('tahun',2019)->where('bulan',$x)->where('status_realisasi',2)->first();
                if($validasi>=1){$nil=1;$val='<td class="ttdr" style="background:#38ef46">'.date('d/m/y',strtotime($ket['tgl_validasi_atasan'])).'</td>';}
                else{$nil=0;$val='<td class="ttdr" style="background:#fd2d2d;font-size:9px"></td>';}   


                $jumvalidasi->push([
                    'validasi' => $val
                ]);
                $tglvalidasi->push([
                    'tanggal' => date('d/m/y',strtotime($ket['tgl_validasi_atasan']))
                ]);
                $not +=$nil;
            }
            $tervalid->push([
                'jumlah' => round(($not/12)*100,1).'%',
                'kode_unit' => $o->kode_unit
            ]);
            $isitervalid->push([
                'jumlah' => round(($not/12)*100,1).'%',
                'kode_unit' => $o->kode_unit
            ]);
            $tervalid2->push([
                'jumlah' => round(($not/12)*100,1),
                'kode_unit' => $o->kode_unit
            ]);
            $isitervalid2->push([
                'jumlah' => round(($not/12)*100,1),
                'kode_unit' => $o->kode_unit
            ]);
        }

        $tahun=DeploymentTarget::groupBy('tahun')->get();
        if(is_null($request->bulan) || is_null($request->tahun)){
            $bulan='Tidak Ada bulan yang dipilih';
            $cekbulanan=DeploymentTarget::where('tahun',date('Y'))->where('status_realisasi',2)->where('bulan',date('m'))->groupBy('kode_unit')->count();
            $totalunit=Deployment::where('tahun',date('Y'))->groupBy('kode_unit')->get();
            $totalunitok=DeploymentTarget::where('tahun',date('Y'))->where('status_realisasi',2)->where('bulan',date('m'))->groupBy('kode_unit')->get();
        }else{
            $bulan=$request->bulan;
            $cekbulanan=DeploymentTarget::where('tahun',$request->tahun)->where('status_realisasi',2)->where('bulan',$request->bulan)->groupBy('kode_unit')->count();
            $totalunit=Deployment::where('tahun',$request->tahun)->groupBy('kode_unit')->get();
            $totalunitok=DeploymentTarget::where('tahun',$request->tahun)->where('status_realisasi',2)->where('bulan',$request->bulan)->groupBy('kode_unit')->get();
        }
       //dd($totalunit);
       $nilunit=0;
       foreach($totalunit as $oo){
           $nilunit+=1;
       }

       $nilunitok=0;
       foreach($totalunitok as $oo){
           $nilunitok+=1;
       }

       if($nilunit==0){
            $nilun=1;
       }else{
            $nilun=$nilunit;
       }

       $persem=100/$nilun;
       $totalada=round($persem*$nilunitok);
       $totalsisa=round(($nilunit-$nilunitok)*$persem);
       $totalsemua=collect([]);
       for($a=1;$a<13;$a++){
           if(is_null($request->tahun)){$thh=date('Y');}else{$thh=$request->tahun;}
            $toti=DeploymentTarget::where('tahun',$thh)->where('status_realisasi',2)->where('bulan',$a)->groupBy('kode_unit')->count();
            //$sisa=$nilunit-$toti;
            $totalsemua->push([
                'total' => ($nilun-$toti)*$persem
            ]);
       }
       
       //dd($nilunitok);
        return view('dashboard.index', compact('kode','tahuun','tahun','unit','tglvalidasi','jumvalidasi','kodeun','nama','namaunit','tervalid','tervalid2','isitervalid','isitervalid2','totalunit','nilunit','nilunitok','totalada','totalsisa','totalsemua'));
        
    }
    public function test(Request $request)
    {
        // return $request->filled('remember');
        dd( Auth::user() );
    }
}
