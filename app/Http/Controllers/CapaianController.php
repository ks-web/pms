<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Deployment;
use App\User;
use App\Unit;
use App\Accumulation;
use App\DeploymentTransaction as DeploymentTarget;
use App\DeploymentRealization;
class CapaianController extends Controller
{
    public function indexatasan(Request $request){
        
        $units = Unit::where('nik_atasan', Auth::user()->username)->get();
        $unitakhir = Unit::where('nik_atasan', Auth::user()->username)->first();
        
        if($request->input('tahun') != null)
        {
            
            $deployments = Deployment::where('kode_unit', $unitakhir->kode)
                ->where('status_id',4)->where('tahun', $request->input('tahun'))
                ->get();
            $tahunnya=$request->input('tahun');  
            $deploymentargett=DeploymentTarget::where('kode_unit', $unitakhir->kode)
            ->where('tahun',$request->input('tahun'))->get();
        }
        else 
        {
            
            $deployments = Deployment::where('kode_unit', $unitakhir->kode)
                ->where('status_id',4)->where('tahun', date('Y'))
                ->get();
            $tahunnya=date('Y');
            $deploymentargett=DeploymentTarget::where('kode_unit', $unitakhir->kode)
            ->where('tahun',date('Y'))->get();
        }

        //======================================
        




        
        $deploymentarget=DeploymentTarget::all();
        
    
        $isitarget = collect([]);
        $isirealisasi = collect([]);
        $isicapaian = collect([]);
        
        $totselisih = collect([]);
        $capaian = collect([]);
        $capaian2 = collect([]);
        $capaian3 = collect([]);
        $isitotaltarget = collect([]);
        $isitotalrealisasi = collect([]);
        $validasi=DeploymentTarget::where('kode_unit',$unitakhir->kode)->groupBy('kode_unit');
        
        foreach($deployments as $o){
            $targetisi=DeploymentTarget::where('id_deployment',$o->id)->where('target','!=',0)->count();
            $realisasiisi=DeploymentTarget::where('id_deployment',$o->id)->where('realisasi','!=',0)->count();
            $realisasidivalidasi=DeploymentTarget::where('id_deployment',$o->id)->where('status_realisasi',2)->count();
            $maxniltarget=DeploymentTarget::where('id_deployment',$o->id)->orderBy('target','Desc')->value('target');
            $maxnilrealisasi=DeploymentTarget::where('id_deployment',$o->id)->orderBy('realisasi','Desc')->value('realisasi');
            
            
            if($o['rumus_capaian']==3){
                $sumtarget=0;
                $totaltarget=0;
                $isitotaltarget->push([
                    'target' => $totaltarget
                ]);
                $isitotalrealisasi->push([
                    'realisasi' => 0
                ]);
            }else{
                $sumtarget=DeploymentTarget::where('id_deployment',$o->id)->sum('target');
                $sumrealisasi=DeploymentTarget::where('id_deployment',$o->id)->sum('realisasi');

                if($o['rumus_akumulasi']==1){
                    $totaltarget=$sumtarget;
                    $totalrealisasi=$sumrealisasi;
                    $isitotaltarget->push([
                        'target' => $totaltarget
                    ]);
                    $isitotalrealisasi->push([
                        'realisasi' => $totalrealisasi
                    ]);
                }elseif($o['rumus_akumulasi']==2){
                    $totaltarget=$sumtarget/$targetisi;
                    if($sumrealisasi==0){
                        $totalrealisasi=$sumrealisasi;
                    }else{
                        $totalrealisasi=$sumrealisasi/$realisasiisi;
                    }
                    
                    $isitotaltarget->push([
                        'target' => $totaltarget
                    ]);
                    $isitotalrealisasi->push([
                        'realisasi' => $totalrealisasi
                    ]);
                }else{
                    $totaltarget=$maxniltarget;
                    $totalrealisasi=$maxnilrealisasi;
                    $isitotaltarget->push([
                        'target' => $totaltarget
                    ]);
                    $isitotalrealisasi->push([
                        'realisasi' => $totalrealisasi
                    ]);
                }
            }
            for($x=1;$x<13;$x++){
                foreach($deploymentarget->where('bulan',$x)->where('id_deployment',$o->id) as $targetItem)
                {
                    
                        if($o->rumus_capaian==3){
                            if($targetItem->target==0){
                                $fctarget=0;
                                if($targetItem->realisasi==0){
                                    $fcrealisasi=0;
                                }else{
                                    $fcrealisasi=date("d/m/y",strtotime($targetItem->realisasi));
                                }
                                
                                
                            }else{
                                $fctarget=date("d/m/y",strtotime($targetItem->target));
                                if($targetItem->realisasi==0){
                                    $fcrealisasi=0;
                                }else{
                                    $fcrealisasi=date("d/m/y",strtotime($targetItem->realisasi));
                                }
                                
                            }
                            
                        }else{
                            $fctarget=$targetItem->target;
                            $fcrealisasi=$targetItem->realisasi;
                        }
                    // }
                        

                    


                    $isitarget->push([
                        'target' => $fctarget
                    ]);
                    $isirealisasi->push([
                        'realisasi' => $fcrealisasi
                    ]);
                    $isicapaian->push([
                        'capaian' => $targetItem->selisih
                    ]);
                    

                    if($o['rumus_capaian']==1){
                        if($targetItem['target']==0){
                            $cap=100;
                        }else{
                            if($targetItem['realisasi']==0){
                                $cap=0;
                            }else{
                                $cap=(1-(($targetItem['target']-$targetItem['realisasi'])/$targetItem['target']))*100;
                                
                            }
                            
                            
                        }

                        if($cap>120){
                            $nicap=120;
                        }else{
                            $nicap=$cap;
                        }
                        $capaian->push([
                            'capaian' => round($nicap)
                        ]);
                        $capaian2->push([
                            'capaian' => round($nicap)
                        ]);
                        $capaian3->push([
                            'capaian' => round($nicap)
                        ]); 
                    }

                    
                    if($o['rumus_capaian']==2){
                        if($targetItem['target']==0){
                            $cap=100;
                        }else{
                            
                            if($targetItem['realisasi']==0){
                                $cap=0;
                            }else{
                                $cap=(1+(($targetItem['target']-$targetItem['realisasi'])/$targetItem['target']))*100;
                                
                            }
                            
                        }

                        if($cap>120){
                            $nicap=120;
                        }else{
                            $nicap=$cap;
                        }
                        $capaian->push([
                            'capaian' => round($nicap)
                        ]);
                        $capaian2->push([
                            'capaian' => round($nicap)
                        ]);  
                        $capaian3->push([
                            'capaian' => round($nicap)
                        ]); 
                        
                        
                    }

                    if($o['rumus_capaian']==3){
                        
                        if(is_null($targetItem['target']) || $targetItem['target']==0){
                            $cap=100;
                        }else{
                            $tanggal1 = date_create(date('Y-m-d h:i:s',strtotime($targetItem['realisasi'])));
                            $tanggal2 = date_create(date('Y-m-d h:i:s',strtotime($targetItem['target'])));
                            $perbedaan = $tanggal1->diff($tanggal2)->format("%d");
                            if($targetItem['realisasi']==0){
                                $cap=0;
                            }else{
                                if($targetItem['realisasi']>$targetItem['target']){
                                    $cap=(1+(-$perbedaan/30))*100;
                                }else{
                                    $cap=(1+($perbedaan/30))*100;
                                }
                                
                            }
                            
                        }

                        if($cap>120){
                            $nicap=120;
                        }else{
                            $nicap=$cap;
                        }
                        $capaian->push([
                            'capaian' => round($nicap)
                        ]);
                        $capaian2->push([
                            'capaian' => round($nicap)
                        ]);
                        $capaian3->push([
                            'capaian' => round($nicap)
                        ]); 
                        
                    }

                    if($o['rumus_capaian']==4){
                        if($targetItem['target']==0){
                            $cap=100;
                        }else{
                            $expld=explode("-",$targetItem['target']);
                            if($expld[0]>$targetItem['realisasi']){
                                $cap=(1-(($expld[0]-$targetItem['realisasi'])/$expld[0]))*100;
                            }else{
                                $cap=(1-(($targetItem['realisasi']-$expld[1])/$expld[1]))*100;
                            }
                        }
                        if($cap>120){
                            $nicap=120;
                        }else{
                            $nicap=$cap;
                        }

                        $capaian->push([
                            'capaian' => round($nicap)
                        ]);

                        $capaian2->push([
                            'capaian' => round($nicap)
                        ]);
                        $capaian3->push([
                            'capaian' => round($nicap)
                        ]); 

                        
                    }

                    if($o['rumus_capaian']==5){
                        if($targetItem['target']==0){
                            $nicap=100;
                        }else{
                            
                            if($targetItem['realisasi']>=120){
                                $nicap=100;
                            }else if($targetItem['realisasi']>=110){
                                $nicap=98;
                            }else if($targetItem['realisasi']>=100){
                                $nicap=95;
                            }else{
                                $nicap=0;
                            }
                            
                        }
                        $capaian->push([
                            'capaian' => round($nicap)
                        ]);

                        $capaian2->push([
                            'capaian' => round($nicap)
                        ]);
                        $capaian3->push([
                            'capaian' => round($nicap)
                        ]); 

                        
                    }

                }
                
            }
        }

        $validasinya=collect([]);
        $nilaicapaian=collect([]);
        for($x=1;$x<13;$x++)
        {   $nilo=0;
            $ceknulvalidasi=DeploymentTarget::where('kode_unit', $unitakhir->kode)->where('bulan',$x)->where('tahun',$tahunnya)->where('realisasi',0)->where('target','!=',0)->count();
            $cektarget=DeploymentTarget::where('kode_unit', $unitakhir->kode)->where('bulan',$x)->where('tahun',$tahunnya)->where('realisasi','!=',0)->where('target','!=',0)->count();
            $tidakdisiisi=DeploymentTarget::where('kode_unit', $unitakhir->kode)->where('bulan',$x)->where('tahun',$tahunnya)->where('target',0)->count();
            $jumlahdata=Deployment::where('kode_unit', $unitakhir->kode)->where('tahun',$tahunnya)->count();
            $cekvalidasi=DeploymentTarget::where('kode_unit', $unitakhir->kode)->where('bulan',$x)->where('tahun',$tahunnya)->where('status_realisasi',2)->count();
            $ket=DeploymentTarget::where('kode_unit', $unitakhir->kode)->where('bulan',$x)->where('tahun',$tahunnya)->where('status_realisasi',2)->first();
            //dd($unitakhir->kode);
            $totalwajib=$jumlahdata-$tidakdisiisi;
            if($cektarget==$totalwajib){
                $tampil='<span class="btn btn-danger btn-sm" onclick="validasiyah('.$x.','.$unitakhir->kode.','.$tahunnya.')"><i class="fa fa-check"></i></span>';
            }else{
                if($cekvalidasi==$jumlahdata){
                    $tampil=date('d/m/y',strtotime($ket['tgl_validasi_atasan']));
                }else{
                    $tampil='<span class="btn btn-warning btn-sm" disable >'.$cektarget.'/'.$totalwajib.'<i class="fa fa-check"></i></span>';
                }
               
            }
             
            $validasinya->push([
                'validasi' => $tampil
            ]);
            
            
            foreach($deploymentargett->where('bulan',$x) as $targetItem)
            {
                
                if($targetItem->deployment['rumus_capaian']==1){
                    if($targetItem['target']==0){
                        $cap=100;
                    }else{
                        $cap=(1-(($targetItem['target']-$targetItem['realisasi'])/$targetItem['target']))*100;
                        
                    }

                    if($cap>120){
                        $nicap=120*($targetItem->deployment['bobot_tahunan']/100);
                    }else{
                        $nicap=$cap*($targetItem->deployment['bobot_tahunan']/100);
                    }
                    
                }

                
                if($targetItem->deployment['rumus_capaian']==2){
                    if($targetItem['target']==0){
                        $cap=100;
                    }else{
                        $cap=(1+(($targetItem['target']-$targetItem['realisasi'])/$targetItem['target']))*100;
                        
                    }

                    if($cap>120){
                        $nicap=120*($targetItem->deployment['bobot_tahunan']/100);
                    }else{
                        $nicap=$cap*($targetItem->deployment['bobot_tahunan']/100);
                    }
                    
                    
                    
                }

                if($targetItem->deployment['rumus_capaian']==3){
                    
                    if(is_null($targetItem['target']) || $targetItem['target']==0){
                        $cap=100;
                    }else{
                        $tanggal1 = date_create(date('Y-m-d h:i:s',strtotime($targetItem['realisasi'])));
                        $tanggal2 = date_create(date('Y-m-d h:i:s',strtotime($targetItem['target'])));
                        $perbedaan = $tanggal1->diff($tanggal2);
                        $cap=(1+($perbedaan->days/30))*100;
                    }

                    if($cap>120){
                        $nicap=120*($targetItem->deployment['bobot_tahunan']/100);
                    }else{
                        $nicap=$cap*($targetItem->deployment['bobot_tahunan']/100);
                    }
                    
                    
                }

                if($targetItem->deployment['rumus_capaian']==4){
                    if($targetItem['target']==0){
                        $cap=100;
                    }else{
                        $expld=explode("-",$targetItem['target']);
                        if($expld[0]>$targetItem['realisasi']){
                            $cap=(1-(($expld[0]-$targetItem['realisasi'])/$expld[0]))*100;
                        }else{
                            $cap=(1-(($targetItem['realisasi']-$expld[1])/$expld[1]))*100;
                        }
                    }
                    if($cap>120){
                        $nicap=120*($targetItem->deployment['bobot_tahunan']/100);
                    }else{
                        $nicap=$cap*($targetItem->deployment['bobot_tahunan']/100);
                    }
                    
                    
                    
                }
                $nilo+=round($nicap,2);
            }
            
            $nilaicapaian->push([
                'capaian' => round($nilo)
            ]);
        }
        
        $unitnya=$unitakhir['kode'];
        $nmunitnya=$unitakhir['nama'];
            
        return view('capaian.indexatasan', compact('validasi','nmunitnya','tahunnya','unitnya','deploymentarget','unitkerja','deployments','isitarget','isirealisasi','isicapaian','validasinya','units','capaian','capaian2','capaian3','nilaicapaian','isitotaltarget','isitotalrealisasi'));
       
    }


    public function laporancapaian(Request $request){
        
            $units = Unit::where('nik_atasan', Auth::user()->username)->get();
            $unitakhir = Unit::where('nik_atasan', Auth::user()->username)->first();
            
            if($request->input('tahun') != null)
            {
                
                $deployments = Deployment::where('kode_unit', $unitakhir->kode)
                    ->where('status_id',4)->where('tahun', $request->input('tahun'))
                    ->get();
                $tahunnya=$request->input('tahun');  
            }
            else 
            {
                
                $deployments = Deployment::where('kode_unit', $unitakhir->kode)
                    ->where('status_id',4)->where('tahun', date('Y'))
                    ->get();
                $tahunnya=date('Y');
            }

            
            $deploymentarget=DeploymentTarget::all();
        
            $isitarget = collect([]);
            $isirealisasi = collect([]);
            $isicapaian = collect([]);
            $validasinya = collect([]);
            $totselisih = collect([]);

            $validasi=DeploymentTarget::where('kode_unit',$unitakhir->kode)->groupBy('kode_unit');
            
            foreach($deployments as $o){
                for($x=1;$x<13;$x++){
                    foreach($deploymentarget->where('bulan',$x)->where('id_deployment',$o->id) as $targetItem)
                    {
                        if($targetItem->target==0 || $targetItem->realisasi==0){
                            $fctarget=0;
                            $fcrealisasi=0;
                            
                        }else{
                            if($o->rumus_capaian==3){
                                $fctarget=date("d/m/y",strtotime($targetItem->target));
                                $fcrealisasi=date("d/m/y",strtotime($targetItem->realisasi));
                            }else{
                                $fctarget=$targetItem->target;
                                $fcrealisasi=$targetItem->realisasi;
                            }
                        }

                        if(is_null($targetItem->tgl_validasi_atasan)){
                            $vali='';
                        }else{
                            $vali=date('d/m/y',strtotime($targetItem->tgl_validasi_atasan));
                        }

                        $isitarget->push([
                            'target' => $fctarget
                        ]);
                        $isirealisasi->push([
                            'realisasi' => $fcrealisasi
                        ]);
                        $isicapaian->push([
                            'capaian' => $targetItem->selisih
                        ]);
                        $validasinya->push([
                            'validasi' => $vali
                        ]);
                    }
                    
                }
            }
        
            $unitnya=$unitakhir['kode'];
            $nmunitnya=$unitakhir['nama'];
            
        return view('capaian.laporancapaian', compact('validasi','nmunitnya','tahunnya','unitnya','deploymentarget','unitkerja','deployments','isitarget','isirealisasi','isicapaian','validasinya','units'));
    
    }

    public function laporancapaianadmin(Request $request,$kode=null,$nama=null,$tahun=null){
        if(Auth::user()->hasRole('keyperson')){
            $units = Unit::where('nik', Auth::user()->username)->get();
        }elseif(Auth::user()->hasRole('boss')){
            $units = Unit::where('nik_atasan', Auth::user()->username)->get();
        }else{
            $units = Unit::all();
        }
       
        
        
        if($tahun != null)
        {
            $tahunnya=$tahun;
        }
        else 
        {
            $tahunnya=date('Y');
        }
        if(Auth::user()->hasRole('keyperson') || Auth::user()->hasRole('administrator')){
            $deployments = Deployment::where('kode_unit', $kode)
                ->where('status_id',4)->where('tahun', $tahunnya)
                ->get();
            
            $deploymentarget=DeploymentTarget::where('tahun',$tahunnya)->get();
            $deploymentargett=DeploymentTarget::where('tahun',$tahunnya)->where('kode_unit',$kode)->get();
            $validasi=DeploymentTarget::where('kode_unit',$kode)->groupBy('kode_unit');
            $unitnya=$tahunnya;
            $nmunitnya=$nama;
        }else{
            
            $unitsatasan = Unit::where('nik_atasan', Auth::user()->username)->first();
            
            $deployments = Deployment::where('kode_unit', $unitsatasan->kode)
            ->where('status_id',4)->where('tahun',$tahunnya)
            ->get();
            // dd($deployments);
            $deploymentarget=DeploymentTarget::where('tahun',date('Y'))->get();
            $deploymentargett=DeploymentTarget::where('tahun',date('Y'))->where('kode_unit',$unitsatasan->kode)->get();
            $validasi=DeploymentTarget::where('kode_unit',$unitsatasan->kode)->groupBy('kode_unit');
            $unitnya=date('Y');
            $nmunitnya=$unitsatasan->nama;
        }
        $isitarget = collect([]);
        $isirealisasi = collect([]);
        $isicapaian = collect([]);
        $validasinya = collect([]);
        $totselisih = collect([]);
        $capaian = collect([]);
        $capaian2 = collect([]);
        $capaian3 = collect([]);
        $isitotaltarget = collect([]);
        $isitotalrealisasi = collect([]);
        $scoreisitotalrealisasi = collect([]);
        $scoreisitotaltarget = collect([]);
        
        
        foreach($deployments as $o){
            $targetisi=DeploymentTarget::where('id_deployment',$o->id)->where('target','!=',0)->count();
            $realisasiisi=DeploymentTarget::where('id_deployment',$o->id)->where('realisasi','!=',0)->count();
            $realisasidivalidasi=DeploymentTarget::where('id_deployment',$o->id)->where('status_realisasi',2)->count();
            $maxniltarget=DeploymentTarget::where('id_deployment',$o->id)->orderBy('target','Desc')->value('target');
            $maxnilrealisasi=DeploymentTarget::where('id_deployment',$o->id)->orderBy('realisasi','Desc')->value('realisasi');
            
            
            if($o['rumus_capaian']==3){
                $sumtarget=0;
                $totaltarget=0;
                $isitotaltarget->push([
                    'target' => $totaltarget
                ]);
                $scoreisitotaltarget->push([
                    'score' => 0
                ]);
                $isitotalrealisasi->push([
                    'realisasi' => 0
                ]);
                $scoreisitotalrealisasi->push([
                    'score' => 0
                ]);
            }else{
                $sumtarget=DeploymentTarget::where('id_deployment',$o->id)->sum('target');
                $sumrealisasi=DeploymentTarget::where('id_deployment',$o->id)->sum('realisasi');

                if($o['rumus_akumulasi']==1){
                    $totaltarget=$sumtarget;
                    $totalrealisasi=$sumrealisasi;
                    $isitotaltarget->push([
                        'target' => $totaltarget
                    ]);
                    $scoreisitotaltarget->push([
                        'score' => $totaltarget*$o->bobot_tahunan
                    ]);
                    $isitotalrealisasi->push([
                        'realisasi' => $totalrealisasi
                    ]);
                    $scoreisitotalrealisasi->push([
                        'score' => $totalrealisasi*$o->bobot_tahunan
                    ]);
                }elseif($o['rumus_akumulasi']==2){
                    $totaltarget=$sumtarget/$targetisi;
                    if($sumrealisasi==0){
                        $totalrealisasi=$sumrealisasi;
                    }else{
                        $totalrealisasi=$sumrealisasi/$realisasiisi;
                    }
                    
                    
                    $isitotaltarget->push([
                        'target' => $totaltarget
                    ]);
                    $scoreisitotaltarget->push([
                        'score' => $totaltarget*$o->bobot_tahunan
                    ]);
                    $isitotalrealisasi->push([
                        'realisasi' => $totalrealisasi
                    ]);
                    $scoreisitotalrealisasi->push([
                        'score' => $totalrealisasi*$o->bobot_tahunan
                    ]);
                }else{
                    $totaltarget=$maxniltarget;
                    $totalrealisasi=$maxnilrealisasi;
                    $isitotaltarget->push([
                        'target' => $totaltarget
                    ]);
                    $scoreisitotaltarget->push([
                        'score' => $totaltarget*$o->bobot_tahunan
                    ]);
                    $isitotalrealisasi->push([
                        'realisasi' => $totalrealisasi
                    ]);
                    $scoreisitotalrealisasi->push([
                        'score' => $totalrealisasi*$o->bobot_tahunan
                    ]);
                }
            }

            //dd($maxtarget);

            // dd($sumtarget);
            
            for($x=1;$x<13;$x++)
            {
                $realnil=DeploymentTarget::where('bulan',$x)->where('id_deployment',$o->id)->first();
                $nilo=0;
                foreach($deploymentarget->where('bulan',$x)->where('id_deployment',$o->id) as $targetItem)
                {
                    if($o->rumus_capaian==3){
                        if($targetItem->target==0){
                            $fctarget=0;
                            if($targetItem->realisasi==0){
                                $fcrealisasi=0;
                            }else{
                                $fcrealisasi=date("d/m/y",strtotime($targetItem->realisasi));
                            }
                            
                            
                        }else{
                            $fctarget=date("d/m/y",strtotime($targetItem->target));
                            if($targetItem->realisasi==0){
                                $fcrealisasi=0;
                            }else{
                                $fcrealisasi=date("d/m/y",strtotime($targetItem->realisasi));
                            }
                            
                        }
                        
                    }else{
                        $fctarget=$targetItem->target;
                        $fcrealisasi=$targetItem->realisasi;
                    }

                    // if(is_null($targetItem->tgl_validasi_atasan)){
                    //     $vali='';
                    // }else{
                    //     $vali=date('d/m/y',strtotime($targetItem->tgl_validasi_atasan));
                    // }

                    $isitarget->push([
                        'target' => $fctarget
                    ]);
                    $isirealisasi->push([
                        'realisasi' => $fcrealisasi
                    ]);
                    $isicapaian->push([
                        'capaian' => $targetItem->selisih
                    ]);
                    // $validasinya->push([
                    //     'validasi' => $vali
                    // ]);

                    if($o['rumus_capaian']==1){
                        if($targetItem['target']==0){
                            $cap=100;
                        }else{
                            $cap=(1-(($targetItem['target']-$targetItem['realisasi'])/$targetItem['target']))*100;
                            
                        }
                        
                        if($targetItem['status_realisasi']==2){
                            if($cap>120){
                                $nicap=120;
                            }else{
                                $nicap=$cap;
                            }
                        }else{
                            $nicap=0;
                        }

                        $capaian->push([
                            'capaian' => round($nicap)
                        ]);
                        $capaian2->push([
                            'capaian' => round($nicap)
                        ]);
                        $capaian3->push([
                            'capaian' => round($nicap)
                        ]); 
                    }

                    
                    if($o['rumus_capaian']==2){
                        if($targetItem['target']==0){
                            $cap=100;
                        }else{
                            $cap=(1+(($targetItem['target']-$targetItem['realisasi'])/$targetItem['target']))*100;
                            
                        }

                        if($targetItem['status_realisasi']==2){
                            if($cap>120){
                                $nicap=120;
                            }else{
                                $nicap=$cap;
                            }
                        }else{
                            $nicap=0;
                        }

                        $capaian->push([
                            'capaian' => round($nicap)
                        ]);
                        $capaian2->push([
                            'capaian' => round($nicap)
                        ]);  
                        $capaian3->push([
                            'capaian' => round($nicap)
                        ]); 
                        
                        
                    }

                    if($o['rumus_capaian']==3){
                        
                        if(is_null($targetItem['target']) || $targetItem['target']==0){
                            $cap=100;
                        }else{
                            $tanggal1 = date_create(date('Y-m-d h:i:s',strtotime($targetItem['realisasi'])));
                            $tanggal2 = date_create(date('Y-m-d h:i:s',strtotime($targetItem['target'])));
                            $par = $tanggal1->diff($tanggal2)->format("%m");
                            $perbedaan = $tanggal1->diff($tanggal2);
                            $cap=(1+($perbedaan->days/30))*100;
                        }

                        if($targetItem['status_realisasi']==2){
                            if($cap>120){
                                $nicap=120;
                            }else{
                                $nicap=$cap;
                            }
                        }else{
                            $nicap=0;
                        }
                        
                        $capaian->push([
                            'capaian' => round($nicap)
                        ]);
                        $capaian2->push([
                            'capaian' => round($nicap)
                        ]);
                        $capaian3->push([
                            'capaian' => round($nicap)
                        ]); 
                        
                    }

                    if($o['rumus_capaian']==4){
                        if($targetItem['target']==0){
                            $cap=100;
                        }else{
                            $expld=explode("-",$targetItem['target']);
                            if($expld[0]>$targetItem['realisasi']){
                                $cap=(1-(($expld[0]-$targetItem['realisasi'])/$expld[0]))*100;
                            }else{
                                $cap=(1-(($targetItem['realisasi']-$expld[1])/$expld[1]))*100;
                            }
                        }
                        if($cap>120){
                            $nicap=120;
                        }else{
                            $nicap=$cap;
                        }

                        $capaian->push([
                            'capaian' => round($nicap)
                        ]);

                        $capaian2->push([
                            'capaian' => round($nicap)
                        ]);
                        $capaian3->push([
                            'capaian' => round($nicap)
                        ]); 

                        
                    }

                    if($o['rumus_capaian']==5){
                        if($targetItem['target']==0){
                            $nicap=100;
                        }else{
                            
                            if($targetItem['realisasi']>=120){
                                $nicap=100;
                            }else if($targetItem['realisasi']>=110){
                                $nicap=98;
                            }else if($targetItem['realisasi']>=100){
                                $nicap=95;
                            }else{
                                $nicap=0;
                            }
                            
                        }
                        $capaian->push([
                            'capaian' => round($nicap)
                        ]);

                        $capaian2->push([
                            'capaian' => round($nicap)
                        ]);
                        $capaian3->push([
                            'capaian' => round($nicap)
                        ]); 

                        
                    }

                    
                }
                
            }
            
            
           
        }
        $unitakhir=Unit::where('kode',$kode)->first();
        $nilaicapaian = collect([]);
        $potongan = collect([]);
        $finis = collect([]);
        $nilo=0;
        for($x=1;$x<13;$x++)
        {   
            $ceknulvalidasi=DeploymentTarget::where('kode_unit', $kode)->where('bulan',$x)->where('tahun',$tahunnya)->where('status_realisasi',null)->count();
            //$cekvalidasi=DeploymentTarget::where('kode_unit', $unitakhir->kode)->where('bulan',$x)->where('tahun',$tahunnya)->where('status_realisasi',2)->count();
            $cekvalidasi=DeploymentTarget::where('kode_unit', $kode)->where('bulan',$x)->where('tahun',$tahunnya)->where('status_realisasi',2)->count();
            $ket=DeploymentTarget::where('kode_unit', $kode)->where('bulan',$x)->where('tahun',$tahunnya)->where('status_realisasi',2)->first();
            
            if($cekvalidasi==0){
               
                     $tampil='';
               
                 
                 
                
             }else{
                 $tampil=date('d/m/y',strtotime($ket['tgl_validasi_atasan']));
             }
            
            $validasinya->push([
                'validasi' => $tampil
            ]);

            //----penerapan nilai potongan-------------------------------
            if(is_null($ket['tgl_validasi_atasan'])){
                $potongannya='0';
            }else{
                if($x>11){
                    $bul=1;
                    $tah=$tahun+1;
                }else{
                    $bul=$x+1;
                    $tah=$tahun;
                }
                $tgll =explode('-',$ket['tgl_validasi_atasan']);
                $mulai=$ket['tgl_validasi_atasan'];
                $sampai=$tgll[0].'-'.$bul.'-05';
                $tanggal1 = date_create(date('Y-m-d h:i:s',strtotime($mulai)));
                $tanggal2 = date_create(date('Y-m-d h:i:s',strtotime($sampai)));
                $perbedaan = $tanggal1->diff($tanggal2);
                if(num_tgl($sampai)-num_tgl($mulai)>1){
                    $potongannya=0;
                }else{
                    $potongannya=$perbedaan->days;
                }
              // dd($potongannya);
                //dd(num_tgl($mulai).'-'.num_tgl($sampai).'='.(num_tgl($sampai)-num_tgl($mulai)));
                
            }
            
            if($potongannya*0.05>50){
                $potongan->push([
                    'potongan' => '50%'
                ]);
            }else{
                $potongan->push([
                    'potongan' => $potongannya*0.05.'%'
                ]);
            }
            
            



            foreach($deploymentargett->where('bulan',$x) as $targetItem)
            {
                
                if($targetItem->deployment['rumus_capaian']==1){
                    if($targetItem['target']==0){
                        $cap=100;
                    }else{
                        $cap=(1-(($targetItem['target']-$targetItem['realisasi'])/$targetItem['target']))*100;
                        
                    }

                    if($cap>120){
                        $nicap=120*($targetItem->deployment['bobot_tahunan']/100);
                    }else{
                        $nicap=$cap*($targetItem->deployment['bobot_tahunan']/100);
                    }
                    
                }

                
                
                if($targetItem->deployment['rumus_capaian']==2){
                    if($targetItem['target']==0){
                        $cap=100;
                    }else{
                        $cap=(1+(($targetItem['target']-$targetItem['realisasi'])/$targetItem['target']))*100;
                        
                    }

                    if($cap>120){
                        $nicap=120*($targetItem->deployment['bobot_tahunan']/100);
                    }else{
                        $nicap=$cap*($targetItem->deployment['bobot_tahunan']/100);
                    }
                    
                    
                    
                }

                if($targetItem->deployment['rumus_capaian']==3){
                    
                    if(is_null($targetItem['target']) || $targetItem['target']==0){
                        $cap=100;
                    }else{
                        $tanggal1 = date_create(date('Y-m-d h:i:s',strtotime($targetItem['realisasi'])));
                        $tanggal2 = date_create(date('Y-m-d h:i:s',strtotime($targetItem['target'])));
                        $perbedaan = $tanggal1->diff($tanggal2);
                        $cap=(1+($perbedaan->days/30))*100;
                    }

                    if($cap>120){
                        $nicap=120*($targetItem->deployment['bobot_tahunan']/100);
                    }else{
                        $nicap=$cap*($targetItem->deployment['bobot_tahunan']/100);
                    }
                    
                    
                }

                if($targetItem->deployment['rumus_capaian']==4){
                    if($targetItem['target']==0){
                        $cap=100;
                    }else{
                        $expld=explode("-",$targetItem['target']);
                        if($expld[0]>$targetItem['realisasi']){
                            $cap=(1-(($expld[0]-$targetItem['realisasi'])/$expld[0]))*100;
                        }else{
                            $cap=(1-(($targetItem['realisasi']-$expld[1])/$expld[1]))*100;
                        }
                    }
                    if($cap>120){
                        $nicap=120*($targetItem->deployment['bobot_tahunan']/100);
                    }else{
                        $nicap=$cap*($targetItem->deployment['bobot_tahunan']/100);
                    }
                    
                }

                if($targetItem->deployment['rumus_capaian']==5){
                    if($targetItem['realisasi']>=120){
                        $nicap=100;
                    }else if($targetItem['realisasi']>=110){
                        $nicap=98;
                    }else if($targetItem['realisasi']>=100){
                        $nicap=95;
                    }else{
                        $nicap=0;
                    }
                    
                }
                $nilo+=$nicap;
            }
            
            $nilaicapaian->push([
                'capaian' => round($nilo)
            ]);
            
           
            $finis->push([
                'finis' => round($nilo)-($potongannya*0.05)
            ]);
        }
        
        $kode=$kode;
        
        return view('capaian.laporanadmin', compact('kode','tahun','finis','scoreisitotaltarget','scoreisitotalrealisasi','potongan','validasi','nmunitnya','tahunnya','unitnya','deploymentarget','unitkerja','deployments','isitarget','isirealisasi','isicapaian','validasinya','units','capaian','capaian2','capaian3','nilaicapaian','isitotaltarget','isitotalrealisasi'));
   
    }
    

    public function laporan(Request $request,$kode=null,$tahun=null){
        if(Auth::user()->hasRole('keyperson')){
            $units = Unit::where('nik', Auth::user()->username)->get();
        }elseif(Auth::user()->hasRole('boss')){
            $units = Unit::where('nik_atasan', Auth::user()->username)->get();
        }else{
            $units = unit();
        }
        $kode=$request->kode;
        $nama=$request->nama;
        $tahun=$request->tahun;
        return view('capaian.laporan' ,compact('units','kode','nama','tahun'));
    }

    public function laporan_validasi(Request $request,$kode=null,$tahun=null){
        if(Auth::user()->hasRole('keyperson')){
            $units = Unit::where('nik', Auth::user()->username)->get();
        }elseif(Auth::user()->hasRole('boss')){
            $units = Unit::where('nik_atasan', Auth::user()->username)->get();
        }else{
            $units = unit();
        }
        $kode=$request->kode;
        $nama=$request->nama;
        $tahun=$request->tahun;
        return view('capaian.validasi' ,compact('units','kode','nama','tahun'));
    }



    public function view_laporan($kode=null,$tahun=null){
        
            $tahun=$tahun;
            $kode=$kode;
        
        
        if(Auth::user()->hasRole('keyperson')){
            $units = Unit::where('nik', Auth::user()->username)->get();
        }elseif(Auth::user()->hasRole('boss')){
            $units = Unit::where('nik_atasan', Auth::user()->username)->get();
        }else{
            $units = Unit::where('unit_id','!=',4)->where('unit_id','!=',6)->get();
        }

        if(Auth::user()->hasRole('keyperson') || Auth::user()->hasRole('administrator')){
            $deployments = Deployment::where('kode_unit', $kode)->where('status_id',4)->where('tahun', $tahun)->get();
            
        }else{
            $deployments = Deployment::where('kode_unit', $kode)->where('status_id',4)->where('tahun',$tahun)->get();
            
        }
        // dd(date('Y-m-d',strtotime(2020-2-1)));
        echo'
            <table border="1" width="140%">
                <thead>
                    <tr> 
                        <th class="isidatath" rowspan="2" width="4%">Kode KPI</th>
                        <th class="isidatath" rowspan="2">Nama KPI</th>
                        <th class="isidatath" rowspan="2" width="5%">Bobot</th>
                        <th class="isidatath" rowspan="2" width="5%">Target</th>
                        <th class="isidatath" rowspan="2" width="5%">Satuan</th>
                        <th class="isidatath" rowspan="2" width="5%">Ket</th>
                        <th class="isidatath" colspan="12">Bulan</th>
                        <th class="isidatath" rowspan="2" width="5%">Akumulasi</th>
                        <th class="isidatath" rowspan="2" width="5%">Score</th>
                        
                    </tr>
                    <tr>';
                        for($i=1; $i<=12; $i++){
                            echo'<th class="isidatath" width="3%" style="text-align:center;">'.$i.'</th>';
                        }
                    echo'
                    </tr>';
                    for($i=1; $i<=12; $i++){
                        $hasilcapaian[$i]=0;
                    }
                        $subscore=0;
                        $subskumulasi=0;
                        foreach ($deployments as $item){
                            echo'
                                <tr>
                                    <td  rowspan="3">'.$item->id.'</td>
                                    <td rowspan="3">'.$item->kpi['kpi'].'</td>
                                    <td rowspan="3" class="isidata">'.$item->bobot_tahunan.'</td>
                                    <td rowspan="3" class="isidata">'.$item->target_tahunan.'</td>
                                    <td rowspan="3" class="isidata">'.$item->kpi['satuan'].'/'.$item->rumus_akumulasi.'/'.$item->rumus_capaian.'</td>
                                    <td class="isidata" style="text-align:left">Target</td>';
                                        $akumulasicapaian=0;
                                        $totaltarget=0;
                                        $totaltargets=0;
                                        for($i=1; $i<=12; $i++){
                                            $target=DeploymentTarget::where('id_deployment',$item->id)->where('bulan',$i)->where('tahun',$tahun)->first();
                                            if($item->rumus_capaian==3){
                                                $akumulasicapaian+=0;
                                            }else{
                                                $akumulasicapaian+=target($item['rumus_capaian'],$target['target']);
                                            }
                                            if($target['target']==0){
                                                $totaltarget+=0;
                                            }else{
                                                $totaltarget+=1;
                                            }
                                            if($item->rumus_akumulasi==1){
                                                if($target['realisasi']==0){
                                                    $totaltargets+=0;
                                                }else{
                                                    $totaltargets+=$target['target'];
                                                }
                                            }
                                            echo'<td align="center" style="font-size:12px">'.target($item['rumus_capaian'],$target['target']).'</td>';
                                        }
                                     echo'
                                    <td class="isidata">';
                                        if($item->rumus_akumulasi==1){
                                            echo''.round($totaltargets).'';
                                        }elseif($item->rumus_akumulasi==2){
                                            echo''.round($akumulasicapaian/$totaltarget).'';
                                        }else{
                                            echo''.progres_target($item->id).'';
                                        }

                                        echo'
                                    </td>
                                    <td class="isidata" style="border-bottom:solid 1px #fff"></td>
                                </tr>

                                <tr>
                                    <td class="isidata" style="text-align:left">Realisasi</td>';
                                        $akumulasirealisasi=0;
                                        $totalrealisasi=0;
                                        for($i=1; $i<=12; $i++){
                                            $realisasi=DeploymentTarget::where('id_deployment',$item->id)->where('bulan',$i)->where('tahun',$tahun)->first();
                                            if($item->rumus_capaian==3){
                                                $akumulasirealisasi+=0;
                                            }else{
                                                $akumulasirealisasi+=target($item['rumus_capaian'],$realisasi['realisasi']);
                                            }
                                            if($realisasi['realisasi']==0){
                                                $totalrealisasi+=0;
                                            }else{
                                                $totalrealisasi+=1;
                                            }
                                            echo'<td align="center" style="font-size:12px">'.target($item['rumus_capaian'],$realisasi['realisasi']).'</td>';
                                        }
                                    echo'
                                        <td class="isidata">';
                                            if($item->rumus_akumulasi==1){
                                                echo''.round($akumulasirealisasi).'';
                                            }elseif($item->rumus_akumulasi==2){
                                                echo''.round($akumulasirealisasi/$totalrealisasi).'';
                                            }else{
                                                echo''.progres_realisasi($item->id).'';
                                            }

                                        echo'
                                        
                                        </td>
                                        <td class="isidata" style="border-top:solid 1px #fff"></td>
                                </tr>

                                <tr>
                                    <td class="isidata" style="text-align:left">Capaian</td>';
                                    $akumulasi=0;
                                    $totalcapaian=0;
                                    for($i=1; $i<=12; $i++){
                                        $capaian=DeploymentTarget::where('id_deployment',$item->id)->where('bulan',$i)->where('tahun',$tahun)->first();
                                        $hasilcapaian[$i]+=rumus_capaian($item['rumus_capaian'],$item['bobot_tahunan'],$capaian['target'],$capaian['realisasi']);
                                        $akumulasi+=rumus_capaian($item['rumus_capaian'],$item['bobot_tahunan'],$capaian['target'],$capaian['realisasi']);
                                        if($capaian['realisasi']==0){
                                            $totalcapaian+=0;
                                        }else{
                                            $totalcapaian+=1;
                                        }
                                        echo'<td align="center" style="font-size:12px">'.rumus_capaian($item['rumus_capaian'],$item['bobot_tahunan'],$capaian['target'],$capaian['realisasi']).'%</td>';
                                    }
                                    echo'
                                    <td class="isidata">';
                                        if($item->rumus_capaian==3){
                                            if($item->rumus_akumulasi==1){
                                                $hscp=round($akumulasi);
                                            }elseif($item->rumus_akumulasi==2){
                                                $hscp=round($akumulasi/$totalcapaian);
                                            }else{
                                                $hscp=round($akumulasi);
                                            }
                                        }else{
                                            if($item->rumus_akumulasi==1){
                                                $hscp=round($akumulasirealisasi)/round($totaltargets);
                                            }elseif($item->rumus_akumulasi==2){
                                                $hscp=round($akumulasirealisasi/$totalrealisasi)/round($akumulasicapaian/$totaltarget);
                                            }else{
                                                $hscp=progres_realisasi($item->id)/progres_target($item->id);
                                            }
                                        }
                                        if($item->rumus_capaian==3){
                                            echo''.substr($hscp,0,4).'%';
                                            $subskumulasi+=substr($hscp,0,4);
                                        }else{
                                            echo''.substr(($hscp*100),0,5).'%';
                                            $subskumulasi+=substr(($hscp*100),0,5);
                                        }
                                       
                                       echo'
                                    <td class="isidata">';
                                        if($item->rumus_capaian==3){
                                            $score=substr(($hscp*$item->bobot_tahunan),0,4);
                                        }else{
                                            $score=substr((($hscp*100)*$item->bobot_tahunan),0,4);
                                        }
                                        $subscore+=($score/100);
                                    echo''.($score/100).'
                                        
                                    </td>
                                </tr>
                            ';
                        }
                        echo'
                                
                                <tr>
                                    <td colspan="6" align="left"><b>Total Capaian</b></td>';
                                    for($i=1; $i<=12; $i++){
                                        echo'<td class="isidata" width="3%" style="text-align:center;">'.hasil_capaian($hasilcapaian[$i],total_kpi($kode,$tahun,$i)).'%</td>';
                                    }
                                    echo'
                                    <td class="isidata">'.round($subskumulasi/tot_kpi($kode,$tahun,1)).'%</td>
                                    <td class="isidata">'.$subscore.'</td>
                                </tr>
                                <tr>
                                    <td colspan="6" align="left"><b>Potongan Keterlambatan</b></td>';
                                    $subpotongan=0;
                                    for($i=1; $i<=12; $i++){
                                        $subpotongan+=potongan($kode,$tahun,$i);
                                        echo'<td class="isidata" width="3%" style="text-align:center;">'.potongan($kode,$tahun,$i).'%</td>';
                                    }
                                    echo'
                                    <td class="isidata">'.$subpotongan.'%</td>
                                    <td class="isidata"></td>
                                </tr>
                                <tr>
                                    <td colspan="6" align="left"><b>Capaian Akhir</b></td>';
                                    $subcapaianakhir=0;
                                    for($i=1; $i<=12; $i++){
                                        $subcapaianakhir+=(hasil_capaian($hasilcapaian[$i],total_kpi($kode,$tahun,$i))-potongan($kode,$tahun,$i));
                                        echo'<td class="isidata" width="3%" style="text-align:center;">'.(hasil_capaian($hasilcapaian[$i],total_kpi($kode,$tahun,$i))-potongan($kode,$tahun,$i)).'%</td>';
                                    }
                                    echo'
                                    <td class="isidata">'.(round($subskumulasi/tot_kpi($kode,$tahun,1))-$subpotongan).'%</td>
                                    <td class="isidata"></td>
                                </tr>
                                <tr>
                                    <td colspan="6" align="left"><b>Tanggal Validasi Pimpinan Unit</b></td>';
                                    for($i=1; $i<=12; $i++){
                                        echo'<td class="isidata" width="3%" style="text-align:center;">'.text_tgl(validation($kode,$tahun,$i)).'d</td>';
                                    }
                                    echo'
                                    <td class="isidata"></td>
                                    <td class="isidata"></td>
                                </tr>
                               
                   
                </thead>
            </table>';
            
    }

    public function view_laporan_acc($kode=null,$tahun=null){
        
            $tahun=$tahun;
            $kode=$kode;
        
        
        if(Auth::user()->hasRole('keyperson')){
            $units = Unit::where('nik', Auth::user()->username)->get();
        }elseif(Auth::user()->hasRole('boss')){
            $units = Unit::where('nik_atasan', Auth::user()->username)->get();
        }else{
            $units = Unit::where('unit_id','!=',4)->where('unit_id','!=',6)->get();
        }

        if(Auth::user()->hasRole('keyperson') || Auth::user()->hasRole('administrator')){
            $deployments = Deployment::where('kode_unit', $kode)->where('status_id',4)->where('tahun', $tahun)->get();
            
        }else{
            $deployments = Deployment::where('kode_unit', $kode)->where('status_id',4)->where('tahun',$tahun)->get();
            
        }
        // dd(date('Y-m-d',strtotime(2020-2-1)));
        echo'
            <table border="1" width="140%">
                <thead>
                    <tr> 
                        <th class="isidatath" rowspan="2" width="4%">Kode KPI</th>
                        <th class="isidatath" rowspan="2">Nama KPI</th>
                        <th class="isidatath" rowspan="2" width="5%">Bobot</th>
                        <th class="isidatath" rowspan="2" width="5%">Target</th>
                        <th class="isidatath" rowspan="2" width="5%">Satuan</th>
                        <th class="isidatath" rowspan="2" width="5%">Ket</th>
                        <th class="isidatath" colspan="12">Bulan</th>
                        
                    </tr>
                    <tr>';
                        for($i=1; $i<=12; $i++){
                            echo'<th class="isidatath" width="3%" style="text-align:center;">'.$i.'</th>';
                        }
                    echo'
                    </tr>';
                    for($i=1; $i<=12; $i++){
                        $hasilcapaian[$i]=0;
                    }
                        $subscore=0;
                        $subskumulasi=0;
                        foreach ($deployments as $item){
                            echo'
                                <tr>
                                    <td  rowspan="3">'.$item->kode_kpi.'</td>
                                    <td rowspan="3">'.$item->kpi['kpi'].'</td>
                                    <td rowspan="3" class="isidata">'.$item->bobot_tahunan.'</td>
                                    <td rowspan="3" class="isidata">'.$item->target_tahunan.'</td>
                                    <td rowspan="3" class="isidata">'.$item->kpi['satuan'].'</td>
                                    <td class="isidata" style="text-align:left">Target</td>';
                                        $akumulasicapaian=0;
                                        $totaltarget=0;
                                        $totaltargets=0;
                                        for($i=1; $i<=12; $i++){
                                            $target=DeploymentTarget::where('id_deployment',$item->id)->where('bulan',$i)->where('tahun',$tahun)->first();
                                            if($item->rumus_capaian==3){
                                                $akumulasicapaian+=0;
                                            }else{
                                                $akumulasicapaian+=target($item['rumus_capaian'],$target['target']);
                                            }
                                            if($target['target']==0){
                                                $totaltarget+=0;
                                            }else{
                                                $totaltarget+=1;
                                            }
                                            if($item->rumus_akumulasi==1){
                                                if($target['realisasi']==0){
                                                    $totaltargets+=0;
                                                }else{
                                                    $totaltargets+=$target['target'];
                                                }
                                            }
                                            echo'<td align="center" style="font-size:12px">'.target($item['rumus_capaian'],$target['target']).'</td>';
                                        }
                                     echo'
                                    
                                </tr>

                                <tr>
                                    <td class="isidata" style="text-align:left">Realisasi</td>';
                                        $akumulasirealisasi=0;
                                        $totalrealisasi=0;
                                        for($i=1; $i<=12; $i++){
                                            $realisasi=DeploymentTarget::where('id_deployment',$item->id)->where('bulan',$i)->where('tahun',$tahun)->first();
                                            if($item->rumus_capaian==3){
                                                $akumulasirealisasi+=0;
                                            }else{
                                                $akumulasirealisasi+=target($item['rumus_capaian'],$realisasi['realisasi']);
                                            }
                                            if($realisasi['realisasi']==0){
                                                $totalrealisasi+=0;
                                            }else{
                                                $totalrealisasi+=1;
                                            }
                                            echo'<td align="center" style="font-size:12px">'.target($item['rumus_capaian'],$realisasi['realisasi']).'</td>';
                                        }
                                    echo'
                                        
                                </tr>

                                <tr>
                                    <td class="isidata" style="text-align:left">Capaian</td>';
                                    $akumulasi=0;
                                    $totalcapaian=0;
                                    for($i=1; $i<=12; $i++){
                                        $capaian=DeploymentTarget::where('id_deployment',$item->id)->where('bulan',$i)->where('tahun',$tahun)->first();
                                        $hasilcapaian[$i]+=rumus_capaian($item['rumus_capaian'],$item['bobot_tahunan'],$capaian['target'],$capaian['realisasi']);
                                        $akumulasi+=rumus_capaian($item['rumus_capaian'],$item['bobot_tahunan'],$capaian['target'],$capaian['realisasi']);
                                        if($capaian['realisasi']==0){
                                            $totalcapaian+=0;
                                        }else{
                                            $totalcapaian+=1;
                                        }
                                        echo'<td align="center" style="font-size:12px">'.rumus_capaian($item['rumus_capaian'],$item['bobot_tahunan'],$capaian['target'],$capaian['realisasi']).'%</td>';
                                    }
                                    echo'
                                    
                                </tr>
                            ';
                        }
                        echo'
                                
                                
                                <tr>
                                    <td colspan="6" align="left"><b>Total Capaian</b></td>';
                                    for($i=1; $i<=12; $i++){
                                        echo'<td class="isidata" width="3%" style="text-align:center;">'.hasil_capaian($hasilcapaian[$i],total_kpi($kode,$tahun,$i)).'%</td>';
                                    }
                                    echo'
                                 </tr>
                                 <tr>
                                    <td colspan="6" align="left"><b>Tanggal Validasi</b></td>';
                                    for($i=1; $i<=12; $i++){

                                        echo'<td class="isidata" width="3%" style="text-align:center;">';
                                            if(cek_validasi($kode,$tahun,$i)==tervalidasi($kode,$tahun,$i)){
                                                if(cek_tgl_validasi($kode,$tahun,$i)>0){
                                                    echo Tglvalidasi($kode,$i,$tahun);
                                                    
                                                }else{
                                                    echo'<span class="btn btn-danger btn-sm" onclick="validasiyah('.$i.','.$kode.','.$tahun.')"><i class="fa fa-check"></i></span>';
                                                }
                                                
                                            }else{
                                                echo'<span class="btn btn-default btn-sm" disabled>'.cek_validasi($kode,$tahun,$i).'/'.total_kpi($kode,$tahun,$i).'</span>';
                                            }
                                        echo'</td>';
                                    }
                                 echo'
                                </tr>
                               
                   
                </thead>
            </table>';
            
    }

    
}
                   