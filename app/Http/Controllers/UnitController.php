<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Unit;
use App\User;
use App\Role;
use App\Imports\UnitsImport;
use Maatwebsite\Excel\Facades\Excel;


class UnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ambil_unit(){
        $json = file_get_contents('https://portal.krakatausteel.com/eos/api/organization');
        $item = json_decode($json,true);
        
        $unit=$item;
        //var_dump($item);
        foreach($unit as $o){
            $potng=explode(' ',$o['Objectname']);
            $cekdata=Unit::where('kode',$o['ObjectID'])->count();
            
            if($potng[0]=='Subdit'){$unt=1; $pim='General Manager';}
            elseif($potng[0]=='Divisi'){$unt=3;$pim='Manager';}
            elseif($potng[0]=='Direktorat'){$unt=5;$pim='Direktur Utama';}
            elseif($potng[0]=='Dinas'){$unt=4;$pim='Supertintendant';}
            else{$unt=2;$pim='none';}

            if($cekdata>0){
                $key            =   Unit::where('kode',$o['ObjectID'])->first();
                $key->kode      =   $o['ObjectID'];
                $key->nama      =   $o['Objectname'];
                $key->kode_unit =   $o['Objectabbr'];
                $key->unit_id   =   $unt;
                $key->pimpinan  =   $pim;
                $key->save();
                
            }else{
				if($potng[0]=='PT'){
					
				}else{
					 $key            =   New Unit;
					$key->kode      =   $o['ObjectID'];
					$key->nama      =   $o['Objectname'];
					$key->kode_unit =   $o['Objectabbr'];
					$key->unit_id   =   $unt;
					$key->pimpinan  =   $pim;
					$key->save();
				}
               
            }
            
        }    
            
    }

    public function index()
    {
        //
        $units = Unit::orderBy('nama','DESC')->where('pimpinan','!=','none')->where('unit_id','!=',4)->where('unit_id','!=',6)->get();
        return view('units.index', compact('units'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function formImport($id)
    {
		$unit=Unit::where('kode',$id)->first();
        return view('units.create',compact('unit'));
    }

    public function importProcess(Request $request)
    {
       
		$data            =   Unit::where('kode',$request->kode)->first();
        $data->nik       =   $request->nik;
        $data->nik_atasan       =   $request->nik_atasan;
        $data->nama_pic  =   $request->nama_pic;
        $data->save();

        $cek=User::where('username',$request->nik)->count();
        if($cek>0){

        }else{
            $user = new User();
            $user->name = $request->nama_pic;
            $user->username = $request->nik;
            $user->password = Hash::make($request->nik);
            $user->save();
            $user->assignRole(2);

        }

        $cek=User::where('username',$request->nik_atasan)->count();
        if($cek>0){

        }else{
            $user = new User();
            $user->name = $request->nama_atasan;
            $user->username = $request->nik_atasan;
            $user->password = Hash::make($request->nik_atasan);
            $user->save();
            $user->assignRole(3);

        }
        
        return redirect('/unit');
    }
}
