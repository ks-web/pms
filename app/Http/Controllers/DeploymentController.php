<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Deployment;
use App\Unit;
use App\Accumulation;
use App\Rumuscapaian;
use App\DeploymentHistory;
use App\Imports\DeploymentsImport; 
use App\Imports\DeploymentTransaction; 
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class DeploymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       
        if(Auth::user()->hasRole('administrator'))
        {
            if($request->input('unit') == 'all' || $request->input('unit') == null)
            {
                $deployments = Deployment::all();
                $units = Unit::where('pimpinan','!=','none')->orderBy('nama','asc')->get();
            }
            else
            {
                $deployments = Deployment::where('kode_unit', $request->input('unit'))
                    ->where('tahun', $request->input('tahun'))
                    ->get();
                $units = Unit::all();
            }
            
        }
        return view('deployments.index', compact('deployments','units'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $deployment = Deployment::where('id', $id)
                ->first();
        $capaian = Rumuscapaian::all();
        $accumulations = Accumulation::all();
        return view('deployments.edit',compact('capaian','deployment','accumulations'));
    }

    public function carikodeunit()
    {
        $unit = Unit::where('kode', $_GET['kode'])
                ->first();
        $jum = Unit::where('kode', $_GET['kode'])
                ->count();
        if($jum>0){
            echo $unit->nama ;
        }else{
            echo 'Tidak Terdaftar' ;
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        

        // $deploy = Deployment::where('id',$request->input('id_deployment'))->update(
        //     [
        //         'kode_unit' => $request->input('kode_unit'),
        //         'rumus_akumulasi' => $request->input('rumus_akumulasi'),
        //         'rumus_capaian' => $request->input('rumus_capaian'),
        //         'kode_unit_tingkat' => $request->input('kode_unit_tingkat'),
        //         'bobot_tahunan' => $request->input('bobot_tahunan'),
        //         'target_tahunan' => $request->input('target_tahunan'),
        //     ]
        // );
                $deploy                  = Deployment::where('id',$request->id_deployment)->first();
                $deploy->kode_unit       = $request->kode_unit;
                $deploy->rumus_akumulasi = $request->rumus_akumulasi;
                $deploy->rumus_capaian   = $request->rumus_capaian;
                $deploy->kode_unit_tingkat = $request->kode_unit_tingkat;
                $deploy->bobot_tahunan   = $request->bobot_tahunan;
                $deploy->target_tahunan  = $request->target_tahunan;
                $deploy->save();

                $deployhis                  = new DeploymentHistory;
                $deployhis->kode_unit       = $request->kode_unit;
                $deployhis->rumus_akumulasi = $request->rumus_akumulasi;
                $deployhis->rumus_capaian   = $request->rumus_capaian;
                $deployhis->kode_kpi        = $request->kode_kpi;
                $deployhis->tahun           = $request->tahun;
                $deployhis->kode_unit_tingkat = $request->kode_unit_tingkat;
                $deployhis->bobot_tahunan   = $request->bobot_tahunan;
                $deployhis->target_tahunan  = $request->target_tahunan;
                $deployhis->save();
       


        return redirect('/deployment/');
      
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function remove($id) {
        $deploy = Deployment::where('id',$id)->delete();
        $deploytarget = DeploymentTransaction::where('id_deployment',$id)->delete();


        return redirect('/deployment/');
    }

    public function formTarget() {
        
    }

    public function formImport()
    {
        return view('deployments.create');
    }

    public function importProcess(Request $request)
    {
       
		// menangkap file excel
        $filess = $request->file('files');
        
        //dd($file);
 
		// membuat nama file unik
		//$nama_file = rand().$file->getClientOriginalName();
 
		// upload ke folder file_siswa di dalam folder public
		//$file->move('file_import',$nama_file);
 
		// import data
		Excel::import(new DeploymentsImport,$filess);
        return redirect()
            ->route('deployment.index')
            ->with('success', 'Data berhasil di upload.');
    }
}
