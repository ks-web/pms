<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Accumulation extends Model
{
    protected $table = 'accumulations';
    public $timestamps = false;

    public function kpi() 
    {
        return $this->hasMany('App\Kpi', 'id', 'rumus_akumulasi');
    }

    public function deployment() 
    {
        return $this->hasMany('App\Deployment', 'id', 'rumus_akumulasi');
    }
}
