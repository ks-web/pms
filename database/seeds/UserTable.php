<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;

class UserTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // role administrator mendapat semua permission
        $adminisRole = Role::create(['name' => 'administrator']);
        $keypersonRole = Role::create(['name' => 'keyperson']);
        $bossRole = Role::create(['name' => 'boss']);

        $user = new User;
        $user->name = 'administrator';
        $user->username = 'administrator';
        $user->email = 'administrator@gmail.com';
        $user->email_verified_at = date('Y-m-d H:i:s');
        $user->password = Hash::make('1');
        $user->save();
        $user->assignRole($adminisRole);// tandai sebagai admin

        $user = new User;
        $user->name = 'mira';
        $user->username = 'mira';
        $user->email = 'mira@krakatausteel.com';
        $user->email_verified_at = date('Y-m-d H:i:s');
        $user->password = Hash::make('1');
        $user->save();
        $user->assignRole($adminisRole); // tandai sebagai admin

        $user = new User;
        $user->name = 'abdulh muslim';
        $user->username = 'abdulh.muslim';
        $user->email = 'abdulh.muslim@krakatausteel.com';
        $user->email_verified_at = date('Y-m-d H:i:s');
        $user->password = Hash::make('1');
        $user->save();
        $user->assignRole($keypersonRole); // tandai sebagai keyperson

        $user = new User;
        $user->name = 'helmi widiyarto';
        $user->username = 'helmi.widiyarto';
        $user->email = 'helmi.widiyarto@krakatausteel.com';
        $user->email_verified_at = date('Y-m-d H:i:s');
        $user->password = Hash::make('1');
        $user->save();
        $user->assignRole($bossRole); //tandai sebagai admin


    }
}
