<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTargetRealisasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deployment_realizations', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('id_deployment');
            $table->integer('id_deployment_target');
            $table->string('tahun');
            $table->string('bulan');
            $table->integer('target');
            $table->timestamps();
        });

        Schema::create('accumulations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deployment_realizations');
        Schema::dropIfExists('accumulations');
    }
}
