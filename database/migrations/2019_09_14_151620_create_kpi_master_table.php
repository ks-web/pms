<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKpiMasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('units', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode_unit');
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('kpis', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode_kpi');
            $table->string('kpi');
            $table->string('periode');
            $table->string('satuan');
            $table->integer('rumus_capaian');
            $table->integer('rumus_akumulasi');
            $table->string('unit_tidak_edit_nilai');
            $table->integer('max_capaian');
            $table->string('capaian_table');
            $table->string('edit_target_bulan');
            $table->string('tahun');
            $table->string('masih_dipakai');
            $table->string('kode_catalog');
            $table->bigInteger('target');
            $table->string('kode_unix');
            $table->timestamps();
        });

        
        Schema::create('deployments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode_unit');
            $table->string('kode_kpi');
            $table->integer('target_tahunan');
            $table->integer('bobot_tahunan');
            $table->string('tahun');
            $table->string('id_kpi_unix');
            $table->timestamps();
        });

        Schema::create('deployment_targets', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('id_deployment');
            $table->string('tahun');
            $table->string('bulan');
            $table->integer('target');
            $table->timestamps();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kpi');
        Schema::dropIfExists('unit');
        Schema::dropIfExists('deployments');
        Schema::dropIfExists('deployment_transactions');
    }
}
