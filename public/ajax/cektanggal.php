<?php

if(is_null($_GET['sampai']) || $_GET['sampai']==0 || $_GET['mulai']==0){
    $cap=100;
}else{
    $tanggal1 = date_create(date('Y-m-d h:i:s',strtotime($_GET['mulai'])));
    $tanggal2 = date_create(date('Y-m-d h:i:s',strtotime($_GET['sampai'])));
    $perbedaan = $tanggal1->diff($tanggal2)->format("%d");
    if($_GET['mulai']>$_GET['sampai']){
       
        $cap=(1+(-$perbedaan/30))*100;
    }else{
        $cap=(1+($perbedaan/30))*100;
    }
}

if($cap>120){
    $nicap=120;
}else{
    $nicap=$cap;
}
echo round($nicap);
