@extends('core-ui.layouts.app')

@push('style')
<link href="{{ asset('vendors/DataTables/datatables.min.css') }}" rel="stylesheet">
@endpush

@push('script')
<script src="{{ asset('vendors/DataTables/datatables.min.js') }}"></script> 
<script>
$(document).ready(function() {
    $('#table').DataTable({
        responsive: true,
    });

});
</script>
@endpush

@include('core-ui.layouts._layout')

@section('content')
<div class="row justify-content-center">
    <div class="col m-3">
        <div class="card" style="width: 100%;">
            <div class="card-header">
                <a href="{{route('deployment.import')}}" class="btn btn-primary">Upload Data</a>
            </div>
        </div>
        <div class="card" style="width: 100%;">
            <div class="card-header">
                <form class="form-inline" method="get" action="{{ route('deployment.index', 'all') }}">
                    @csrf
                    <div class="form-group mb-2">
                        <input required type="text" name="tahun" id="tahun" placeholder="tahun" value="{{old('tahun')}}" class="form-control" />
                    </div>
                    <div class="form-group mb-2">
                        <select required id="unit" name="unit" class="form-control" placeholder="nama unit">
                            <option value="">Pilih nama unit</option>
                            @foreach (unit() as $unit)
                                <option value="{{ $unit->kode }}">{{ $unit->nama }}</option>
                            @endforeach
                            <option value="all">All Data</option>
                        </select>
                    </div>
                    <button class="btn mx-sm-3 btn-primary mb-2">
                        <i class="fa fa-search"></i>
                    </button>
                </form>
            </div>
            <div class="card-header">
                <b>Master Data Deployment</b>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <div class="table-responsive" style="width:100%;overflow-x:scroll">
                    <table class="table table-hover responsive no-wrap table-striped" id="table" width="130%">
                        <thead>
                            <tr> 
                                <th width="10%">Level</th>
                                <th width="25%">Kode Unit</th>
                                <th width="25%">Kpi</th>
                                <th>Target Tahunan</th>
                                <th>Bobot Tahunan</th>
                                <th>Tahun</th>
                                <th width="20%">Type Capaian</th>
                                <th>Akumulasi</th>
                                <th width="15%">Last Update</th>
                                <th width="10%">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($deployments as $item)
                                <tr>
                                    <td>{{$item->levelnya['name']}}</td>
                                    <td>
                                        <b>[{{$item->kode_unit }}]</b> {{$item->unit['nama'] }}
                                    </td>
                                    <td>
                                        <b>[{{$item->kode_kpi }}]</b> {{$item->kpi['kpi']}}
                                    </td>
                                    <td>{{$item->target_tahunan}}</td>
                                    <td>{{$item->bobot_tahunan}}</td>
                                    <td>{{$item->tahun}}</td>
                                    <td>{{$item->rumuscapaian['ket_capaian']}}</td>
                                    <td>{{$item->rumusakumulasi['name']}}</td>
                                    <td>{{$item->updated_at}}</td>
                                    <td>
                                        <a href="{{ url('/deployment/edit/'.$item->id)}}">
                                            <button class="btn btn-success btn-sm ml-1">
                                                <i class="fa fa-edit"></i>
                                            </button>
                                        </a>
                                        <a href="{{ url('/deployment/delete/'.$item->id)}}" onclick="return confirm('Hapus data ini?')">
                                            <button class="btn btn-danger btn-sm ml-1">
                                                <i class="fa fa-remove"></i>
                                            </button>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    th{font-size:12px;}
    td{font-size:12px;}
</style>
@endsection