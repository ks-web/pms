@extends('core-ui.layouts.app')

@push('style')
<link href="{{ asset('vendors/datetimepicker/jquery.datetimepicker.min.css') }}" rel="stylesheet">
<link href="{{ asset('vendors/select2/css/select2.min.css') }}" rel="stylesheet" />
<link href="{{ asset('vendors/select2/css/select2-bootstrap4.min.css') }}" rel="stylesheet" />
<link href="{{ asset('vendors/summernote-master/summernote-bs4.css') }}" rel="stylesheet" />
<link href="{{ asset('vendors/DataTables/datatables.min.css') }}" rel="stylesheet">
@endpush

@push('script')
<script src="{{ asset('vendors/datetimepicker/jquery.datetimepicker.full.min.js') }}"></script>
<script src="{{ asset('vendors/select2/js/select2.min.js') }}"></script>
<script src="{{ asset('vendors/summernote-master/summernote-bs4.min.js') }}"></script>
<script src="{{ asset('vendors/DataTables/datatables.min.js') }}"></script> 
<script>
$(document).ready(function() {

    jQuery.datetimepicker.setLocale('id');

    jQuery('#start').datetimepicker({
        timepicker:false,
        mask:true,
        format:'d/m/Y'
    });
    jQuery('#end').datetimepicker({
        timepicker:false,
        mask:true,
        format:'d/m/Y'
    });

    $('#note').summernote({
        height: 90, 
    });


});
function submit(){
    $('#form').submit();
}


</script>
@endpush
<?php
    function bulan($x) {
        $bulan = array (1=>'Januari',2=>'Februari',3=>'Maret',4=>'April',
                5=>'Mei',6=>'Juni',7=>'Juli',8=>'Agustus',
                9=>'September',10=>'Oktober',11=>'November',12=>'Desember');
        return $bulan[$x];
    }

?>
@include('core-ui.layouts._layout')

@section('content')
<div class="row justify-content-center">
    <div class="col m-3">
        <div class="card" style="width: 100%;">
            <div class="card-header">
                Form Deployment
            </div>
            <div class="card-body">
                <form id="form" method="post" enctype="multipart/form-data" action="{{ url('/deployment/update') }}">
                    <div class="row row-form">
                        @csrf
                        <input type="hidden" name="tahun" value="{{$deployment->tahun}}">
                        <input type="hidden" name="id_deployment" value="{{$deployment->id}}">
                        <div class="col-sm-6">
                            <label for="note">Kode KPI</label>
                            <input type="text" name="kode_kpi" readonly class="form-control mb-1 border-primary" autocomplete="off" value={{ isset($deployment->kode_kpi) ? $deployment->kode_kpi : ''}}>
                        </div>

                        <div class="col-sm-6">
                            <label for="note">Nama Kpi </label>
                            <textarea class="form-control mb-1 border-primary"  autocomplete="off"> {{$deployment->kpi['kpi']}} </textarea>
                        </div>

                        <div class="col-sm-6">
                            <label for="note">Kode Unit</label><br>
                            <input type="text" onkeyup="carikode()" id="kode_unit" name="kode_unit" style="width:18%;display:inline" class="form-control mb-1 border-primary" autocomplete="off" value="{{$deployment->kode_unit}}">
                            <input type="text"  id="nama_unit" readonly style="width:80%;display:inline" class="form-control mb-1 border-primary" autocomplete="off" value="{{$deployment->unit['nama']}}">
                        </div>
                        <div class="col-sm-6">
                            <label for="note">Unit Atasan</label><br>
                            <input type="text" onkeyup="carikodeatasan()" name="kode_unit_tingkat" id="kode_unit_tingkat" style="width:18%;display:inline" class="form-control mb-1 border-primary" autocomplete="off" value="{{$deployment->kode_unit_tingkat}}">
                            <input type="text"  readonly id="nama_unit_tingkat" style="width:80%;display:inline" class="form-control mb-1 border-primary" autocomplete="off" value="{{$deployment->unitatasan['nama']}}">
                            
                        </div>
                        <div class="col-sm-6">
                            <label for="note">Target Tahunan</label>
                            <input type="text" name="target_tahunan"  class="form-control mb-1 border-primary" autocomplete="off" value="{{$deployment->target_tahunan}}">
                        </div>

                        <div class="col-sm-6">
                            <label for="note">Rumus Akumulasi</label>
                             <select class="form-control border-primary" name="rumus_akumulasi" id="rumus_akumulasi" >
                                @foreach ($accumulations as $accumulation)
                                   
                                        <option value="{{ $accumulation->id }}"   @if($deployment->rumus_akumulasi==$accumulation->id) selected @endif>{{ $accumulation->name }}</option>
                                   
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-6">
                            <label for="note">Rumus Capaian</label>
                                <select class="form-control border-primary" name="rumus_capaian" id="rumus_capaian" >
                                @foreach ($capaian as $capaian)
                                    
                                        <option value="{{ $capaian->id }}"   @if($deployment->rumus_capaian==$capaian->id) selected @endif>{{ $capaian->ket_capaian }}</option>
                                    
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-6">
                            <label for="note">Bobot Tahunan</label>
                            <input type="text" name="bobot_tahunan"  class="form-control mb-1 border-primary" autocomplete="off" value="{{$deployment->bobot_tahunan}}">
                        </div>
                        
                        
                    </div>
                </form>
                <div class="float-left mr-3 mt-2">
                    <button class="btn btn-primary btn-sm" type="button" onclick="submit()">
                        <i class="fa fa-save"></i>
                        Simpan
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
   function carikode(){
       var kode=$('#kode_unit').val();
        $.ajax({
            type: 'GET',
            url: "{{url('deployment/carikodeunit/')}}",
            data: 'kode='+kode,
            success: function(msg){
               
                    $('#nama_unit').val(msg);
                    
               
            }
        });
   }

   function carikodeatasan(){
       var kode=$('#kode_unit_tingkat').val();
        $.ajax({
            type: 'GET',
            url: "{{url('deployment/carikodeunit/')}}",
            data: 'kode='+kode,
            success: function(msg){
                $('#nama_unit_tingkat').val(msg);
            }
        });
   }
</script>
<style>
    .border-primary {
        border-color: #e9f0f3 !important;
    }
</style>

@endsection
