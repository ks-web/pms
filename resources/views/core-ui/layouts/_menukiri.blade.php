<div class="sidebar">
    <div style="border-radius:10px;text-transform:uppercase;background:#c4c5d0;width:90%;padding:5px;height:150px;margin:10px;text-align:center;color:#000">
        <img src="{{url('img/logoo.png')}}" width="90%"><h6><u>{{substr(Auth::user()->name,0,20)}}</u><br>{{Auth::user()->username}}</h6>
    </div>
    <nav class="sidebar-nav">
        <ul class="nav">
            @if(Auth::user()->hasRole('boss') || Auth::user()->hasRole('administrator'))
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('dashboard') }}">
                        <i class="nav-icon icon-speedometer"></i> Dashboard
                    </a>
                </li>
            @else
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('dashboard?kodenyaunit=') }}">
                        <i class="nav-icon icon-speedometer"></i> Dashboard
                    </a>
                </li>
            @endif

            <li class="nav-title">Menu</li>

            @role('administrator')
            <li class="nav-item">
                <a class="nav-link" href="{{ route('unit.index') }}">
                    <i class="nav-icon icon-people"></i> User</a>
            </li>
            @endrole
            
            @role('administrator')
                <li class="nav-item nav-dropdown ">
                    <a class="nav-link nav-dropdown-toggle" href="#">
                        <i class="nav-icon icon-list"></i> Master Data</a>
                    <ul class="nav-dropdown-items" style="margin-left:10px">
                        <li class="nav-item">
                            <a href="{{ route('kpi.index') }}" class="nav-link" href="base/breadcrumb.html">
                            <i class="nav-icon icon-minus"></i> Kpi </a>
                        </li>
                        {{-- <li class="nav-item">
                            <a href="{{ route('unit.index') }}" class="nav-link" href="base/breadcrumb.html">
                            <i class="nav-icon icon-circle"></i> Unit </a>
                        </li> --}}
                        <li class="nav-item">
                            <a href="{{ route('deployment.index') }}" class="nav-link" href="base/breadcrumb.html">
                            <i class="nav-icon icon-minus"></i> Deployment </a>
                        </li>
                    </ul>
                </li>
            @endrole
            <li class="nav-item nav-dropdown ">
                <a class="nav-link nav-dropdown-toggle" href="#">
                    <i class="nav-icon icon-list"></i>Transaksi</a>
                <ul class="nav-dropdown-items" style="margin-left:10px">
                    <li class="nav-item">
                        <a href="{{ route('deployment.target.index') }}" class="nav-link" href="base/breadcrumb.html">
                        <i class="nav-icon icon-minus"></i> Target bulanan </a>
                    </li>
                    @role('boss')
                        <li class="nav-item">
                            <a href="{{ url('/capaian/deployment/validasi') }}" class="nav-link" href="base/breadcrumb.html">
                            <i class="nav-icon icon-minus"></i> Realisasi per bulan </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('capaian/deployment/laporan/') }}" class="nav-link" href="base/breadcrumb.html">
                            <i class="nav-icon icon-minus"></i>Laporan Capaian </a>
                        </li>

                    @endrole

                    @role('keyperson|adminkpi')
                        <li class="nav-item">
                            <a href="{{ route('deployment.realisasi.index') }}" class="nav-link" href="base/breadcrumb.html">
                            <i class="nav-icon icon-minus"></i> Realisasi per bulan </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('capaian/deployment/laporan/') }}" class="nav-link" href="base/breadcrumb.html">
                            <i class="nav-icon icon-minus"></i>Laporan Capaian </a>
                        </li>
                    @endrole

                    @role('administrator')
                        <li class="nav-item">
                            <a href="{{ route('deployment.realisasi.index') }}" class="nav-link" href="base/breadcrumb.html">
                            <i class="nav-icon icon-minus"></i> Realisasi per bulan </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('capaian/deployment/laporan/') }}" class="nav-link" href="base/breadcrumb.html">
                            <i class="nav-icon icon-minus"></i>Laporan Capaian </a>
                        </li>
                    @endrole
                    
                </ul>
            </li>
            {{-- <li class="nav-item">
                <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="nav-icon fa fa-sign-out"></i> Logout </a>
                
            </a> --}}
            </li>
        </ul>
    </nav>

    {{-- <button class="sidebar-minimizer brand-minimizer" type="button"></button> --}}
</div>