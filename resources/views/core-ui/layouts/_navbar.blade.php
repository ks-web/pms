<header class="app-header navbar mb-0">
    <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="#">
        <img class="navbar-brand-full" src="img/krakatausteel-logo-text.png"  height="35" alt="CoreUI Logo">
        <img class="navbar-brand-minimized" src="img/krakatausteel-logo.png" width="30" height="30" alt="CoreUI Logo">
    </a>
    <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="col d-md-down-none">
        {{-- <h3 class="text-uppercase">{{ config('app.name') }}</h3> --}}
        <h3 class="text-uppercase">Performance Management System</h3>
    </div>
    
    {{-- <ul class="nav navbar-nav d-md-down-none">
        <li class="nav-item px-3">
            <a class="nav-link" href="#">Dashboard</a>
        </li>
        <li class="nav-item px-3">
            <a class="nav-link" href="#">Users</a>
        </li>
        <li class="nav-item px-3">
            <a class="nav-link" href="#">Settings</a>
        </li>
    </ul> --}}


    <ul class="nav navbar-nav d-md-down-none">
       
            <li class="nav-item px-3">
                <a download href="{{ url('/storage/images/001.pdf') }}"  data-toggle="tooltip" data-placement="bottom" title="Panduan penggunaan aplikasi">
                    {{-- <i class="ace-icon fa fa-question-circle fa-2x"></i> --}}
                    <i><img src="{{ url('/storage/images/01.png') }}" style="width:50;height:50px;">Buku Panduan</i>
                </a>
            </li>
             <li class="nav-item px-3">                
                <a href="{{ route('logout') }}"
                    onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                    <i class="fa fa-lock"></i>
                    {{ __('Logout') }}&nbsp;&nbsp;
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            
        </li>
    </ul>


    {{-- <ul class="nav navbar-nav ml-auto">
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <object data="https://portal.krakatausteel.com/eos/storage/pic/{{ Auth::user()->id }}.jpg" class="rounded" type="image/png" width="25px">
                    <img class="img-avatar" src="img/avatars/7.jpg" alt="Stack Overflow logo and icons and such">
                </object>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
                <div class="dropdown-header text-center text-uppercase">
                    <strong>{{ Auth::user()->name }}</strong>
                </div>
                <object data="https://portal.krakatausteel.com/eos/storage/pic/{{ Auth::user()->id }}.jpg" class="mx-auto d-block img-thumbnail my-2" type="image/png" width="100px">
                    <img class="rounded" src="img/avatars/7.jpg" alt="-" width="100%">
                </object>
                
                <a class="dropdown-item" href="{{ route('logout') }}"
                    onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                    <i class="fa fa-lock"></i>
                    {{ __('Logout') }}
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </li>
    </ul> --}}

    {{-- <button class="navbar-toggler aside-menu-toggler d-md-down-none" type="button" data-toggle="aside-menu-lg-show">
        <span class="navbar-toggler-icon"></span>
    </button>
    <button class="navbar-toggler aside-menu-toggler d-lg-none" type="button" data-toggle="aside-menu-show">
        <span class="navbar-toggler-icon"></span>
    </button> --}}
</header>