@extends('core-ui.layouts.app')

@push('style')
<link href="{{ asset('vendors/DataTables/datatables.min.css') }}" rel="stylesheet">
@endpush

@push('script')
<script src="{{ asset('vendors/DataTables/datatables.min.js') }}"></script> 
<script>
    $(document).ready(function() {
        $('#table').DataTable({
            responsive: true,
        });

        $('tbody').on('click','.modal-valid',function(e){
            var token = $("meta[name='csrf-token']").attr("content");
            var url = $(event.target).data('url');
            $.ajax({ 
                url: url,
                type: "GET",
                data: {"_token": token, },
                success: function (data, textStatus, jqXHR) {
                    $("#modalresponse").html(data.html);
                    $('#exampleModal').modal('show');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert("AJAX error: " + textStatus + ' : ' + errorThrown);
                },
            });
        });
    });
</script>
@endpush

@include('core-ui.layouts._layout')

@section('content')
<div class="row justify-content-center">
    <div class="col m-3">
        @if (session('success'))
        <div class="alert alert-success" role="alert">
            {{ session('success') }}
            </div>
        @endif
        <div class="card" style="width: 100%;">
            
                <div class="card-header">
                    @role('administrator')
                        <form class="form-inline" method="get" action="{{ route('deployment.target.index') }}">
                            @csrf
                            <div class="form-group mb-2">
                                <input type="text" name="tahun" id="tahun" placeholder="tahun" value="{{old('tahun')}}" class="form-control" />
                            </div>
                            <div class="form-group mb-2">
                                <select id="unit" name="unit" class="form-control" placeholder="nama unit">
                                    <option value="">Pilih nama unit</option>
                                    @foreach (unit() as $unit)
                                        <option value="{{ $unit->kode }}">{{ ucwords(strtolower($unit->nama)) }}</option>
                                    @endforeach
                                    @role('administrator')
                                        <option value="all">All Data</option>
                                    @endrole
                                </select>
                            </div>
                            <button class="btn mx-sm-3 btn-primary mb-2">
                                <i class="fa fa-search"></i>
                            </button>
                        </form>
                    @endrole
                    @role('adminkpi')
                        <form class="form-inline" method="get" action="{{ route('deployment.target.index') }}">
                            @csrf
                            <div class="form-group mb-2">
                                <input type="text" name="tahun" id="tahun" placeholder="tahun" value="{{old('tahun')}}" class="form-control" />
                            </div>
                           
                            <button class="btn mx-sm-3 btn-primary mb-2">
                                <i class="fa fa-search"></i>
                            </button>
                        </form>
                    @endrole

                    @role('keyperson|boss')
                        <form class="form-inline" method="get" action="{{ route('deployment.target.index') }}">
                            @csrf
                            <div class="form-group mb-2">
                                <input type="text" name="tahun" id="tahun" placeholder="tahun" value="{{old('tahun')}}" class="form-control" />
                            </div>
                            <div class="form-group mb-2">
                                <select id="unit" name="unit" class="form-control" placeholder="nama unit">
                                    <option value="">Pilih nama unit</option>
                                    @foreach ($units as $unit)
                                        <option value="{{ $unit->kode }}">{{ ucwords(strtolower($unit->nama)) }}</option>
                                    @endforeach
                                    @role('administrator')
                                        <option value="all">All Data</option>
                                    @endrole
                                </select>
                            </div>
                            <button class="btn mx-sm-3 btn-primary mb-2">
                                <i class="fa fa-search"></i>
                            </button>
                        </form>
                    @endrole
                </div>
            
            <div class="card-header" style="text-transform:uppercase">
              <b>Data Target "{{$namaunit}}"</b><hr>
              @role('administrator')
                {!!$tombol!!}
            @endrole
            </div>
            <div class="card-body">

                <div class="table-responsive" style="width:100%;overflow-x:scroll;min-height:400px">
                    <table class="table table-hover responsive no-wrap table-striped" id="table" width="120%">
                        <thead>
                            <tr> 
                                <th width="5%">Aksi</th>
                                @role('administrator')
                                <th width="30%">Unit Kerja</th>
                                @endrole
                                <th width="40%">Nama Kpi</th>
                                <th width="15%">Target</th>
                                <th width="10%">Bobot</th>
                                <th>Akumulasi</th>
                                <th>Tahun</th>
                                <th>Status</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($deployments as $item)
                                <tr>
                                    <td> 
                                        @include('deployment_targets._action')
                                    </td>
                                    @role('administrator')
                                    <td>
                                        [<b>{{$item->kode_unit }}</b>] <br>{{$item->unit['nama']}} 
                                    </td>
                                    @endrole
                                    <td>
                                        <b>[{{$item->kode_kpi}}]</b> {{$item->kpi['kpi']}}
                                    </td>
                                    <td>{{$item->target_tahunan}}</td>
                                    <td>{{$item->bobot_tahunan}}</td>
                                    <td>{{$item->rumusakumulasi['name']}}</td>
                                    <td>{{$item->tahun}}</td>
                                    <td><span class="badge badge-primary">{{$item->status->name}}</span></td>
                                    
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- modal --}}
<div id="modalresponse" style="margin-top:2rem">
</div>
{{--  end modal --}}
<style>
    td{font-size:12px;}
</style>
<script>
    function allvalidasi(tahun){
        $.ajax({
            type: 'GET',
            url: "{{url('target/deployment/allvalidasitarget')}}",
            data: 'tahun='+tahun,
            success: function(msg){
                alert('Validasi Telah Selesai');   
                document.location.reload();
            }
        });
    }
    function onevalidasi(tahun,kode){
        $.ajax({
            type: 'GET',
            url: "{{url('target/deployment/onevalidasitarget')}}",
            data: 'tahun='+tahun+'&kode='+kode,
            success: function(msg){
                alert('Validasi Telah Selesai');   
                document.location.reload();
            }
        });
       
    }
</script>
@endsection
