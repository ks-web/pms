@role('keyperson')
    @if($item->status_id == 1 || $item->status_id == 2 || $item->status_id == 3)
        <a href="{{ route('deployment.target.create', $item->id) }}" data-toggle="tooltip" data-placement="bottom" title="Input target">
            <button class="btn btn-success btn-sm ml-1">
                <i class="fa fa-plus"></i>
            </button>
        </a>
    @endif

    {{-- @if($item->status_id == 3)
        <button class="btn btn-success btn-sm ml-1" data-toggle="tooltip" data-placement="bottom" title="Telah Validasi">
            <i class="fa fa-check"></i>
        </button>
    @else   
		<a href="{{ route('deployment.target.create', $item->id) }}">
            <button class="btn btn-outline-primary btn-sm ml-1">
                <i class="fa fa-gear"></i>
            </button>
        </a>
        <button class="btn btn-outline-danger btn-sm ml-1" data-toggle="tooltip" data-placement="bottom" title="Menunggu Validasi">
            <i class="fa fa-refresh"></i>
        </button>
    @endif --}}
@endrole

@role('boss')
    @if($item->status_id == 2)
        <a href="{{ route('deployment.target.validasitarget', $item->id) }}">
            <button class="btn btn-outline-primary btn-sm ml-1 modal-valid"  title="Validasi">
                <i class="fa fa-edit" ></i>
            </button>
        </a>
    @endif
@endrole

@role('administrator')
    @if($item->status_id == 3 || $item->status_id == 4)
        <a href="{{ route('deployment.target.validasitarget', $item->id) }}">
            <button class="btn btn-outline-primary btn-sm ml-1 modal-valid"  title="Validasi">
                <i class="fa fa-edit" ></i>
            </button>
        </a>
    @endif
    
@endrole

{{-- all role --}}
{{-- <a href="{{ route('deployment.target.validasitarget', $item->id) }}">
    <button class="btn btn-outline-primary btn-sm ml-1">
        <i class="fa fa-search"></i>
    </button>
</a> --}}