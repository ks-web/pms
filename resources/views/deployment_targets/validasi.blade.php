@extends('core-ui.layouts.app')

@push('style')
<link href="{{ asset('vendors/datetimepicker/jquery.datetimepicker.min.css') }}" rel="stylesheet">
<link href="{{ asset('vendors/select2/css/select2.min.css') }}" rel="stylesheet" />
<link href="{{ asset('vendors/select2/css/select2-bootstrap4.min.css') }}" rel="stylesheet" />
<link href="{{ asset('vendors/summernote-master/summernote-bs4.css') }}" rel="stylesheet" />
<link href="{{ asset('vendors/DataTables/datatables.min.css') }}" rel="stylesheet">
@endpush

@push('script')
<script src="{{ asset('vendors/datetimepicker/jquery.datetimepicker.full.min.js') }}"></script>
<script src="{{ asset('vendors/select2/js/select2.min.js') }}"></script>
<script src="{{ asset('vendors/summernote-master/summernote-bs4.min.js') }}"></script>
<script src="{{ asset('vendors/DataTables/datatables.min.js') }}"></script> 
<script>
$(document).ready(function() {

    jQuery.datetimepicker.setLocale('id');

    jQuery('#start').datetimepicker({
        timepicker:false,
        mask:true,
        format:'d/m/Y'
    });
    jQuery('#end').datetimepicker({
        timepicker:false,
        mask:true,
        format:'d/m/Y'
    });

    $('#note').summernote({
        height: 90, 
    });


});
function submit(){
    $('#form').submit();
}


</script>
@endpush
<?php
    function bulan($x) {
        $bulan = array (1=>'Januari',2=>'Februari',3=>'Maret',4=>'April',
                5=>'Mei',6=>'Juni',7=>'Juli',8=>'Agustus',
                9=>'September',10=>'Oktober',11=>'November',12=>'Desember');
        return $bulan[$x];
    }

?>
@include('core-ui.layouts._layout')

@section('content')
<div class="row justify-content-center">
    <div class="col m-3">
        <div class="card" style="width: 100%;">
            <div class="card-header">
                Form Target
            </div>
            <div class="card-body">
                <form id="form" method="post" enctype="multipart/form-data" action="@if($deployment->status_id==4) {{ route('deployment.realisasi.store') }} @else {{ route('deployment.store') }} @endif ">
                    <div class="row row-form">
                        @csrf
                        <input type="hidden" name="tahun" value="{{$deployment->tahun}}">
                        <input type="hidden" name="id_deployment" value="{{$deployment->id}}">
                        <input type="hidden" name="status_id" value="@if($deployment->status_id==1) 2 @elseif($deployment->status_id==2) 3 @else 4 @endif">
                        <div class="col-sm-6">
                            <label for="note">Kode KPI</label>
                            <input type="text" name="kode_kpi" readonly class="form-control mb-1 border-primary" autocomplete="off" value={{ isset($deployment->kode_kpi) ? $deployment->kode_kpi : ''}}>
                        </div>

                        <div class="col-sm-6">
                            <label for="note">Nama Kpi </label>
                            <textarea class="form-control mb-1 border-primary" readonly autocomplete="off"> {{$deployment->kpi['kpi']}} </textarea>
                        </div>

                        <div class="col-sm-6">
                            <label for="note">Kode Unit</label>
                            <input type="text" name="kode_unit" readonly class="form-control mb-1 border-primary" autocomplete="off" value="{{$deployment->kode_unit}}">
                        </div>

                        <div class="col-sm-6">
                            <label for="note">Target Tahunan</label>
                            <input type="text" name="target_tahunan"  id="target_tahunan" readonly class="form-control mb-1 border-primary" autocomplete="off" value="{{$deployment->target_tahunan}}">
                        </div>

                        <div class="col-sm-6">
                            <label for="note">Bobot Tahunan</label>
                            <input type="text" name="target_tahunan" readonly class="form-control mb-1 border-primary" autocomplete="off" value="{{$deployment->bobot_tahunan}}">
                        </div>
						
						<div class="col-sm-6">
                            <label for="note">Satuan</label>
                            <input type="text" name="satuan" readonly class="form-control mb-1 border-primary" autocomplete="off" value="{{$deployment->kpi['satuan']}}">
                        </div>

                        <div class="col-sm-6">
                            <label for="note">Rumus Capaian</label>
                            <input type="text"  readonly class="form-control mb-1 border-primary" autocomplete="off"  value="{{$deployment->rumuscapaian['ket_capaian']}}">
                            <input type="hidden" name="rumus_capaian" readonly class="form-control mb-1 border-primary" autocomplete="off" id="rumus_capaian" value="{{$deployment->rumus_capaian}}">
                        </div>

                        <div class="col-sm-6">
                            <label for="rumus_akumulasi">Rumus Akumulasi: {{$deployment->rumus_akumulasi}}</label>
                            @role('administrator')   
                                @if($deployment->status_id==3 || $deployment->status_id==4 )
                                    <select class="form-control border-primary" required onchange="rumus()" name="rumus_akumulasi" id="rumus_akumulasi" >
                                        @foreach ($accumulations as $accumulation)
                                            @if($deployment->rumus_akumulasi==$accumulation->id)
                                                <option value="{{ $accumulation->id }}"  >{{ $accumulation->name }}</option>
                                            @endif
                                            
                                        @endforeach
                                    </select>
                                @else
                                    <select class="form-control border-primary" required onchange="rumus()" name="rumus_akumulasi_ok" id="rumus_akumulasi"   >
                                        @foreach ($accumulations as $accumulation)
                                            <option value="{{ $accumulation->id }}" @if($deployment->rumus_akumulasi==$accumulation->id) selected @endif   >{{ $accumulation->name }}</option>
                                        @endforeach
                                    </select>
                                @endif
                            @endrole
                            @role('keyperson')
                                <select class="form-control border-primary" required onchange="rumus()" name="rumus_akumulasi" id="rumus_akumulasi" >
                                    @foreach ($accumulations as $accumulation)
                                        @if($deployment->rumus_akumulasi==$accumulation->id)
                                            <option value="{{ $accumulation->id }}"  >{{ $accumulation->name }}</option>
                                        @endif
                                        
                                    @endforeach
                                </select>
                            @endrole

                        </div>

                        <div class="col-sm-6">
                            <label for="note">Akumulasi</label>
                            {{-- <input type="text" name="nil_akumulasi" id="rumusg_akumulasi" id="form-control mb-1 border-primary" autocomplete="off" value="{{$deployment->kpi->rumus_akumulasi}}"> --}}
                            <input type="text" readonly name="totalbulanan" id="totalbulanan" class="form-control mb-1 border-primary" autocomplete="off" value="{{$deployment->akumulasi}}">
                        </div>
                        <div class="col-sm-6">
                            <label for="note">Keterangan</label><br>
                            <span class="badge badge-primary"><b><i>{{$deployment->status->name}}</i></b></span>
                        </div>
                        <div style="width:100%;padding:10px;background:rgb(144, 159, 218) none repeat scroll 0% 0%;margin:10px;font-weight:bold"> 
                            Target Perbulan
                        </div>
						
						
							
                        @if($jumlah != 0)
                            @if($deployment->status_id==4 )
									<table width="100%" border="1" style="margin-left:10px">
									<tr bgcolor="aqua">
                                        <td  class="tht" width="3%">No</td>
                                        <td class="tht" width="13%" >Bulan</td>
                                        <td class="tht" >Target</td>
                                        <td class="tht" >Realisasi</td>
                                        <td class="tht" width="8%">Capaian</td>
                                        <td class="tht" width="5%">File</td>
                                        <td class="tht" width="15%">Act</td>
                                        <td class="tht" width="10%">Masalah</td>
                                    </tr>
									@for($a=1; $a<=12; $a++)
										@foreach($targetdeployment->where('bulan',$a) as $tar)
                                        <tr>
                                                <td  class="tht">{{$a}}</td>
                                                <td class="tdtd">{{bulan($a)}}</td>
                                                <td class="tdtd">
                                                    <input type="text" onkeypress="return hanyaAngka(event)" style="width:100%;height:23px;font-size:10px" name="bulan{{$a}}" id="bulan{{$a}}" class="form-control mb-1 border-primary" autocomplete="off" value="{{ isset($tar->target) ? $tar->target :'0' }}" onkeyup="hitung()"  @if($deployment->status_id==4) readonly @endif >
    
                                                    <input type="hidden" name="id_target{{$a}}" id="id_target{{$a}}" class="form-control mb-1 border-primary" autocomplete="off" value="{{ isset($tar->id) ? $tar->id : '' }}">
                                                
                                                </td>
                                                <td  class="tdtd">
                                                    <input type="text" readonly onkeypress="return hanyaAngka(event)" style="width:100%;height:23px;font-size:10px" name="realisasi{{$a}}" @if($deployment->status_id==1 || $deployment->status_id==2) readonly @else  @endif     id="realisasi{{$a}}" class="form-control mb-1 border-primary" autocomplete="off" value="{{ isset($tar->realisasi) ? $tar->realisasi :'0' }}"  onkeyup="hitungrealisasi()">
                                                
                                                    <input type="hidden" name="id_targetok{{$a}}" id="id_target{{$a}}" class="form-control mb-1 border-primary" autocomplete="off" value="{{ isset($tar->id) ? $tar->id : '' }}">
                                                </td>
                                                <td class="tdt">
                                                    {{$capaian2->shift()['capaian']}}%
                                                </td>
                                                <td  class="tdt">
                                                    @if(is_null($tar->file))
                                                        
                                                    @else
                                                        <a href="{{url('/storage/'.$tar->file)}}" target="_balnk"><img src="{{url('/img/download.png')}}" title="Download File" width="20px" height="20px"></a>
                                                    @endif
                                                </td>
                                                <td  class="tdt">
                                                    @if(is_null($tar->status_realisasi) )
                                                        
                                                    @elseif($tar->status_realisasi==1)
                                                        
                                                    @elseif($tar->status_realisasi==2)
                                                        {{$tar->tgl_validasi_atasan}}
                                                    @endif
                                                </td>
                                                <td  class="tdtisi">
                                                    @if($capaian3->shift()['capaian']>95 )
                                                        
                                                    @else
                                                        @if($tar->realisasi==0)

                                                        @else
                                                            <span class="badge badge-success" data-toggle="modal" data-target="#masalahnya{{$tar->id}}"><i class="fa fa-search" ></i> View Progress</span>
                                                        @endif
                                                        
                                                    @endif
                                                </td>
                                            </tr>
										@endforeach
									@endfor
									
							@else
									<table width="50%" border="1" style="margin-left:10px">
										<tr bgcolor="aqua">
											<td  class="tht" width="10%">No</td>
											<td class="tht"  >Bulan</td>
											<td class="tht" width="30%">Target</td>
										</tr>
									@for($a=1; $a<=12; $a++)
                                        @foreach($targetdeployment->where('bulan',$a) as $tar)
                                            @if($deployment->status_id==3 )
                                                <tr>
                                                    <td  class="tht">{{$a}}</td>
                                                    <td class="tdtd">{{bulan($a)}}</td>
                                                    <td class="tdt">
                                                        <input type="text" onkeypress="return hanyaAngka(event)" style="width:100%;height:23px;font-size:10px" name="bulan{{$a}}" id="bulan{{$a}}" class="form-control mb-1 border-primary" autocomplete="off" value="{{ isset($tar->target) ? $tar->target :'0' }}" onkeyup="hitung(this.value)"   readonly >

                                                        <input type="hidden" name="id_target{{$a}}" id="id_target{{$a}}" class="form-control mb-1 border-primary" autocomplete="off" value="{{ isset($tar->id) ? $tar->id : '' }}">
                                                    
                                                    </td>
                                                    
                                                </tr>
                                            @else   
                                                <tr>
                                                    <td  class="tht">{{$a}}</td>
                                                    <td class="tdtd">{{bulan($a)}}</td>
                                                    <td class="tdt">
                                                        <input type="text" onkeypress="return hanyaAngka(event)" style="width:100%;height:23px;font-size:10px" name="bulan{{$a}}" id="bulan{{$a}}" class="form-control mb-1 border-primary" autocomplete="off" value="{{ isset($tar->target) ? $tar->target :'0' }}" onkeyup="hitung(this.value)"    >

                                                        <input type="hidden" name="id_target{{$a}}" id="id_target{{$a}}" class="form-control mb-1 border-primary" autocomplete="off" value="{{ isset($tar->id) ? $tar->id : '' }}">
                                                    
                                                    </td>
                                                    
                                                </tr>
                                            @endif
										@endforeach
									@endfor
							@endif
							
							
                        @else
							<table width="50%" border="1" style="margin-left:10px">
								<tr bgcolor="aqua">
									<td  class="tht" width="10%">No</td>
									<td class="tht"  >Bulan</td>
									<td class="tht" width="30%">Target</td>
								</tr>
                            @for($a=1; $a<=12; $a++)
                                <tr>
                                        <td  class="tht">{{$a}}</td>
                                        <td class="tdtd">{{bulan($a)}}</td>
                                        <td class="tdt">
											 <input type="text" onkeypress="return hanyaAngka(event)" style="width:100%;height:23px" name="bulan{{$a}}" id="bulan{{$a}}" class="form-control mb-1 border-primary" autocomplete="off"  value="{{ isset($tar->id) ? $tar->id : '' }}" onkeyup="hitung()" >

											<input type="hidden" name="id_target{{$a}}" id="id_target{{$a}}" class="form-control mb-1 border-primary" autocomplete="off" value="{{ isset($tar->id) ? $tar->id : '' }}">
										
										</td>
                                       
                                    </tr>
                            @endfor
                        @endif
						</table>
                    </div>
                </form>
                <div class="float-left mr-3 mt-2" id="tampil">
                        @if($deployment->status_id==4 )
                            <span class="btn btn-danger btn-sm" type="button" onclick="unvalidasi({{$deployment->id}})" onclick="unvalidasi()">
                                <i class="fa fa-remove"></i>
                                UnValidasi
                            </span>
                        @elseif($deployment->status_id==3 )
                            <span class="btn btn-success btn-sm" type="button" id="simpan" onclick="submit()">
                                <i class="fa fa-check"></i>
                                Validasi
                            </span>
                            
                        @else
                            
                        @endif
                    
                </div>
            </div>
        </div>
    </div>
</div>
@for($a=1; $a<=12; $a++)
    @foreach($targetdeployment->where('bulan',$a) as $tar)
    <div id="masalahnya{{$tar->id}}" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg" style="width:90%">
        
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Masalah</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table width="100%" border="1">
                        <tr align="center" bgcolor="aqua">
                            <td >Masalah</td>
                            <td width="30%">Rencana</td>
                            <td width="15%">Progress</td>
                            <td width="15%">Tgl Lapor</td>
                        </tr>
                        @foreach($progres->where('deployment_targets_id',$tar->id) as $prog)
                        <tr>
                            <td class="tdr">{{$prog->masalah}}</td>
                            <td class="tdr">{{$prog->rencana}}</td>
                            <td class="tdr">{{$prog->progres}}%</td>
                            <td class="tdr">{{$prog->tgl_progres}}</td>
                        </tr>
                        @endforeach
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        
        </div>
    </div>
    <div id="alasannya{{$tar->id}}" class="modal fade" role="dialog">
            <div class="modal-dialog">
            
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Alasan</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>{{$tar->keterangan}}</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            
            </div>
        </div>

        <div id="masalahnya{{$tar->id}}" class="modal fade" role="dialog">
            <div class="modal-dialog">
            
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Masalah</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>{{$tar->masalah}}</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            
            </div>
        </div>


        
        
    @endforeach
@endfor
<style>
.tht{text-align:center;background:#03a9f4;font-weight:bold;}
.tdt{text-align:center;background:#fff;font-weight:bold;}
.tdtd{text-align:center;background:aqua;font-weight:bold;}
</style>
@push('plug')
<script type="text/javascript">

$(document).ready(function(e) {
    
	// var akumulasi=$('#totalbulanan').val();
	// var target=$('#target_tahunan').val();
	// var rumus=$('#rumus_akumulasi').val();
    
    // if(rumus==3){
    //     $('#simpan').css("opacity","");
    //     $("#simpan").removeAttr("disabled");
    // }else{
    //     if(akumulasi==target){
    //         $('#simpan').css("opacity","");
    //         $("#simpan").removeAttr("disabled");
    //     }else{
    //         $('#simpan').attr("disabled","disabled");
    //         $('#simpan').css("opacity",".5");
    //     }
    // }
        
});

    function unvalidasi(id){
        $.ajax({
            type: 'GET',
            url: "{{url('deployments/unvalid')}}/"+id,
            data: 'ok=ok',
            success: function(data){
                alert('Data berhasil dikembalikan kepada admin');
                window.history.go(-2);
            }
        });
    }
    function hanyaAngka(evt) {
		  var charCode = (evt.which) ? evt.which : event.keyCode
		   if (charCode > 31 && (charCode < 48 || charCode > 57))
 
		    return false;
		  return true;
	}
    function hitung(b){
        var akumulasi = $("#rumus_akumulasi").val();
        var target=$('#target_tahunan').val();
        
        var jum = 0;
        for(a=1; a<=12; a++){
            jum += eval($("#bulan"+a).val());
        }
        if(akumulasi==1){
            total = jum;
        }
        if(akumulasi==2){
           total = jum/12;
        }
        if(akumulasi==3){
            total = b;
        }
       //alert(total);
       $("#totalbulanan").val(total);
	   
	//    if(total==target){
	// 		$('#simpan').css("opacity","");
	// 		$("#simpan").removeAttr("disabled");
	// 	}else{
	// 		 $('#simpan').attr("disabled","disabled");
	// 		 $('#simpan').css("opacity",".5");
	// 	}
    }

    function rumus(){
        var akumulasi = $("#rumus_akumulasi").val();
         var target=$('#target_tahunan').val();
        var jum = 0;
		
		
        for(a=1; a<=12; a++){
            jum += eval($("#bulan"+a).val());
        }
         if(akumulasi==1){
            total = jum;
        }
        if(akumulasi==2){
           total = jum/12;
        }
        if(akumulasi==3){
            $('#simpan').css("opacity","");
			$("#simpan").removeAttr("disabled");
        }
       //alert(total);
       $("#totalbulanan").val(total);
	//    if(total==target){
	// 		$('#simpan').css("opacity","");
	// 		$("#simpan").removeAttr("disabled");
	// 	}else{
	// 		 $('#simpan').attr("disabled","disabled");
	// 		 $('#simpan').css("opacity",".5");
	// 	}
    }
</script>
@endpush

@endsection
