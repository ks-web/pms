@extends('core-ui.layouts.app')

@push('style')
<link href="{{ asset('vendors/datetimepicker/jquery.datetimepicker.min.css') }}" rel="stylesheet">
<link href="{{ asset('vendors/select2/css/select2.min.css') }}" rel="stylesheet" />
<link href="{{ asset('vendors/select2/css/select2-bootstrap4.min.css') }}" rel="stylesheet" />
<link href="{{ asset('vendors/summernote-master/summernote-bs4.css') }}" rel="stylesheet" />
<link href="{{ asset('vendors/DataTables/datatables.min.css') }}" rel="stylesheet">
@endpush

@push('script')
<script src="{{ asset('vendors/datetimepicker/jquery.datetimepicker.full.min.js') }}"></script>
<script src="{{ asset('vendors/select2/js/select2.min.js') }}"></script>
<script src="{{ asset('vendors/summernote-master/summernote-bs4.min.js') }}"></script>
<script src="{{ asset('vendors/DataTables/datatables.min.js') }}"></script> 
<script>
$(document).ready(function() {

    jQuery.datetimepicker.setLocale('id');

    jQuery('.start').datetimepicker({
        timepicker:false,
        mask:true,
        format:'Y-m-d'
    });
    @for($x=1; $x<=12; $x++)
        jQuery('#datepicker{{$x}}').datetimepicker({
            format:'Y-m-d'
        });
        jQuery('.datepickert{{$x}}').datetimepicker({
            format:'Y-m-d'
        });
    @endfor
    jQuery('#end').datetimepicker({
        timepicker:false,
        mask:true,
        format:'d/m/Y'
    });

    $('#note').summernote({
        height: 90, 
    });


});
function submit(){
    $('#form').submit();
}


</script>
@endpush
<?php
    
    function bulan($x) {
        $bulan = array (1=>'Januari',2=>'Februari',3=>'Maret',4=>'April',
                5=>'Mei',6=>'Juni',7=>'Juli',8=>'Agustus',
                9=>'September',10=>'Oktober',11=>'November',12=>'Desember');
        return $bulan[$x];
    }

?>
@include('core-ui.layouts._layout')

@section('content')
<div class="row justify-content-center">
    <div class="col m-3">
        <div class="card" style="width: 100%;">
            <div class="card-header">
                Form Target
            </div>
            <div class="card-body">
                <form id="form" method="post" enctype="multipart/form-data" action="@if($deployment->status_id==4) {{ route('deployment.realisasi.store') }} @else {{ route('deployment.store') }} @endif ">
                    
                    <div class="row row-form">
                        @csrf
                        <input type="hidden" name="tahun" value="{{$deployment->tahun}}">
                        <input type="hidden" name="id_deployment" value="{{$deployment->id}}">
                        <input type="hidden" name="status_id" value="@if($deployment->status_id==1) 2 @else {{$deployment->status_id}} @endif">
                        <div class="col-sm-6">
                            <label for="note">Kode KPI</label>
                            <input type="text" name="kode_kpi" readonly class="form-control mb-1 border-primary" autocomplete="off" value={{ $deployment->kode_kpi}}>
                        </div>

                        <div class="col-sm-6">
                            <label for="note">Nama Kpi </label>
                            <textarea class="form-control mb-1 border-primary" readonly autocomplete="off"> {{$deployment->kpi['kpi']}} </textarea>
                        </div>

                        <div class="col-sm-6">
                            <label for="note">Kode Unit</label>
                            <input type="text" name="kode_unit" readonly class="form-control mb-1 border-primary" autocomplete="off" value="{{$deployment->kode_unit}}">
                        </div>

                        <div class="col-sm-6">
                            <label for="note">Target Tahunan</label>
                            <input type="text" name="target_tahunan"  id="target_tahunan" readonly class="form-control mb-1 border-primary" autocomplete="off" value="{{$deployment->target_tahunan}}">
                        </div>

                        <div class="col-sm-6">
                            <label for="note">Bobot Tahunan</label>
                            <input type="text" name="target_tahunan" readonly class="form-control mb-1 border-primary" autocomplete="off" value="{{$deployment->bobot_tahunan}}">
                        </div>
                        <div class="col-sm-6">
                            <label for="note">Rumus Akumulasi:</label>
                            <input type="text"  readonly class="form-control mb-1 border-primary" autocomplete="off" value="{{$deployment->rumusakumulasi['name']}}">
                            <input type="hidden" name="rumus_akumulasi" id="rumus_akumulasi" readonly class="form-control mb-1 border-primary" autocomplete="off" value="{{$deployment->rumus_akumulasi}}">
                        </div>
                        
                        <div class="col-sm-6">
                            <label for="note">Satuan</label>
                            <input type="text" name="satuan" readonly class="form-control mb-1 border-primary" autocomplete="off" value="{{$deployment->kpi['satuan']}}">
                        </div>
                        <div class="col-sm-6">
                            <label for="note">Rumus Capaian</label>
                            <input type="text"  readonly class="form-control mb-1 border-primary" autocomplete="off"  value="{{$deployment->rumuscapaian['ket_capaian']}}">
                            <input type="hidden" name="rumus_capaian" readonly class="form-control mb-1 border-primary" autocomplete="off" id="rumus_capaian" value="{{$deployment->rumus_capaian}}">
                        </div>
                        <div class="col-sm-6">
                            <label for="note">Akumulasi</label>
                            {{-- <input type="text" name="nil_akumulasi"  id="form-control mb-1 border-primary" autocomplete="off" value="{{$deployment->kpi->rumus_akumulasi}}"> --}}
                            <input type="text" readonly name="totalbulanan" id="totalbulanan" class="form-control mb-1 border-primary" autocomplete="off" value="{{$deployment->akumulasi}}">
                        </div>
                        <div class="col-sm-6">
                            <label for="note">Deskripsi</label>
                            {{-- <input type="text" name="nil_akumulasi"  id="form-control mb-1 border-primary" autocomplete="off" value="{{$deployment->kpi->rumus_akumulasi}}"> --}}
                            <input type="text" readonly name="totalbulanan" id="totalbulanan" class="form-control mb-1 border-primary" autocomplete="off" value="{{deskripsi($deployment->id)}}">
                        </div>
                        <div class="col-sm-6">
                            <label for="note">Keterangan</label><br>
                            <span class="badge badge-primary"><b><i>{{$deployment->status->name}}</i></b></span>
                        </div>
                       
                        
                        <div style="width:100%;padding:10px;background:rgb(217, 222, 242) none repeat scroll 0% 0%;margin:10px"> 
                            Target Perbulan
                        </div>
                        @if($deployment->status_id==4)
                                <div class="table-responsive" style="width:100%;overflow-x:scroll">
                                <table width="100%" border="1" style="margin-left:10px;margin-right:10px">
                                    <tr bgcolor="aqua">
                                        <td  class="tht" width="3%">No</td>
                                        <td class="tht" width="10%" >Bulan</td>
                                        <td class="tht" >Target</td>
                                        <td class="tht" >Realisasi</td>
                                        <td class="tht" width="8%">Capaian</td>
                                        <td class="tht" width="5%">File</td>
                                        <td class="tht" width="15%">Act</td>
                                        <td class="tht" width="10%">Masalah</td>
                                    </tr>
                                    @for($a=1; $a<=12; $a++)
                                        @foreach($targetdeployment->where('bulan',$a) as $tar)
                                            <tr>
                                                <td  class="tht">{{$a}}</td>
                                                <td class="tdtd">{{bulan($a)}}</td>
                                                <td class="tdtd">
                                                    <input type="text" onkeypress="return hanyaAngka(event)" style="width:100%;height:23px;font-size:10px" name="bulan{{$a}}" id="bulan{{$a}}" class="form-control mb-1 border-primary" autocomplete="off" value="{{$tar->target}}" onkeyup="hitung()"  @if($deployment->status_id==4) readonly @endif >

                                                    <input type="hidden" name="id_target{{$a}}" id="id_target{{$a}}" class="form-control mb-1 border-primary" autocomplete="off" value="{{ isset($tar->id) ? $tar->id : '' }}">
                                                
                                                </td>
                                                <td  class="tdtd">
                                                     <!-- <input type="text" onkeypress="return hanyaAngka(event)" style="width:100%;height:23px" name="realisasi{{$a}}" @if($a==(date('m')-1))  @else readonly @endif     id="realisasi{{$a}}" class="form-control mb-1 border-primary" autocomplete="off" value="{{ isset($tar->realisasi) ? $tar->realisasi :'0' }}"  onkeyup="hitungrealisasi()">  -->
                                                    <input type="text" readonly onkeypress="return hanyaAngka(event)" style="width:100%;height:23px;font-size:10px" name="realisasi{{$a}}" @if($deployment->status_id==1 || $deployment->status_id==2) readonly @else  @endif     id="realisasi{{$a}}" class="form-control mb-1 border-primary" autocomplete="off" value="{{ $tar->realisasi }}"  onkeyup="hitungrealisasi()">
                                                
                                                    <input type="hidden" name="id_targetok{{$a}}" id="id_target{{$a}}" class="form-control mb-1 border-primary" autocomplete="off" value="{{ isset($tar->id) ? $tar->id : '' }}">
                                                </td>
                                                <td class="tdt">
                                                    {{$capaian->shift()['capaian']}}%
                                                </td>
                                                <td  class="tdt">
                                                    @if($tar->target!=0)
                                                        @if(is_null($tar->file))
                                                            <!-- <img src="{{url('/img/loadd.gif')}}" width="20px" height="20px"> -->
                                                        @else
                                                            <a href="{{url('/storage/'.$tar->file)}}" target="_balnk"><img src="{{url('/img/download.png')}}" title="Download File" width="20px" height="20px"></a>
                                                        @endif
                                                    @else
                                                        <i class="fa fa-close"></i>
                                                    @endif
                                                </td>
                                                <td  class="tdt">
                                                    @if(is_null($tar->status_realisasi) || $tar->status_realisasi==1 )
                                                        @if($tar->target!=0)
                                                            <span data-toggle="modal" data-target="#exampleModal{{$tar->id}}" class="btn btn-success btn-sm" >
                                                                <i class="fa fa-edit"></i>
                                                            </span>
                                                        @else
                                                            <i class="fa fa-close"></i>
                                                        @endif
                                                    @else
                                                        {{$tar->tgl_validasi_atasan}}
                                                    @endif
                                                </td>
                                                <td  class="tdtisi" align="center">
                                                    @if($capaian3->shift()['capaian']>95 )
                                                        
                                                    @else
                                                        @if($tar->realisasi==0)

                                                        @else
                                                            <span class="badge badge-success" data-toggle="modal" data-target="#masalahnya{{$tar->id}}"><i class="fa fa-search" ></i> View Progress</span>
                                                        @endif
                                                        
                                                    @endif
                                                </td>
                                               
                                            </tr>
                                        @endforeach
                                    @endfor
                                        </table>
                                    </div>
							
							
                        @elseif($deployment->status_id==2 || $deployment->status_id==3)
                            <table width="30%" border="1" style="margin-left:10px">
								<tr bgcolor="aqua">
									<td  class="tht" width="10%">No</td>
									<td class="tht"  >Bulan</td>
                                    <td class="tht" width="40%">Target</td>
								</tr>
                                @for($a=1; $a<=12; $a++)
                                    @foreach($targetdeployment->where('bulan',$a) as $tar)
                                        <tr>
                                            <td  class="tht">{{$a}}</td>
                                            <td class="tdtd">{{bulan($a)}}</td>
                                            <td class="tdt">
                                            @if($deployment->rumus_capaian==3)
                                                <input type="text" id="datepicker{{$a}}" style="width:100%;height:23px;font-size:10px" name="bulan{{$a}}" id="bulan{{$a}}" class="form-control mb-1 border-primary" autocomplete="off"  value="{{ $tar->target}}" onkeyup="hitung(this.value)" >
                                            @elseif($deployment->rumus_capaian==1)
                                                <input type="text" onkeypress="return hanyaAngka(event)" style="width:100%;height:23px;font-size:10px" name="bulan{{$a}}" id="bulan{{$a}}" class="form-control mb-1 border-primary" autocomplete="off"  value="{{ $tar->target}}" onkeyup="hitung(this.value)" >
                                            @else
                                                <input type="text" style="width:100%;height:23px;font-size:10px" name="bulan{{$a}}" id="bulan{{$a}}" class="form-control mb-1 border-primary" autocomplete="off"  value="{{ $tar->target}}" onkeyup="hitung(this.value)" >
                                            @endif
                                               
                                                <!-- <input type="text" onkeypress="return hanyaAngka(event)" style="width:100%;height:23px;font-size:10px" name="bulan{{$a}}" id="bulan{{$a}}" class="form-control mb-1 border-primary" autocomplete="off"  value="{{ $tar->target}}" onkeyup="hitung(this.value)" > -->

                                                <input type="hidden" name="id_target{{$a}}" id="id_target{{$a}}" class="form-control mb-1 border-primary" autocomplete="off" value="{{ isset($tar->id) ? $tar->id : '' }}">
                                            </td>
                                            
                                        </tr>
                                    @endforeach
                                @endfor
                            </table>
                        @else
                            <table width="30%" border="1" style="margin-left:10px">
								<tr bgcolor="aqua">
									<td  class="tht" width="10%">No</td>
									<td class="tht"  >Bulan</td>
                                    <td class="tht" width="40%">Target</td>
								</tr>
                                @for($a=1; $a<=12; $a++)
                                    <tr>
                                        <td  class="tht">{{$a}}</td>
                                        <td class="tdtd">{{bulan($a)}}</td>
                                        <td class="tdt">
                                            @if($deployment->rumus_capaian==3)
                                                <input type="text"  style="width:100%;height:23px;font-size:10px" name="bulan{{$a}}" id="bulan{{$a}}" class="datepickert{{$a}} form-control mb-1 border-primary" autocomplete="off"  value="" onkeyup="hitung(this.value)" >
                                            @elseif($deployment->rumus_capaian==4)
                                                <input type="text"  style="width:100%;height:23px;font-size:10px" name="bulan{{$a}}" id="bulan{{$a}}" class="form-control mb-1 border-primary" autocomplete="off"  value="" onkeyup="hitung(this.value)" >
                                            @elseif($deployment->rumus_capaian==1)
                                                <input type="text" onkeypress="return hanyaAngka(event)" style="width:100%;height:23px;font-size:10px" name="bulan{{$a}}" id="bulan{{$a}}" class="form-control mb-1 border-primary" autocomplete="off"  value="" onkeyup="hitung(this.value)" >
                                            @else
                                                <input type="text" style="width:100%;height:23px;font-size:10px" name="bulan{{$a}}" id="bulan{{$a}}" class="form-control mb-1 border-primary" autocomplete="off"  value="" onkeyup="hitung(this.value)" >
                                            @endif 
                                            <input type="hidden" name="id_target{{$a}}" id="id_target{{$a}}" class="form-control mb-1 border-primary" autocomplete="off" value="">
                                        
                                        </td>
                                        
                                    </tr>
                                @endfor
                            </table>
                        @endif
                        
                    </div>
                </form>
                <div class="float-left mr-3 mt-2" id="tampil">
                    @if($deployment->status_id==1 || $deployment->status_id==2 )
                    <button class="btn btn-primary btn-sm" type="button" id="simpann" onclick="submit()">
                        <i class="fa fa-save"></i>
                        Simpan
                    </button>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

@for($a=1; $a<=12; $a++)
    @foreach($targetdeployment->where('bulan',$a) as $tar)
        
        
        <div id="masalahnya{{$tar->id}}" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg" style="width:90%">
            
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Masalah</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div id="alertalasannya{{$tar->id}}" class="alertalasannya"></div>
                        <form method="post" id="forminputalasannya{{$tar->id}}" enctype="multipart/form-data">
                            <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                            <input type = "hidden" name = "id_target" value = "{{$tar->id}}">
                            <div class="form-group"  >
                                <label for="message-text" class="col-form-label">Masalah Yang di Hadapi:</label>
                                <textarea class="form-control" readonly  >{{$tar->masalah}}</textarea>
                                
                                <label for="message-text" class="col-form-label">Rencana Perbaikan:</label>
                                <textarea class="form-control" readonly >{{$tar->rencana}}</textarea>
                                
                                <label for="recipient-name" class="col-form-label">Target Penyelasian (yyyy-mm-dd):</label>
                                <input type="text" class="form-control"  readonly value="{{$tar->tgl_target_penyelesaian}}" >
    
                                <label for="recipient-name" class="col-form-label">Progres (%):</label>
                                <input type="text" class="form-control"  name="progres"  value="{{$tar->progres}}" >
        
                                    
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <input type="submit" id="submitBtnalasan{{$tar->id}}" onclick="simpanalasan({{$tar->id}})" class="btn btn-primary" value="Proses">
                    </div>
                </div>
            
            </div>
        </div>

        <div id="rencananya{{$tar->id}}" class="modal fade" role="dialog">
            <div class="modal-dialog">
            
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Rencana Penyelesaian</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>{{$tar->rencana}}</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            
            </div>
        </div>
    
        <div class="modal fade" id="exampleModal{{$tar->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Penginputan Realisasi Periode {{bulan($a)}} {{date('Y')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body">
                    <div id="alertnya{{$tar->id}}" class="alertnya"></div>
                    <form method="post" id="forminput{{$tar->id}}" enctype="multipart/form-data">
                        <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                        <input type = "hidden" name = "id_target" value = "{{$tar->id}}">
                        <input type = "hidden" name = "id_deployment" value = "{{$tar->id_deployment}}">
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Target:</label>
                            <input type="text" class="form-control" id="target{{$tar->id}}" value="{{$tar->target}}" readonly >
                        </div>
                        @if($deployment->rumus_capaian==3)
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Realisasi:</label>
                                <input type="text" class="form-control start" onchange="nilairealisasi({{$tar->id}},this.value)" name="realisasi" value="{{$tar->realisasi}}" id="realisasi{{$tar->id}}">
                            </div>
                        @else
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Realisasi:</label>
                                <input type="text" class="form-control" onkeypress="return hanyaAngka(event)" onkeyup="nilairealisasi({{$tar->id}},this.value)" name="realisasi" value="{{$tar->realisasi}}" id="realisasi{{$tar->id}}">
                            </div>
                        @endif
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Capaian:</label>
                            <input type="text" class="form-control"  name="selisih" id="selisih{{$tar->id}}" value="{{$capaian2->shift()['capaian']}}">
                        </div>
                        <div class="form-group">
                            @if(is_null($tar->file))
                                <label for="recipient-name" class="col-form-label">File:</label>
                                <input type="file" class="form-control"  name="file" >
                                
                            @else
                                <label for="recipient-name" class="col-form-label">File:</label><br>
                                <img src="{{url('/img/download.png')}}" title="Download File" width="100px" height="100px">
                                <span class="btn btn-danger btn-sm" onclick="hapusfile({{$tar->id}})"><i class="fa fa-remove"></i></span>
                            @endif
                                <input type="hidden" class="form-control"  name="editfile" value="{{$tar->file}}">
                            
                        </div>
                        <div class="form-group" id="tampilmasalah{{$tar->id}}" >
                            <label for="message-text" class="col-form-label">Masalah Yang di Hadapi:</label>
                            <textarea class="form-control" name="masalah" >{{$tar->masalah}}</textarea>
                            
                            <label for="message-text" class="col-form-label">Rencana Perbaikan:</label>
                            <textarea class="form-control" name="rencana" >{{$tar->rencana}}</textarea>
                            
                            <label for="recipient-name" class="col-form-label">Target Penyelasian (yyyy-mm-dd):</label>
                            <input type="text" class="form-control"  name="tgl_target_penyelesaian" value="{{$tar->tgl_target_penyelesaian}}" >

                            <label for="recipient-name" class="col-form-label">Progres (%):</label>
                            <input type="text" class="form-control"  name="progres" value="{{$tar->progres}}" >
    
                                
                        </div>
                    </form>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" id="submitBtn{{$tar->id}}" onclick="simpan({{$tar->id}})" class="btn btn-primary" value="Proses">
                    </div>
                </div>
                </div>
            </div>

        
        
    @endforeach
@endfor




@push('plug')
<style>
    .tht{text-align:center;background:#03a9f4;font-weight:bold;}
    .tdt{text-align:center;background:#fff;font-weight:bold;font-size:9px;}
    .tdtisi{text-align:left;background:#fff;padding-left:4px;}
    .tdtd{text-align:center;background:aqua;font-weight:bold;}
    .alertnya{padding:5px;background:#ececea;}
    .alertalasannya{padding:5px;background:#ececea;}
    .tdr{padding-left:8px;}
    .border-primary {
        border-color: #c1d4dc !important;
    }
</style>
@for($a=1; $a<=12; $a++)
    @foreach($targetdeployment->where('bulan',$a) as $tar)
    <script type="text/javascript">
    
        

        $(document).ready(function(){
            
            var selisih=$('#selisih{{$tar->id}}').val();
            var realisasi=$('#realisasi{{$tar->realisasi}}').val();
            var capaian=$('#rumus_capaian').val();
            if(selisih>94){
                $('#tampilmasalah{{$tar->id}}').hide();
            }else{
                $('#tampilmasalah{{$tar->id}}').show();
            }
            $('#alertnya{{$tar->id}}').hide();
            $('#tampilmasalah{{$tar->id}}').hide();
            if(capaian==3){
                if(realisasi==0){
                    $('#selisih{{$tar->id}}').val('');
                }else{
                    $('#selisih{{$tar->id}}').val();
                }
            }
            
            
        })
    </script>
    @endforeach
@endfor
<script type="text/javascript">

function hapusfile(no){
    $.ajax({
        type: 'GET',
        url: "{{url('realisasi/hapusfile/')}}",
        data: 'id='+no,
        success: function(msg){
            document.location.reload();
        }
    });
}


function simpan(no){
    var formm=document.getElementById('forminput'+no);
    var id_deployment=$('#id_deployment').val();
    $.ajax({
        type: 'POST',
        url: "{{url('realisasi/insrealisasi')}}",
        data: new FormData(formm),
        contentType: false,
        cache: false,
        processData:false,
        beforeSend: function(){
            $('#submitBtn'+no).attr("disabled","disabled");
        },
        success: function(msg){
            if(msg=='ok'){
                document.location.reload();
            }else{
                $('#alertnya'+no).show();
                $('#alertnya'+no).html(msg);
                $("#submitBtn"+no).removeAttr("disabled");
            }
            
        }
    });
}

function simpanalasan(no){
    

    var data = $('#forminputalasannya'+no).serialize();
    $.ajax({
        type: 'POST',
        url: "{{url('realisasi/insalasannya')}}",
        data: data,
        beforeSend: function(){
            $('#submitBtnalasan'+no).attr("disabled","disabled");
        },
        success: function(data) {
            if(data=='ok'){
                document.location.reload();
            }else{
                $('#alertalasannya'+no).show();
                $('#alertalasannya'+no).html(data);
                $("#submitBtnalasan"+no).removeAttr("disabled");
            }
        }
    });
}


function nilairealisasi(no,a){
    var rumus_capaian=$('#rumus_capaian').val();
    var target=$('#target'+no).val();
    var realisasi=$('#realisasi'+no).val();
        
        if(rumus_capaian==1){
            selis=(1-((target-a)/target))*100;
            seli=Math.round(selis);
            if(seli>120){
                nilainya=120;
            }else{
                nilainya=seli;
            }

            $('#selisih'+no).val(nilainya);
            if(nilainya>94){
                $('#tampilmasalah'+no).hide();
            }else{
                $('#tampilmasalah'+no).show();
            }
        }

        if(rumus_capaian==2){
            selis=(1+((target-a)/target))*100;
            seli=Math.round(selis);
            if(seli>120){
                nilainya=120;
            }else{
                nilainya=seli;
            }
            //alert(target);
            //sss="(1+(("+target+"-"+a+")/"+target+"))*100";
            $('#selisih'+no).val(nilainya);
            if(nilainya>94){
                $('#tampilmasalah'+no).hide();
            }else{
                $('#tampilmasalah'+no).show();
            }
        }

        if(rumus_capaian==3){
            
            $.ajax({
                type: 'GET',
                url: "{{url('ajax/cektanggal.php')}}",
                data: 'mulai='+a+'&sampai='+target,
                success: function(msg){
                    //alert(msg);
                    if(msg>0){
                        $('#selisih'+no).val(msg);
                        if(msg>94){
                            $('#tampilmasalah'+no).hide();
                        }else{
                            $('#tampilmasalah'+no).show();
                        }
                    }else{
                        $('#selisih'+no).val(0);
                        
                    }
                    
                     

                }
            });
             
        }

        if(rumus_capaian==4){
            minmax=target.split('-');
            
                
            if(minmax[1]>a){
                seli=(1-((a-minmax[1])/minmax[1]))*100;
            }else{
                seli=(1-((minmax[0]-a)/minmax[0]))*100;
                
            }

                
           

            if(seli>120){
                nilainya=120;
            }else{
                nilainya=seli;
            }
            
            $('#selisih'+no).val(seli);
            if(nilainya>94){
                $('#tampilmasalah'+no).hide();
            }else{
                $('#tampilmasalah'+no).show();
            }
        }
        
        if(rumus_capaian==5){
            
            if(a>=120){
                nilainya=100;
            }else if(a>=110){
                nilainya=98;
            }else if(a>=100){
                nilainya=95;
            }else{
                nilainya=0;
            }

            $('#selisih'+no).val(nilainya);
            if(nilainya>94){
                $('#tampilmasalah'+no).hide();
            }else{
                $('#tampilmasalah'+no).show();
            }
        }
       
        
}


$(document).ready(function(e) {
    
	var akumulasi=$('#totalbulanan').val();
	var target=$('#target_tahunan').val();
	var rumus=$('#rumus_akumulasi').val();
    if(rumus==3){

    }else{
        if(akumulasi==target){
            $('#simpan').css("opacity","");
            $("#simpan").removeAttr("disabled");
        }else{
            $('#simpan').attr("disabled","disabled");
            $('#simpan').css("opacity",".5");
        }
    }
        
});

function hanyaAngka(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode==45 || charCode==46 || charCode==47 || charCode==48 || charCode==49 || charCode==50|| charCode==51|| charCode==52|| charCode==53|| charCode==54 || charCode==55|| charCode==56|| charCode==57)

        return true;
        return false;
}

function hitung(b){
    var akumulasi = $("#rumus_akumulasi").val();
    //var target=$('#target_tahunan').val();
    
    var jum = 0;
    
    for(a=1; a<=12; a++){
        jum += eval($("#bulan"+a).val());
    }
    if(akumulasi==1){
        total = jum.toFixed(2);
    }
    if(akumulasi==2){
        total = jum/12;
    }
    if(akumulasi==3){
        total = b;
    }
    
    $("#totalbulanan").val(total);
    

}

function rumus(){
    var akumulasi = $("#rumus_akumulasi").val();
        var target=$('#target_tahunan').val();
    var jum = 0;
    
    
    for(a=1; a<=12; a++){
        jum += eval($("#bulan"+a).val());
    }
        if(akumulasi==1){
        total = jum;
    }
    if(akumulasi==2){
        total = jum/12;
    }
    //alert(total);
    $("#totalbulanan").val(total);
    if(total==target){
        $('#simpan').css("opacity","");
        $("#simpan").removeAttr("disabled");
    }else{
            $('#simpan').attr("disabled","disabled");
            $('#simpan').css("opacity",".5");
    }
}
</script>
@endpush

@endsection
