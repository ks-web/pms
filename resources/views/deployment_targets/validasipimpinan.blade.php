@extends('core-ui.layouts.app')

@push('style')
<link href="{{ asset('vendors/datetimepicker/jquery.datetimepicker.min.css') }}" rel="stylesheet">
<link href="{{ asset('vendors/select2/css/select2.min.css') }}" rel="stylesheet" />
<link href="{{ asset('vendors/select2/css/select2-bootstrap4.min.css') }}" rel="stylesheet" />
<link href="{{ asset('vendors/summernote-master/summernote-bs4.css') }}" rel="stylesheet" />
<link href="{{ asset('vendors/DataTables/datatables.min.css') }}" rel="stylesheet">
@endpush

@push('script')
<script src="{{ asset('vendors/datetimepicker/jquery.datetimepicker.full.min.js') }}"></script>
<script src="{{ asset('vendors/select2/js/select2.min.js') }}"></script>
<script src="{{ asset('vendors/summernote-master/summernote-bs4.min.js') }}"></script>
<script src="{{ asset('vendors/DataTables/datatables.min.js') }}"></script> 
<script>
$(document).ready(function() {

    jQuery.datetimepicker.setLocale('id');

    jQuery('#start').datetimepicker({
        timepicker:false,
        mask:true,
        format:'d/m/Y'
    });
    jQuery('#end').datetimepicker({
        timepicker:false,
        mask:true,
        format:'d/m/Y'
    });

    $('#note').summernote({
        height: 90, 
    });


});
function submit(){
    $('#form').submit();
}


</script>
@endpush
<?php
    function bulan($x) {
        $bulan = array (1=>'Januari',2=>'Februari',3=>'Maret',4=>'April',
                5=>'Mei',6=>'Juni',7=>'Juli',8=>'Agustus',
                9=>'September',10=>'Oktober',11=>'November',12=>'Desember');
        return $bulan[$x];
    }

?>
@include('core-ui.layouts._layout')

@section('content')
<div class="row justify-content-center">
    <div class="col m-3">
        <div class="card" style="width: 100%;">
            <div class="card-header">
                Form Target
            </div>
            <div class="card-body">
                <form id="form" method="post" enctype="multipart/form-data" action="@if($deployment->status_id==4) {{ route('deployment.realisasi.store') }} @else {{ route('deployment.store') }} @endif ">
                    <div class="row row-form">
                        @csrf
                        <input type="hidden" name="tahun" value="{{$deployment->tahun}}">
                        <input type="hidden" name="id_deployment" value="{{$deployment->id}}">
                        <input type="hidden" name="status_id" value="@if($deployment->status_id==1) 2 @elseif($deployment->status_id==2) 3 @elseif($deployment->status_id==3) 4 @endif">
                        <div class="col-sm-6">
                            <label for="note">Kode KPI</label>
                            <input type="text" name="kode_kpi" readonly class="form-control mb-1 border-primary" autocomplete="off" value={{ isset($deployment->kode_kpi) ? $deployment->kode_kpi : ''}}>
                        </div>

                        <div class="col-sm-6">
                            <label for="note">Nama Kpi </label>
                            <textarea class="form-control mb-1 border-primary" readonly autocomplete="off"> {{$deployment->kpi['kpi']}} </textarea>
                        </div>

                        <div class="col-sm-6">
                            <label for="note">Kode Unit</label>
                            <input type="text" name="kode_unit" readonly class="form-control mb-1 border-primary" autocomplete="off" value="{{$deployment->kode_unit}}">
                        </div>

                        <div class="col-sm-6">
                            <label for="note">Target Tahunan</label>
                            <input type="text" name="target_tahunan"  id="target_tahunan" readonly class="form-control mb-1 border-primary" autocomplete="off" value="{{$deployment->target_tahunan}}">
                        </div>

                        <div class="col-sm-6">
                            <label for="note">Bobot Tahunan</label>
                            <input type="text" name="target_tahunan" readonly class="form-control mb-1 border-primary" autocomplete="off" value="{{$deployment->bobot_tahunan}}">
                        </div>
						
						<div class="col-sm-6">
                            <label for="note">Satuan</label>
                            <input type="text" name="satuan" readonly class="form-control mb-1 border-primary" autocomplete="off" value="{{$deployment->kpi['satuan']}}">
                        </div>
                        
                        <div class="col-sm-6">
                            <label for="rumus_akumulasi">Rumus Akumulasi: {{$deployment->status_id}}</label>
                           
                                <select class="form-control border-primary" required onchange="rumus()" name="accumulations_id" id="rumus_akumulasi" >
                                    @foreach ($accumulations as $accumulation)
                                        @if($deployment->rumus_akumulasi==$accumulation->id)
                                            <option value="{{ $accumulation->id }}"  >{{ $accumulation->name }}</option>
                                        @endif
                                        
                                    @endforeach
                                </select>
                            
                        </div>

                        <div class="col-sm-6">
                            <label for="note">Akumulasi</label>
                            {{-- <input type="text" name="nil_akumulasi" id="rumus_akumulasi" id="form-control mb-1 border-primary" autocomplete="off" value="{{$deployment->kpi->rumus_akumulasi}}"> --}}
                            <input type="text" readonly name="totalbulanan" id="totalbulanan" class="form-control mb-1 border-primary" autocomplete="off" value="{{$deployment->akumulasi}}">
                        </div>
                        <div class="col-sm-6">
                            <label for="note">Keterangan</label><br>
                            <span class="badge badge-primary"><b><i>{{$deployment->status->name}}</i></b></span>
                        </div>
                        <div style="width:100%;padding:10px;background:rgb(144, 159, 218) none repeat scroll 0% 0%;margin:10px;font-weight:bold"> 
                            Target Perbulan
                        </div>
						
						
							
                        @if($jumlah != 0)
                            @if($deployment->status_id==4)
                                <div class="table-responsive" style="width:100%;overflow-x:scroll">
                                <table width="100%" border="1" style="margin-left:10px;margin-right:10px">
                                    <tr bgcolor="aqua">
                                        <td  class="tht" width="3%">No</td>
                                        <td class="tht" width="13%" >Bulan</td>
                                        <td class="tht" >Target</td>
                                        <td class="tht" >Realisasi</td>
                                        <td class="tht" width="8%">Capaian</td>
                                        <td class="tht" width="5%">File</td>
                                        <td class="tht" width="15%">Act</td>
                                        <td class="tht" width="10%">Masalah</td>
                                    </tr>
                                @for($a=1; $a<=12; $a++)
                                    @foreach($targetdeployment->where('bulan',$a) as $tar)
                                        <tr>
                                            <td  class="tht">{{$a}}</td>
                                            <td class="tdtd">{{bulan($a)}}</td>
                                            <td class="tdt">
                                                <input type="text" onkeypress="return hanyaAngka(event)" style="width:100%;height:23px" name="bulan{{$a}}" id="bulan{{$a}}" class="form-control mb-1 border-primary" autocomplete="off" value="{{ isset($tar->target) ? $tar->target :'0' }}" onkeyup="hitung()"  @if($deployment->status_id==4) readonly @endif >

                                                <input type="hidden" name="id_target{{$a}}" id="id_target{{$a}}" class="form-control mb-1 border-primary" autocomplete="off" value="{{ isset($tar->id) ? $tar->id : '' }}">
                                            
                                            </td>
                                            <td  class="tdt">
                                                {{-- <input type="text" onkeypress="return hanyaAngka(event)" style="width:100%;height:23px" name="realisasi{{$a}}" @if($a==(date('m')-1))  @else readonly @endif     id="realisasi{{$a}}" class="form-control mb-1 border-primary" autocomplete="off" value="{{ isset($tar->realisasi) ? $tar->realisasi :'0' }}"  onkeyup="hitungrealisasi()"> --}}
                                                <input type="text" readonly onkeypress="return hanyaAngka(event)" style="width:100%;height:23px" name="realisasi{{$a}}" @if($deployment->status_id==1 || $deployment->status_id==2) readonly @else  @endif     id="realisasi{{$a}}" class="form-control mb-1 border-primary" autocomplete="off" value="{{ isset($tar->realisasi) ? $tar->realisasi :'0' }}"  onkeyup="hitungrealisasi()">
                                            
                                                <input type="hidden" name="id_targetok{{$a}}" id="id_target{{$a}}" class="form-control mb-1 border-primary" autocomplete="off" value="{{ isset($tar->id) ? $tar->id : '' }}">
                                            </td>
                                            <td class="tdt">
                                                {{$tar->selisih}}%
                                            </td>
                                            <td  class="tdt">
                                                @if(is_null($tar->file))
                                                    <!-- <img src="{{url('/img/loadd.gif')}}" width="20px" height="20px"> -->
                                                @else
                                                    <img src="{{url('/img/download.png')}}" title="Download File" width="20px" height="20px">
                                                @endif
                                            </td>
                                            <td  class="tdt">
                                                @if(is_null($tar->status_realisasi) )
                                                    <!-- <img src="{{url('/img/loadd.gif')}}" width="20px" height="20px"> -->
                                                @elseif($tar->status_realisasi==1)
                                                    <span  class="btn btn-success btn-sm" data-toggle="modal" data-target="#exampleModal{{$tar->id}}" >
                                                        <i class="fa fa-search"></i>
                                                    </span>
                                                @elseif($tar->status_realisasi==2)
                                                    {{$tar->tgl_validasi_atasan}}
                                                @endif
                                            </td>
                                            <td  class="tdtisi">
                                                @if($tar->selisih>95 ||is_null($tar->selisih) )
                                                    
                                                @else
                                                    <span class="badge badge-success" data-toggle="modal" data-target="#masalahnya{{$tar->id}}"><i class="fa fa-search" ></i> View Progress</span>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @endfor
                                </table>
                            </div>	
							@else
									<table width="50%" border="1" style="margin-left:10px">
										<tr bgcolor="aqua">
											<td  class="tht" width="10%">No</td>
											<td class="tht"  >Bulan</td>
											<td class="tht" width="30%">Target</td>
										</tr>
									@for($a=1; $a<=12; $a++)
										@foreach($targetdeployment->where('bulan',$a) as $tar)
											<tr>
												<td  class="tht">{{$a}}</td>
												<td class="tdtd">{{bulan($a)}}</td>
												<td class="tdt">
													 <input type="text" readonly onkeypress="return hanyaAngka(event)" style="width:100%;height:23px;font-size:10px" name="bulan{{$a}}" id="bulan{{$a}}" class="form-control mb-1 border-primary" autocomplete="off" value="{{ isset($tar->target) ? $tar->target :'0' }}" onkeyup="hitung()"  @if($deployment->status_id==4) readonly @endif >

													<input type="hidden" name="id_target{{$a}}" id="id_target{{$a}}" class="form-control mb-1 border-primary" autocomplete="off" value="{{ isset($tar->id) ? $tar->id : '' }}">
												
												</td>
												
											</tr>
										@endforeach
									@endfor
							@endif
							
							
                        @else
							<table width="50%" border="1" style="margin-left:10px">
								<tr bgcolor="aqua">
									<td  class="tht" width="10%">No</td>
									<td class="tht"  >Bulan</td>
									<td class="tht" width="30%">Target</td>
								</tr>
                            @for($a=1; $a<=12; $a++)
                                <tr>
                                        <td  class="tht">{{$a}}</td>
                                        <td class="tdtd">{{bulan($a)}}</td>
                                        <td class="tdt">
											 <input type="text" onkeypress="return hanyaAngka(event)" style="width:100%;height:23px;font-size:10px" name="bulan{{$a}}" id="bulan{{$a}}" class="form-control mb-1 border-primary" autocomplete="off"  value="{{ isset($tar->id) ? $tar->id : '' }}" onkeyup="hitung()" >

											<input type="hidden" name="id_target{{$a}}" id="id_target{{$a}}" class="form-control mb-1 border-primary" autocomplete="off" value="{{ isset($tar->id) ? $tar->id : '' }}">
										
										</td>
                                       
                                    </tr>
                            @endfor
                        @endif
						</table>
                    </div>
                </form>
                <div class="float-left mr-3 mt-2" id="tampil">
                        @if($deployment->status_id==2 )
                            <button class="btn btn-success btn-sm" type="button" id="simpan" onclick="submit()">
                                <i class="fa fa-check"></i>
                                Validasi
                            </button>
                        @else
                            
                        @endif
                    
                </div>
            </div>
        </div>
    </div>
</div>

@for($a=1; $a<=12; $a++)
    @foreach($targetdeployment->where('bulan',$a) as $tar)
    <div id="masalahnya{{$tar->id}}" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg" style="width:90%">
        
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Masalah</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table width="100%" border="1">
                        <tr align="center" bgcolor="aqua">
                            <td >Masalah</td>
                            <td width="30%">Rencana</td>
                            <td width="15%">Progress</td>
                            <td width="15%">Tgl Lapor</td>
                        </tr>
                        @foreach($progres->where('deployment_targets_id',$tar->id) as $prog)
                        <tr>
                            <td class="tdr">{{$prog->masalah}}</td>
                            <td class="tdr">{{$prog->rencana}}</td>
                            <td class="tdr">{{$prog->progres}}%</td>
                            <td class="tdr">{{$prog->tgl_progres}}</td>
                        </tr>
                        @endforeach
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        
        </div>
    </div>
    <div id="alasannya{{$tar->id}}" class="modal fade" role="dialog">
            <div class="modal-dialog">
            
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Alasan</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>{{$tar->keterangan}}</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            
            </div>
        </div>

        <div id="masalahnya{{$tar->id}}" class="modal fade" role="dialog">
            <div class="modal-dialog">
            
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Masalah</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>{{$tar->masalah}}</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            
            </div>
        </div>

        <div id="rencananya{{$tar->id}}" class="modal fade" role="dialog">
            <div class="modal-dialog">
            
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Rencana Penyelesaian</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>{{$tar->rencana}}</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            
            </div>
        </div>

        <div class="modal fade" id="exampleModal{{$tar->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Validasi Realisasi Periode {{bulan($a)}} {{date('Y')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body">
                <div id="alertnya{{$tar->id}}" class="alertnya"></div>
                {{-- <form method="post" id="forminput{{$tar->id}}" enctype="multipart/form-data"> --}}
                    <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                    <input type = "hidden" name = "id_target" value = "{{$tar->id}}">
                    <input type = "hidden" name = "id_deployment" value = "{{$tar->id_deployment}}">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Target:</label>
                        <input type="text" class="form-control" id="target{{$tar->id}}" value="{{$tar->target}}" readonly >
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Realisasi:</label>
                        <input type="text" class="form-control" onkeyup="nilairealisasi({{$tar->id}})" readonly name="realisasi" value="{{$tar->realisasi}}" id="realisasi{{$tar->id}}">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Selisih:</label>
                        <input type="text" class="form-control"  name="selisih" id="selisih{{$tar->id}}" readonly value="{{$tar->selisih}}">
                    </div>
                    <div class="form-group">
                        @if(is_null($tar->file))
                            
                        @else
                            <label for="recipient-name" class="col-form-label">File:</label><br>
                            <img src="{{url('/img/download.png')}}" title="Download File" width="100px" height="100px">
                            
                        @endif
                            <input type="hidden" class="form-control"  name="editfile" value="{{$tar->file}}">
                        
                    </div>
                    <div class="form-group" id="alasan{{$tar->id}}" >
                        <label for="message-text" class="col-form-label">Keterangan:</label>
                        <textarea readonly class="form-control" name="keterangan" >{{$tar->keterangan}}</textarea>
                    </div>
                {{-- </form> --}}
                </div>
                <div class="modal-footer">
                <span onclick="kembalikan({{$tar->id}})" class="btn btn-primary">Kembalikan</span>
                <span onclick="validasi({{$tar->id}})" class="btn btn-success">Validasi</span>
                </div>
            </div>
            </div>
        </div>

        
        
    @endforeach
@endfor
<style>
.tht{text-align:center;background:#03a9f4;font-weight:bold;}
.tdt{text-align:center;background:#fff;font-weight:bold;}
.tdtd{text-align:center;background:aqua;font-weight:bold;}
</style>
@push('plug')
<script type="text/javascript">
    function validasi(no){
        $.ajax({
            type: 'GET',
            url: "{{url('realisasi/validasi')}}",
            data: 'id='+no,
            success: function(msg){
                
                    document.location.reload();
                
            }
        });
    }

    function kembalikan(no){
        $.ajax({
            type: 'GET',
            url: "{{url('realisasi/kembalikan')}}",
            data: 'id='+no,
            success: function(msg){
                
                    document.location.reload();
                
            }
        });
    }

    $(document).ready(function(e) {
            
                
    });

    function hanyaAngka(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))

            return false;
        return true;
    }
    function hitung(){
        var akumulasi = $("#rumus_akumulasi").val();
        var target=$('#target_tahunan').val();
        
        var jum = 0;
        for(a=1; a<=12; a++){
            jum += eval($("#bulan"+a).val());
        }
        if(akumulasi==1){
            total = jum;
        }
        if(akumulasi==2){
        total = jum/12;
        }
    //alert(total);
    $("#totalbulanan").val(total);
    
    
    }

    function rumus(){
        var akumulasi = $("#rumus_akumulasi").val();
        var target=$('#target_tahunan').val();
        var jum = 0;
        
        
        for(a=1; a<=12; a++){
            jum += eval($("#bulan"+a).val());
        }
        if(akumulasi==1){
            total = jum;
        }
        if(akumulasi==2){
        total = jum/12;
        }
        if(akumulasi==3){
            $('#simpan').css("opacity","");
            $("#simpan").removeAttr("disabled");
        }
    //alert(total);
        $("#totalbulanan").val(total);
            if(total==target){
                $('#simpan').css("opacity","");
                $("#simpan").removeAttr("disabled");
            }else{
                $('#simpan').attr("disabled","disabled");
                $('#simpan').css("opacity",".5");
            }
    }
</script>
@endpush

@endsection
