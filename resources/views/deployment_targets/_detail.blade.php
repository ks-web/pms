<?php
    function bulan($x) {
        $bulan = array (1=>'Januari',2=>'Februari',3=>'Maret',4=>'April',
                5=>'Mei',6=>'Juni',7=>'Juli',8=>'Agustus',
                9=>'September',10=>'Oktober',11=>'November',12=>'Desember');
        return $bulan[$x];
    }

?>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header btn btn-secondary">
          <h5 class="modal-title" id="exampleModalLabel">Detail data</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="row row-form">
                <input type="hidden" name="tahun" value="{{$deployment->tahun}}">
                <input type="hidden" id="id_deployment" name="id_deployment" value="{{$deployment->id}}">
                <div class="col-sm-6">
                    <label for="note">Kode KPI</label>
                    <input type="text" name="kode_kpi" readonly class="form-control mb-1" autocomplete="off" value={{ isset($deployment->kode_kpi) ? $deployment->kode_kpi : ''}}>
                </div>

                <div class="col-sm-6">
                    <label for="note">Nama Kpi </label>
                    <textarea class="form-control mb-1" readonly autocomplete="off"> {{$deployment->kpi['kpi']}} </textarea>
                </div>

                <div class="col-sm-6">
                    <label for="note">Kode Unit</label>
                    <input type="text" name="kode_unit" readonly class="form-control mb-1" autocomplete="off" value="{{$deployment->kode_unit}}">
                </div>

                <div class="col-sm-6">
                    <label for="note">Target Tahunan</label>
                    <input type="text" name="target_tahunan" readonly class="form-control mb-1" autocomplete="off" value="{{$deployment->target_tahunan}}">
                </div>

                <div class="col-sm-6">
                    <label for="note">Bobot Tahunan</label>
                    <input type="text" name="target_tahunan" readonly class="form-control mb-1" autocomplete="off" value="{{$deployment->bobot_tahunan}}">
                </div>

                <div style="width:100%;padding:10px;background:rgb(217, 222, 242) none repeat scroll 0% 0%;margin:10px"> 
                    Target Perbulan
                </div>
                @if($jumlah != 0)
                    @if($deployment->status_id==4)
                        <table width="50%" border="1" style="margin-left:10px">
                            <tr bgcolor="aqua">
                                <td  class="tht" width="10%">No</td>
                                <td class="tht"  >Bulan</td>
                                <td class="tht" width="30%">Target</td>
                                <td class="tht" width="30%">Realisasi</td>
                            </tr>
                        @for($a=1; $a<=12; $a++)
                            @foreach($targetdeployment->where('bulan',$a) as $tar)
                                <tr>
                                    <td  class="tht">{{$a}}</td>
                                    <td class="tdtd">{{bulan($a)}}</td>
                                    <td class="tdt">
                                        <input type="text" onkeypress="return hanyaAngka(event)" style="width:100%;height:23px" name="bulan{{$a}}" id="bulan{{$a}}" class="form-control mb-1 border-primary" autocomplete="off" value="{{ isset($tar->target) ? $tar->target :'0' }}" onkeyup="hitung()"  @if($deployment->status_id==4) readonly @endif >

                                        <input type="hidden" name="id_target{{$a}}" id="id_target{{$a}}" class="form-control mb-1 border-primary" autocomplete="off" value="{{ isset($tar->id) ? $tar->id : '' }}">
                                    
                                    </td>
                                    <td  class="tdt">
                                        <input type="text" onkeypress="return hanyaAngka(event)" style="width:70%" name="realisasi{{$a}}" @if($a==(date('m')-1))  @else readonly @endif     id="realisasi{{$a}}" class="form-control mb-1 border-primary" autocomplete="off" value="{{ isset($tar->realisasi) ? $tar->realisasi :'0' }}"  onkeyup="hitungrealisasi()">
                                    
                                        <input type="hidden" name="id_targetok{{$a}}" id="id_target{{$a}}" class="form-control mb-1 border-primary" autocomplete="off" value="{{ isset($tar->id) ? $tar->id : '' }}">
                                    </td>
                                
                                </tr>
                            @endforeach
                        @endfor
                        
                    @else
                        <table width="50%" border="1" style="margin-left:10px">
                            <tr bgcolor="aqua">
                                <td  class="tht" width="10%">No</td>
                                <td class="tht"  >Bulan</td>
                                <td class="tht" width="30%">Target</td>
                            </tr>
                        @for($a=1; $a<=12; $a++)
                            @foreach($targetdeployment->where('bulan',$a) as $tar)
                                <tr>
                                    <td  class="tht">{{$a}}</td>
                                    <td class="tdtd">{{bulan($a)}}</td>
                                    <td class="tdt">
                                        <input type="text" onkeypress="return hanyaAngka(event)" style="width:100%;height:23px" name="bulan{{$a}}" id="bulan{{$a}}" class="form-control mb-1 border-primary" autocomplete="off" value="{{ isset($tar->target) ? $tar->target :'0' }}" onkeyup="hitung()"  @if($deployment->status_id==4) readonly @endif >

                                        <input type="hidden" name="id_target{{$a}}" id="id_target{{$a}}" class="form-control mb-1 border-primary" autocomplete="off" value="{{ isset($tar->id) ? $tar->id : '' }}">
                                    
                                    </td>
                                    
                                </tr>
                            @endforeach
                        @endfor
                    @endif
        
                @else
                    @for($a=1; $a<=12; $a++)
                        <div class="col-sm-6">
                            <label for="note">Target Bulan {{bulan($a)}}</label>
                            <input type="text" readonly onkeypress="return hanyaAngka(event)" style="width:70%" name="bulan{{$a}}" id="bulan{{$a}}" class="form-control mb-1
                            " autocomplete="off" value="0" onkeyup="hitung()">

                            <input type="hidden" name="id_target{{$a}}" id="id_target{{$a}}" class="form-control mb-1 border-primary" autocomplete="off" value="0">
                        </div>
                    @endfor
                @endif
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">
            <i class="fa fa-window-close"></i>
                Close
            </button>
        </div>
      </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#validasi').on('click',function(e){
                var token = $("meta[name='csrf-token']").attr("content");
                var url = $(event.target).data('url');
                var id_deployment = $('#id_deployment').val();
            
                $.ajax({ 
                    url: url,
                    type: "PUT",
                    data: {
                        "_token": token, 
                        "id_deployment": id_deployment,
                    },
                    success: function (data, textStatus, jqXHR) {
                        $('#exampleModal').modal('hide');
                        location.reload(true);                        
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert("AJAX error: " + textStatus + ' : ' + errorThrown);
                    },
                });
            });
    });
</script>