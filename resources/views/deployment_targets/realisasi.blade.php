@extends('core-ui.layouts.app')

@push('style')
<link href="{{ asset('vendors/datetimepicker/jquery.datetimepicker.min.css') }}" rel="stylesheet">
<link href="{{ asset('vendors/select2/css/select2.min.css') }}" rel="stylesheet" />
<link href="{{ asset('vendors/select2/css/select2-bootstrap4.min.css') }}" rel="stylesheet" />
<link href="{{ asset('vendors/summernote-master/summernote-bs4.css') }}" rel="stylesheet" />
<link href="{{ asset('vendors/DataTables/datatables.min.css') }}" rel="stylesheet">
@endpush

@push('script')
<script src="{{ asset('vendors/datetimepicker/jquery.datetimepicker.full.min.js') }}"></script>
<script src="{{ asset('vendors/select2/js/select2.min.js') }}"></script>
<script src="{{ asset('vendors/summernote-master/summernote-bs4.min.js') }}"></script>
<script src="{{ asset('vendors/DataTables/datatables.min.js') }}"></script> 
<script>
$(document).ready(function() {

    jQuery.datetimepicker.setLocale('id');

    jQuery('#start').datetimepicker({
        timepicker:false,
        mask:true,
        format:'d/m/Y'
    });
    jQuery('#end').datetimepicker({
        timepicker:false,
        mask:true,
        format:'d/m/Y'
    });

    $('#note').summernote({
        height: 90, 
    });


});
function submit(){
    $('#form').submit();
}


</script>
@endpush
<?php
    function bulan($x) {
        $bulan = array (1=>'Januari',2=>'Februari',3=>'Maret',4=>'April',
                5=>'Mei',6=>'Juni',7=>'Juli',8=>'Agustus',
                9=>'September',10=>'Oktober',11=>'November',12=>'Desember');
        return $bulan[$x];
    }

?>
@include('core-ui.layouts._layout')

@section('content')
<div class="row justify-content-center">
    <div class="col m-3">
        <div class="card" style="width: 100%;">
            <div class="card-header">
                Form Target Realisasi
            </div>
            <div class="card-body">
                <form id="form" method="post" enctype="multipart/form-data" action="{{ route('deployment.storerealisasi') }}">
                    <div class="row row-form">
                        @csrf
                        <input type="hidden" name="tahun" value="{{$deployment->tahun}}">
                        <input type="hidden" name="id_deployment" value="{{$deployment->id}}">
                        <input type="text" name="status_id" value="@if($deployment->status_id==1) 2 @else {{$deployment->status_id}} @endif">
                        <div class="col-sm-6">
                            <label for="note">Kode KPI</label>
                            <input type="text" name="kode_kpi" readonly class="form-control mb-1 border-primary" autocomplete="off" value={{ isset($deployment->kode_kpi) ? $deployment->kode_kpi : ''}}>
                        </div>

                        <div class="col-sm-6">
                            <label for="note">Nama Kpi </label>
                            <textarea class="form-control mb-1 border-primary" readonly autocomplete="off"> {{$deployment->kpi['kpi']}} </textarea>
                        </div>

                        <div class="col-sm-6">
                            <label for="note">Kode Unit</label>
                            <input type="text" name="kode_unit" readonly class="form-control mb-1 border-primary" autocomplete="off" value="{{$deployment->kode_unit}}">
                        </div>

                        <div class="col-sm-6">
                            <label for="note">Target Tahunan</label>
                            <input type="text" name="target_tahunan"  id="target_tahunan" readonly class="form-control mb-1 border-primary" autocomplete="off" value="{{$deployment->target_tahunan}}">
                        </div>

                        <div class="col-sm-6">
                            <label for="note">Bobot Tahunan</label>
                            <input type="text" name="target_tahunan" readonly class="form-control mb-1 border-primary" autocomplete="off" value="{{$deployment->bobot_tahunan}}">
                        </div>
                        
                        <div class="col-sm-6">
                            <label for="rumus_akumulasi">Rumus Akumulasi: </label>
                            <select class="form-control border-primary" required onchange="rumus()" name="accumulations_id" id="rumus_akumulasi"  @if($deployment->status_id==4) disabled @endif >
                                @foreach ($accumulations as $accumulation)
                                    <option value={{ $accumulation->id }} @if($deployment->accumulations_id==$accumulation->id) selected @endif   >{{ $accumulation->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-sm-6">
                            <label for="note">Akumulasi</label>
                            {{-- <input type="text" name="nil_akumulasi" id="rumus_akumulasi" id="form-control mb-1 border-primary" autocomplete="off" value="{{$deployment->kpi->rumus_akumulasi}}"> --}}
                            <input type="text" readonly name="totalbulanan" id="totalbulanan" class="form-control mb-1 border-primary" autocomplete="off" value="{{$deployment->akumulasi}}">
                        </div>
                        <div style="width:100%;padding:10px;background:rgb(217, 222, 242) none repeat scroll 0% 0%;margin:10px"> 
                            Target Perbulan
                        </div>
                        @if($jumlah != 0)
                            @for($a=1; $a<=12; $a++)
                                @foreach($targetdeployment->where('bulan',$a) as $tar)
                                    <div class="col-sm-6">
                                        <label for="note">Target Bulan {{bulan($a)}}</label>
                                        <input type="text" onkeypress="return hanyaAngka(event)" style="width:70%" name="bulan{{$a}}" id="bulan{{$a}}" class="form-control mb-1 border-primary" autocomplete="off" value="{{ isset($tar->target) ? $tar->target :'0' }}" onkeyup="hitung()"  @if($deployment->status_id==4) readonly @endif >

                                        <input type="hidden" name="id_target{{$a}}" id="id_target{{$a}}" class="form-control mb-1 border-primary" autocomplete="off" value="{{ isset($tar->id) ? $tar->id : '' }}">
                                    </div>
                                @endforeach
                            @endfor
							
							@if($deployment->status_id==4)
									<div style="width:100%;padding:10px;background:rgb(217, 222, 242) none repeat scroll 0% 0%;margin:10px"> 
										Realisasi Perbulan
									</div>
									 @for($a=1; $a<=12; $a++)
										@foreach($targetdeployment->where('bulan',$a) as $tar)
											<div class="col-sm-6">
												<label for="note">Realisasi Bulan {{bulan($a)}}</label>
												<input type="text" onkeypress="return hanyaAngka(event)" style="width:70%" name="realisasi{{$a}}" id="realisasi{{$a}}" class="form-control mb-1 border-primary" autocomplete="off" value="{{ isset($tar->realisasi) ? $tar->realisasi :'0' }}"  onkeyup="hitung()">

												<input type="hidden" name="id_target{{$a}}" id="id_target{{$a}}" class="form-control mb-1 border-primary" autocomplete="off" value="{{ isset($tar->id) ? $tar->id : '' }}">
												
												
											</div>
										@endforeach
									@endfor
							@endif
                        @else
                            @for($a=1; $a<=12; $a++)
                                <div class="col-sm-6">
                                    <label for="note">Target Bulan {{bulan($a)}}</label>
                                    <input type="text" onkeypress="return hanyaAngka(event)" style="width:70%" name="bulan{{$a}}" id="bulan{{$a}}" class="form-control mb-1 border-primary" autocomplete="off" value="0" onkeyup="hitung()">

                                    <input type="hidden" name="id_target{{$a}}" id="id_target{{$a}}" class="form-control mb-1 border-primary" autocomplete="off" value="{{ isset($tar->id) ? $tar->id : '' }}">
                                </div>
                            @endfor
                        @endif
                    </div>
                </form>
                <div class="float-left mr-3 mt-2" id="tampil">
                    <button class="btn btn-primary btn-sm" type="button" id="simpan" onclick="submit()">
                        <i class="fa fa-save"></i>
                        Simpan
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
@push('plug')
<script type="text/javascript">

$(document).ready(function(e) {
    
	var akumulasi=$('#totalbulanan').val();
	var target=$('#target_tahunan').val();
	if(akumulasi==target){
		 $('#simpan').css("opacity","");
         $("#simpan").removeAttr("disabled");
	}else{
		 $('#simpan').attr("disabled","disabled");
		 $('#simpan').css("opacity",".5");
	}
});

    function hanyaAngka(evt) {
		  var charCode = (evt.which) ? evt.which : event.keyCode
		   if (charCode > 31 && (charCode < 48 || charCode > 57))
 
		    return false;
		  return true;
	}
    function hitung(){
        var akumulasi = $("#rumus_akumulasi").val();
        var target=$('#target_tahunan').val();
        
        var jum = 0;
        for(a=1; a<=12; a++){
            jum += eval($("#bulan"+a).val());
        }
         if(akumulasi==1){
            total = jum;
        }
        if(akumulasi==2){
           total = jum/12;
        }
       //alert(total);
       $("#totalbulanan").val(total);
	   
	   if(total==target){
			$('#simpan').css("opacity","");
			$("#simpan").removeAttr("disabled");
		}else{
			 $('#simpan').attr("disabled","disabled");
			 $('#simpan').css("opacity",".5");
		}
    }

    function rumus(){
        var akumulasi = $("#rumus_akumulasi").val();
         var target=$('#target_tahunan').val();
        var jum = 0;
		
		
        for(a=1; a<=12; a++){
            jum += eval($("#bulan"+a).val());
        }
         if(akumulasi==1){
            total = jum;
        }
        if(akumulasi==2){
           total = jum/12;
        }
       //alert(total);
       $("#totalbulanan").val(total);
	   if(total==target){
			$('#simpan').css("opacity","");
			$("#simpan").removeAttr("disabled");
		}else{
			 $('#simpan').attr("disabled","disabled");
			 $('#simpan').css("opacity",".5");
		}
    }
</script>
@endpush

@endsection
