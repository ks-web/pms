@extends('core-ui.layouts.app')

@push('style')
<link href="{{ asset('vendors/DataTables/datatables.min.css') }}" rel="stylesheet">
@endpush

@push('script')
<script src="{{ asset('vendors/DataTables/datatables.min.js') }}"></script> 
<script>
$(document).ready(function() {
    $('#table').DataTable({
        responsive: true,
    });

});
</script>
@endpush

@include('core-ui.layouts._layout')

@section('content')
<div class="row justify-content-center">
    <div class="col m-3">
        @if(session('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif
        <div class="card" style="width: 100%;">
            <div class="card-header">
                <a href="{{route('kpi.import')}}" class="btn btn-primary">Upload Data</a>
            </div>
        </div>
        <div class="card" style="width: 100%;">
            <div class="card-header">
              <b>Master Data Kpi</b>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <div class="table-responsive" style="width:100%;overflow-x:scroll">
                    <table style="font-size:12px" class="table table-hover responsive no-wrap table-striped" id="table" width="120%">
                        <thead>
                            <tr> 
                                <th>Kode Kpi</th>
                                <th width="20%">Nama KPI</th>
                                <th width="20%">Deskripsi</th>
                                <th>Satuan</th>
                                <th>Tipe Capaian</th>
                                <th>Rumus Akumulasi </th>
                                <th>Tanggal update</th>
                                <th>Aksi</th>

                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($kpis as $kpi)
                                <tr>
                                    <td>{{$kpi->kode_kpi}}</td>
                                    <td>{{$kpi->kpi}}</td>
                                    <td>{{$kpi->deskripsi}}</td>
                                    <td>{{$kpi->satuan}}</td>
                                    <td>{{$kpi->rumuscapaian['ket_capaian']}}</td>
                                    <td>{{$kpi->rumusakumulasi['name']}}</td>
                                    <td>{{date('d/m/Y',strtotime($kpi->updated_at))}}</td>
                                    <td>
                                        <a href="{{ route('kpi.edit', $kpi->id)}}" data-toggle="tooltip" data-placement="bottom" title="edit data kpi">
                                            <button class="btn btn-outline-success btn-sm ml-1" >
                                                <i class="fa fa-edit"></i>
                                            </button>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
