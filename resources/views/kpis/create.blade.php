@extends('core-ui.layouts.app')

@push('style')
<link href="{{ asset('vendors/datetimepicker/jquery.datetimepicker.min.css') }}" rel="stylesheet">
<link href="{{ asset('vendors/select2/css/select2.min.css') }}" rel="stylesheet" />
<link href="{{ asset('vendors/select2/css/select2-bootstrap4.min.css') }}" rel="stylesheet" />
<link href="{{ asset('vendors/summernote-master/summernote-bs4.css') }}" rel="stylesheet" />
<link href="{{ asset('vendors/DataTables/datatables.min.css') }}" rel="stylesheet">
@endpush

@push('script')
<script src="{{ asset('vendors/datetimepicker/jquery.datetimepicker.full.min.js') }}"></script>
<script src="{{ asset('vendors/select2/js/select2.min.js') }}"></script>
<script src="{{ asset('vendors/summernote-master/summernote-bs4.min.js') }}"></script>
<script src="{{ asset('vendors/DataTables/datatables.min.js') }}"></script> 
@endpush

@include('core-ui.layouts._layout')

@section('content')
<div class="row justify-content-center">
    <div class="col m-3">
        <div class="card" style="width: 100%;">
            <div class="card-header">
                <b>Upload master data kpi </b>
            </div>
            <div class="card-body">
                <form id="form" method="post" enctype="multipart/form-data" action="{{ route('kpi.import.process') }}">
                    <div class="row row-form">
                        @csrf
                        <div class="col-sm-6">
                            <div class="col-sm-12">
                                <label for="start"><b>Pilih File</b></label>
                                <input type="file" name="files" class="form-control mb-1 border-primary" id="files" autocomplete="off">
                            </div>
                            <br/>
                            <div class="col-sm-12">
                                <button class="btn btn-primary" type="button" onclick="submit()">
                                    <i class="fa fa-save"></i>
                                    Simpan
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
