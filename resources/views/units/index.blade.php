@extends('core-ui.layouts.app')

@push('style')
<link href="{{ asset('vendors/DataTables/datatables.min.css') }}" rel="stylesheet">
@endpush

@push('script')
<script src="{{ asset('vendors/DataTables/datatables.min.js') }}"></script> 
<script>
$(document).ready(function() {
    $('#table').DataTable({
        responsive: true,
    });

});
</script>
@endpush

@include('core-ui.layouts._layout')

@section('content')
<div class="row justify-content-center">
    <div class="col m-3">
       
        <div class="card" style="width: 100%;">
            <div class="card-header">
              <b>Master Data Unit</b>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <div class="table-responsive" style="width:100%;overflow-x:scroll">
                    <table class="table table-hover responsive no-wrap table-striped" id="table" width="130%">
                        <thead>
                            <tr> 
                                <th>Kode Unit</th>
                                <th>Nama Unit Kerja</th>
                                <th>Pejabat</th>
                                <th>PIC</th>
                                <th>Act</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($units as $unit)
                                <tr>
                                    <td>{{$unit->kode}}</td>
                                    <td>{{$unit->nama}}</td>
                                    <td>{{$unit->pimpinan}}</td>
                                    <td>@if(is_null($unit->nik)) <i>Tidak Ada</i> @else [<b>{{$unit->nik}}</b>] {{$unit->nama_pic}} @endif</td>
                                    <td>
										@role('administrator')
											
												<a href="{{url('pic/unit/'.$unit->kode)}}">
													<button class="btn btn-outline-primary btn-sm ml-1 modal-valid"  title="Validasi">
														<i class="fa fa-edit" ></i>
													</button>
												</a>
											
										@endrole
									</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
