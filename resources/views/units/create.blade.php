@extends('core-ui.layouts.app')

@push('style')
<link href="{{ asset('vendors/datetimepicker/jquery.datetimepicker.min.css') }}" rel="stylesheet">
<link href="{{ asset('vendors/select2/css/select2.min.css') }}" rel="stylesheet" />
<link href="{{ asset('vendors/select2/css/select2-bootstrap4.min.css') }}" rel="stylesheet" />
<link href="{{ asset('vendors/summernote-master/summernote-bs4.css') }}" rel="stylesheet" />
<link href="{{ asset('vendors/DataTables/datatables.min.css') }}" rel="stylesheet">
@endpush

@push('script')
<script src="{{ asset('vendors/datetimepicker/jquery.datetimepicker.full.min.js') }}"></script>
<script src="{{ asset('vendors/select2/js/select2.min.js') }}"></script>
<script src="{{ asset('vendors/summernote-master/summernote-bs4.min.js') }}"></script>
<script src="{{ asset('vendors/DataTables/datatables.min.js') }}"></script> 
@endpush

@include('core-ui.layouts._layout')

@section('content')
<div class="row justify-content-center">
    <div class="col m-3">
        <div class="card" style="width: 100%;">
            <div class="card-header">
                <b>Form PIC Unit Kerja</b>
            </div>
            <div class="card-body">
                <form id="form" method="post" enctype="multipart/form-data" action="{{ route('unit.pic.process') }}">
                    <div class="row row-form">
                        @csrf
                        <div class="col-sm-6">
                            <div class="col-sm-12">
                                <label for="start">Kode Unit</label>
                                <input type="text" name="kode" readonly value="{{$unit->kode}}" id="kode" class="form-control mb-1 border-primary" >
                            </div>
							<div class="col-sm-12">
                                <label for="start">Unit Kerja</label>
                                <input type="text" name="nama" readonly value="{{$unit->nama}}" class="form-control mb-1 border-primary" >
                            </div>
							<div class="col-sm-12">
                                <label for="start">NIK Atasan</label>
                                <input type="text"  id="nik_atasan"  onkeyup="carinikatasan()" value="{{$unit->nik_atasan}}" class="form-control mb-1 border-primary" >
                            </div>
                            <div class="col-sm-12">
                                <label for="start">Nama Atasan </label>
                                <input type="text" name="nama_atasan" readonly id="nama_atasan"  value="{{$unit->user['name']}}" class="form-control mb-1 border-primary" >
                            </div>
							<div class="col-sm-12">
                                <label for="start">NIK</label>
                                <input type="text"  id="nik"  onkeyup="carinik()" value="{{$unit->nik}}" class="form-control mb-1 border-primary" >
                            </div>
							<div class="col-sm-12">
                                <label for="start">Nama </label>
                                <input type="text" name="nama_pic" readonly id="nama"  value="{{$unit->nama_pic}}" class="form-control mb-1 border-primary" >
                            </div>
							
                            <input type="hidden" name="role" readonly  id="role"  value="3" >
                                <input type="hidden" name="nik" readonly  id="nik_ok" value="{{$unit->nik}}" >
                                <input type="hidden" name="nik_atasan" readonly  id="nik_atasan_ok" value="{{$unit->nik}}" >
                            <br/>
                            <div class="col-sm-12">
                                <button class="btn btn-primary" type="button" onclick="submit()">
                                    <i class="fa fa-save"></i>
                                    Simpan
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
<script>
function carinik(){
	var nik=$('#nik').val();
	$.ajax({
			type: 'GET',
			url: "{{url('ajax/cari_nik.php')}}",
			data: 'pic=1&nik='+nik,
			success: function(data){
				cek=data.split('-');
				if(cek[0]=='no'){
					
				}else{
					$('#nama').val(cek[0]);
					$('#unit').val(cek[1]);
					$('#nik_ok').val(cek[2]);
				}
				
			}
		});
}
function carinikatasan(){
	var nik=$('#nik_atasan').val();
	var kode=$('#kode').val();
	$.ajax({
			type: 'GET',
			url: "{{url('ajax/cari_nik.php')}}",
			data: 'pic=2&nik='+nik+'&kode='+kode,
			success: function(data){
				cek=data.split('-');
                
				if(cek[0]=='no'){
					
				}else{
					$('#nama_atasan').val(cek[0]);
					$('#nik_atasan_ok').val(cek[2]);
				}
				
			}
		});
}
</script>
