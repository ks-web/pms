@extends('core-ui.layouts.app')

@push('script')
<script></script>
@endpush
@push('style')
<style></style>
@endpush

@include('core-ui.layouts._layout')
<?php
    function bulan($x) {
        $bulan = array (1=>'Januari',2=>'Februari',3=>'Maret',4=>'April',
                5=>'Mei',6=>'Juni',7=>'Juli',8=>'Agustus',
                9=>'September',10=>'Oktober',11=>'November',12=>'Desember');
        return $bulan[$x];
    }

?>
@section('content')
<div class="container mt-2">
    <div class="row justify-content-center">
        <div class="col">
            <div class="card">
                
                <div class="card-header">
                    <div class="text-center">
                        <img src="{{ url('img/krakatausteel-logo-text.png') }}" alt="logo ks" width="150px" >
                    </div>
                </div>

                <div class="card-body">
                        
                    @if(Auth::user()->hasRole('boss') || Auth::user()->hasRole('administrator'))
                        @if(Auth::user()->hasRole('administrator'))
                        {{-- <div style="width:65%;float:left;margin-bottom:40px;height:432px">
                            <img src="{{url('img/eper.jpg')}}" style="width:100%;height:432px">
                        </div>
                        <div style="width:33%;float:left;background:#ece7f1;height:432px;padding:10px;margin-left: 10px;">
                                <form class="form-inline" method="get" action="{{ route('deployment.target.index') }}">
                                    @csrf
                                    <div class="form-group mb-2">
                                        <select id="unit" name="unit" class="form-control" onchange="cari()" placeholder="nama unit">
                                            <option value="">Pilih nama unit</option>
                                            @foreach ($namaunit as $uni)
                                                <option value="{{ $uni->kode }}-{{ $uni->nama }}">{{ ucwords(strtolower($uni->nama)) }}</option>
                                            @endforeach
                                            
                                        </select>
                                    </div>
                                </form>
                                <table width="100%">
                                        <tr>
                                            <td colspan="2" style="background: #e5d4f7;text-align:center;font-weight:bold">{{$nama}}</td>
                                        </tr>
                                            @for($i=1; $i<=12; $i++)
                                            
                                                <tr class="ttrr">
                                                    <td width="60%" class="ttr">{{bulan($i)}}</td>
                                                    @if($jumvalidasi->shift()['validasi']>0)
                                                        <td class="ttde" style="background: aqua" >{{$jumvalidasi->shift()['tanggal']}}</td>
                                                    @else
                                                        <td class="ttde" ></td>
                                                    @endif
                                                </tr>
                                                        
                                                
                                            @endfor
                                </table>   
                            
                        </div><hr> --}}
                        @endif
                        
                        <form  method="get" action="{{ url('/dashboard') }}"  style="display:inline">
                            @csrf
                            <div class="form-group mb-2">
                                <input type="text" name="tahun" id="tahun" style="width:50%;display:inline" placeholder="tahun" value="{{old('tahun')}}" class="form-control" />
                                <button class="btn mx-sm-3 btn-primary mb-2" style="display:inline">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                            
                        </form>
                        
                        <table width="100%" borde="1">
                            <tr>
                                <td class="tth" width="5%">No</td>
                                <td class="tth">Unit Kerja</td>
                                @for($a=1; $a<=12; $a++)
                                    <td class="tth" width="5%">{{substr(bulan($a),0,3)}}</td>
                                @endfor
                                <td class="tth" width="5%">No</td>
                            </tr>
                            @foreach($unit as $no=>$o)
                                <tr class="ttrt">
                                    <td class="ttd">{{($no+1)}}</td>
                                    <td class="ttd">{{$o->unit['nama']}}</td>
                                    @for($x=1; $x<=12; $x++)
                                       
                                        {!!$jumvalidasi->shift()['validasi']!!}
                                    @endfor
                                    {{-- <td>{{round(($tervalid->shift()['jumlah']/12)*100,2)}}</td> --}}
                                    <td class="ttd" style="font-weight:bold">{{$tervalid->shift()['jumlah']}}</td>
                                </tr>
                                
                            @endforeach
                        </table>
                    @else
                        <div style="width:70%;float:left;">
                            <img src="{{url('img/eper.jpg')}}" style="width:100%">
                        </div>
                        <div style="width:29%;float:left;background:#ece7f1;height:432px;padding:10px;margin-left: 10px;">
                                <form class="form-inline" method="get" action="{{ route('deployment.target.index') }}">
                                    @csrf
                                    <div class="form-group mb-2">
                                        <select id="unit" name="unit" class="form-control" onchange="cari()" placeholder="nama unit">
                                            <option value="">Pilih nama unit</option>
                                            @foreach ($units as $unit)
                                                <option value="{{ $unit->kode }}-{{ $unit->nama }}">{{ ucwords(strtolower($unit->nama)) }}</option>
                                            @endforeach
                                            
                                        </select>
                                    </div>
                                </form>
                                <table width="100%">
                                        <tr>
                                            <td colspan="2" style="background: #e5d4f7;text-align:center;font-weight:bold">{{$nama}}</td>
                                        </tr>
                                            @for($i=1; $i<=12; $i++)
                                            
                                                <tr class="ttrr">
                                                    <td width="60%" class="ttr">{{bulan($i)}}</td>
                                                    @if($jumvalidasi->shift()['validasi']>0)
                                                        <td class="ttde" style="background: aqua" >{{$jumvalidasi->shift()['tanggal']}}</td>
                                                    @else
                                                        <td class="ttde" ></td>
                                                    @endif
                                                </tr>
                                                        
                                                
                                            @endfor
                                </table>   
                            
                        </div>
                    @endif
                
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .tth{padding:5px;background: #5a7777;border:solid 1px #000;color:#fff;}
    .ttde{padding:3px;background: #d9ead7;border:solid 1px #ece7f1;color:#000;}
    .ttr{padding:3px 3px 3px 8px;border:solid 1px #ece7f1;color:#000;}
    .ttrr{padding:3px;background: #c1eabc;border:solid 1px #ece7f1;color:#000;}
    .ttrr:hover{padding:3px;background: #fff;border:solid 1px #ece7f1;color:#000;}
    .ttrt{background: #e1eaf3;}
    .ttrt:hover{background: yellow;}
    .ttd{padding:5px;border:solid 1px #000;}
    .ttds{padding:5px;background: #fff;border:solid 1px #000;font-size:9px;}
</style>
<script>
    function cari(){
        var unit=$('#unit').val();
            un=unit.split('-');
        // $.ajax({
        //     type: 'GET',
        //     url: "{{url('ajax/cektanggal.php')}}",
        //     data: 'mulai='+realisasi+'&sampai='+target,
        //     success: function(msg){
        //         $('#selisih'+no).val(msg);
                    

        //     }
        // });
        location.assign("{{url('dashboard')}}/"+un[0]+"/"+un[1]);
    }
</script>
@endsection