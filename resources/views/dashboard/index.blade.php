@extends('core-ui.layouts.app')

@push('script')
<script></script>
@endpush
@push('style')
<style></style>
@endpush

@include('core-ui.layouts._layout')
<?php
    function bulan($x) {
        $bulan = array (1=>'Januari',2=>'Februari',3=>'Maret',4=>'April',
                5=>'Mei',6=>'Juni',7=>'Juli',8=>'Agustus',
                9=>'September',10=>'Oktober',11=>'November',12=>'Desember');
        return $bulan[$x];
    }

?>
@section('content')
<div class="container mt-2">
    <div class="row justify-content-center">
        <div class="col">
            <div class="card">
                
                <div class="card-header">
                    <div class="text-center">
                        <img src="{{ url('img/krakatausteel-logo-text.png') }}" alt="logo ks" width="150px" >
                    </div>
                </div>

                <div class="card-body">
                        
                    @if(Auth::user()->hasRole('administrator'))
                        
                        <div style="width:100%;padding:10px 5px 1px 5px;background:#bfabe4;text-align:left">
                            <form  method="get" action="{{ url('/dashboard') }}"  style="display:inline">
                                @csrf
                                <div class="form-group mb-2">
                                    <select  class="form-control" name="tahun" id="tahun" style="width:20%;display:inline">
                                        <option value="{{date('Y')}}">{{date('Y')}}</option>
                                        @foreach($tahun as $tah)
                                            <option value="{{$tah->tahun}}">{{$tah->tahun}}</option>
                                        @endforeach
                                    </select>
                                    <select  class="form-control" name="bulan" id="bulan" style="width:20%;display:inline">
                                        <option value="">Pilih bulan--</option>
                                        @for($b=1;$b<13;$b++)
                                            <option value="{{$b}}">{{bulan($b)}}</option>
                                        @endfor
                                    </select>
                                    <button class="btn mx-sm-3 btn-primary mb-2" onclick="caridata()" style="display:inline">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                                
                            </form>
                        </div>
                        <div id="chartContainer" style="height: 300px; width: 100%;margin-top:100px"></div>
                        <div style="width:100%;padding:10px;background:#fff;text-align:center">
                                <span class="btn btn-primary btn-sm" id="tombolbuka" onclick="tampil()"><i class="fa fa-search"></i> Lihat lebih detail </span>
                                <span class="btn btn-success btn-sm" id="tomboltutup" onclick="tutup()"><i class="fa fa-remove"></i> Tutup </span>
                        </div>
                        <div id="tampildetail">
                            <table width="100%" borde="1">
                                <tr>
                                    <td class="tth" style="text-align:center;" width="5%">No</td>
                                    <td class="tth"  style="text-align:center;">Unit Kerja</td>
                                    @for($a=1; $a<=12; $a++)
                                        <td  style="text-align:center;" class="tth" width="5%">{{substr(bulan($a),0,3)}}</td>
                                    @endfor
                                </tr>
                                @foreach($unit as $no=>$o)
                                {{-- @foreach($totalunit as $no=>$o) --}}
                                    <tr class="ttrt">
                                        <td class="ttd" style="text-align:center;background:#85a2ba;color:#fff">{{($no+1)}}</td>
                                        <td class="ttd" style="background:#85a2ba;color:#fff">{{$o->unit['nama']}}</td>
                                        @for($x=1; $x<=12; $x++)
                                            @if(Tglvalidasi($o->kode_unit,$x,$tahuun)==0)
                                            <td style="border:solid 1px #000;background:#e1eaf3;"></td>
                                            @else
                                            <td style="border:solid 1px #000;background:yellow">{{view_Tglvalidasi($o->kode_unit,$x,$tahuun)}}</td>
                                            @endif
                                            
                                        @endfor
                                    </tr>
                                    
                                @endforeach
                                    <tr class="ttrt">
                                        <td class="ttd"></td>
                                        <td class="ttd"></td>
                                        @for($x=1; $x<=12; $x++)
                                        
                                            <td class="ttd">{{round($totalsemua->shift()['total'])}}%</td>
                                        @endfor
                                    </tr>  
                            </table>
                        </div>

                        @elseif(Auth::user()->hasRole('boss'))
                        
                        <div style="width:100%;padding:10px 5px 1px 5px;background:#bfabe4;text-align:left">
                            <form  method="get" action="{{ url('/dashboard') }}"  style="display:inline">
                                @csrf
                                <div class="form-group mb-2">
                                    <select  class="form-control" name="tahun" id="tahun" style="width:20%;display:inline">
                                        <option value="{{date('Y')}}">{{date('Y')}}</option>
                                        @foreach($tahun as $tah)
                                            <option value="{{$tah->tahun}}">{{$tah->tahun}}</option>
                                        @endforeach
                                    </select>
                                    <button class="btn mx-sm-3 btn-primary mb-2" onclick="caridata()" style="display:inline">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                                
                            </form>
                        </div>
                        
                            <table width="100%" borde="1">
                                <tr>
                                    <td class="tth" width="5%">No</td>
                                    <td class="tth">Unit Kerja</td>
                                    @for($a=1; $a<=12; $a++)
                                        <td class="tth" width="5%">{{substr(bulan($a),0,3)}}</td>
                                    @endfor
                                </tr>
                                @foreach($unit as $no=>$o)
                                
                                    <tr class="ttrt">
                                        <td class="ttd">{{($no+1)}}</td>
                                        <td class="ttd">{{$o->unit['nama']}}</td>
                                        @for($x=1; $x<=12; $x++)
                                            @if(cek_tgl_validasi($o['kode_unit'],$o['tahun'],$x)>0)
                                                <td class="ttd" style="background:yellow">{{Tglvalidasi($o['kode_unit'],$x,$o['tahun'])}}</td>
                                            @else
                                                <td class="ttd"></td>
                                            @endif
                                        @endfor
                                    </tr>
                                    
                                @endforeach
                                     
                            </table>
                       
                    @else
                        <div style="width:70%;float:left;">
                            <img src="{{url('img/eper.jpg')}}" style="width:100%">
                        </div>
                        <div style="width:29%;float:left;background:#ece7f1;height:432px;padding:10px;margin-left: 10px;">
                                <form class="form-inline" method="get" action="{{ route('deployment.target.index') }}">
                                    @csrf
                                    <div class="form-group mb-2">
                                        <select id="unit" name="unit" class="form-control" onchange="cari()" placeholder="nama unit">
                                            <option value="">Pilih nama unit</option>
                                            @foreach ($namaunit as $unitnya)
                                                <option value="{{ $unitnya->kode }}-{{ $unitnya->nama }}">{{ ucwords(strtolower($unitnya->nama)) }}</option>
                                            @endforeach
                                            
                                        </select>
                                    </div>
                                </form>
                                <table width="100%">
                                        <tr>
                                            <td colspan="2" style="background: #e5d4f7;text-align:center;font-weight:bold">{{$nama}}</td>
                                        </tr>
                                            @for($i=1; $i<=12; $i++)
                                            
                                                <tr class="ttrr">
                                                    <td width="60%" class="ttr">{{bulan($i)}}</td>
                                                    

                                                    @if(Tglvalidasi($kode,$i,$tahuun)=='')
                                                        <td class="ttde" style="background: aqua" ></td>
                                                    @else
                                                        <td class="ttde" >{{Tglvalidasi($kode,$i,$tahuun)}}</td>
                                                    @endif
                                                </tr>
                                                        
                                                
                                            @endfor
                                </table>   
                            
                        </div>
                    @endif
                
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .tth{padding:5px;background: #5a7777;border:solid 1px #000;color:#fff;}
    .ttde{padding:3px;background: #d9ead7;border:solid 1px #ece7f1;color:#000;}
    .ttr{padding:3px 3px 3px 8px;border:solid 1px #ece7f1;color:#000;}
    .ttrr{padding:3px;background: #c1eabc;border:solid 1px #ece7f1;color:#000;}
    .ttrr:hover{padding:3px;background: #fff;border:solid 1px #ece7f1;color:#000;}
    .ttrt{background: #e1eaf3;}
    .ttrt:hover{background: yellow;}
    .ttd{padding:5px;border:solid 1px #000;}
    .ttdr{padding:4px;border:solid 1px #000;font-size:11px;}
    .ttds{padding:5px;background: #fff;border:solid 1px #000;font-size:9px;}
</style>


<script type="text/javascript">
    $('#tampildetail').hide();
    $('#tomboltutup').hide();
    function tampil(){
        $('#tampildetail').show();
        $('#tomboltutup').show();
        $('#tombolbuka').hide();
    }
    function tutup(){
        $('#tampildetail').hide();
        $('#tomboltutup').hide();
        $('#tombolbuka').show();
    }
    function cari(){
        var unit=$('#unit').val();
            un=unit.split('-');
       
        location.assign("{{url('dashboard')}}/"+un[0]+"/"+un[1]);
    }
    function caridata(){
        var tahun=$('#tahun').val();
        var bulan=$('#bulan').val();
           
       
        location.assign("{{url('dashboard')}}/no/no/"+tahun+"/"+bulan);
    }
window.onload = function() {
    
    var options = {
        title: {
            text: "Website Traffic E-performance"
        },
        data: [{
                type: "pie",
                startAngle: 100,
                showInLegend: "true",
                legendText: "{label}",
                indexLabel: "{label} ({y})",
                yValueFormatString:"#,##0.#"%"",
                dataPoints: [
                    { label: "Tervalidasi", y: {{$totalada}} },
                    { label: "Belum Tervalidasi", y: {{$totalsisa}} }
                ]
        }]
    };
    $("#chartContainer").CanvasJSChart(options);
    
}
</script>
{{-- <script type="text/javascript" src="{{url('js/jquery-1.11.1.min.js')}}"></script>  
<script type="text/javascript" src="{{url('js/jquery.canvasjs.min.js')}}"></script> --}}
@endsection