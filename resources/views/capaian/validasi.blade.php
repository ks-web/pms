@extends('core-ui.layouts.app')

@push('style')
<link href="{{ asset('vendors/DataTables/datatables.min.css') }}" rel="stylesheet">
@endpush

@push('script')
<script src="{{ asset('vendors/DataTables/datatables.min.js') }}"></script> 
<script>
    $(document).ready(function() {
        $('#table').DataTable({
            responsive: true,
        });

        $('tbody').on('click','.modal-valid',function(e){
            var token = $("meta[name='csrf-token']").attr("content");
            var url = $(event.target).data('url');
            $.ajax({ 
                url: url,
                type: "GET",
                data: {"_token": token, },
                success: function (data, textStatus, jqXHR) {
                    $("#modalresponse").html(data.html);
                    $('#exampleModal').modal('show');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert("AJAX error: " + textStatus + ' : ' + errorThrown);
                },
            });
        });

        $('tbody').on('click','.show-detail',function(e){
            var token = $("meta[name='csrf-token']").attr("content");
            var url = $(event.target).data('url');
            $.ajax({ 
                url: url,
                type: "GET",
                data: {"_token": token, },
                success: function (data, textStatus, jqXHR) {
                    $("#modalresponse").html(data.html);
                    $('#exampleModal').modal('show');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert("AJAX error: " + textStatus + ' : ' + errorThrown);
                },
            });
        });
    });
</script>
@endpush

<?php
    function bulan($x) {
        $bulan = array (1=>'Januari',2=>'Februari',3=>'Maret',4=>'April',
                5=>'Mei',6=>'Juni',7=>'Juli',8=>'Agustus',
                9=>'September',10=>'Oktober',11=>'November',12=>'Desember');
        return $bulan[$x];
    }

?>

@include('core-ui.layouts._layout')

@section('content')
<div class="row justify-content-center">
    <div class="col m-3">
        @if (session('success'))
        <div class="alert alert-success" role="alert">
            {{ session('success') }}
            </div>
        @endif
        <div class="card" style="width: 100%;">
            @if(Auth::user()->hasRole('keyperson') || Auth::user()->hasRole('administrator') || Auth::user()->hasRole('boss'))
                <div class="card-header">              
                        <form class="form-inline" method="get" action="{{ url('/capaian/deployment/laporan') }}">
                            @csrf
                            <div class="form-group mb-2">
                                    <input type="text" name="tahun" id="tahun" placeholder="tahun" style="width:20%;display:inline" value="{{$tahun}}" class="form-control" />
                                <select id="unit" name="unit" class="form-control" onchange="cari()" placeholder="nama unit">
                                    <option value="">Pilih nama unit</option>
                                    @foreach ($units as $unit)
                                        <option value="{{ $unit->kode }}" @if($kode==$unit->kode) selected @endif>{{$unit->nama}}</option>
                                    @endforeach
                                    
                                </select>
                            </div>
                        </form>
                </div>
            @endif
            <!-- @if(Auth::user()->hasRole('boss'))
                <div class="card-header">              
                    {{-- <form class="form-inline" method="get" > --}}
                        {{-- @csrf --}}
                        <div class="form-group mb-2">
                            <input type="text" name="tahun" id="tahun" placeholder="tahun" style="width:20%;display:inline" value="{{old('tahun')}}" class="form-control" />
                            <button class="btn mx-sm-3 btn-primary mb-2" onclick="carii()">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                        
                        
                    {{-- </form> --}}
                </div>
            @endif -->
            <div class="card-header">
              <b>Data Capaian</b>
            </div>
            <div class="card-body"> 
                <div style="width:100%;text-align:center;text-transform:uppercase">
                    <b>DAFTAR VALIDASI PENCAPAIAN <br>{{cek_unit($kode)['nama']}}</b>
                </div><hr>
                <div class="table-responsive">
                    <div class="table-responsive" style="width:100%;overflow-x:scroll;min-height:300px">
                        <div id="load_tampilan">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- modal --}}
<div id="modalresponse" style="margin-top:2rem">
</div>
{{--  end modal --}}

@endsection
<style>
    table{font-size:12px;}
    .isidata{background: #fff;font-weight:bold;text-align: center;}
    .isidatath{background: aqua;font-weight:bold;text-align: center;}
</style>
@push('load')
    <script>
        $(document).ready(function(){
            var unit=$('#unit').val();
            var tahun=$('#tahun').val();
            $("#load_tampilan").load("{{url('/capaian/deployment/view_laporan_acc/')}}/"+unit+"/"+tahun);
        });
    </script>
@endpush
<script>
    function validasiyah(a,b,c){
        $.ajax({
            type: 'GET',
            url: "{{url('realisasi/validasiperbulan/')}}",
            data: 'bulan='+a+'&kode='+b+'&tahun='+c,
            success: function(msg){
                alert('Sukses Tervalidasi');
                document.location.reload(); 
                //alert(msg)
            }
        });
        
    }
</script>
<script>
    

    
    function cari(){
        var unit=$('#unit').val();
        var tahun=$('#tahun').val();
        location.assign("{{url('capaian/deployment/validasi')}}/"+unit+"/"+tahun);
    }
    function carii(){
        var tahun=$('#tahun').val();
        location.assign("{{url('capaian/deployment/validasi')}}/all/all/"+tahun);
    }
</script>