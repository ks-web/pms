@extends('core-ui.layouts.app')

@push('style')
<link href="{{ asset('vendors/DataTables/datatables.min.css') }}" rel="stylesheet">
@endpush

@push('script')
<script src="{{ asset('vendors/DataTables/datatables.min.js') }}"></script> 
<script>
    $(document).ready(function() {
        $('#table').DataTable({
            responsive: true,
        });

        $('tbody').on('click','.modal-valid',function(e){
            var token = $("meta[name='csrf-token']").attr("content");
            var url = $(event.target).data('url');
            $.ajax({ 
                url: url,
                type: "GET",
                data: {"_token": token, },
                success: function (data, textStatus, jqXHR) {
                    $("#modalresponse").html(data.html);
                    $('#exampleModal').modal('show');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert("AJAX error: " + textStatus + ' : ' + errorThrown);
                },
            });
        });

        $('tbody').on('click','.show-detail',function(e){
            var token = $("meta[name='csrf-token']").attr("content");
            var url = $(event.target).data('url');
            $.ajax({ 
                url: url,
                type: "GET",
                data: {"_token": token, },
                success: function (data, textStatus, jqXHR) {
                    $("#modalresponse").html(data.html);
                    $('#exampleModal').modal('show');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert("AJAX error: " + textStatus + ' : ' + errorThrown);
                },
            });
        });
    });
</script>
@endpush

<?php
    function bulan($x) {
        $bulan = array (1=>'Januari',2=>'Februari',3=>'Maret',4=>'April',
                5=>'Mei',6=>'Juni',7=>'Juli',8=>'Agustus',
                9=>'September',10=>'Oktober',11=>'November',12=>'Desember');
        return $bulan[$x];
    }

?>

@include('core-ui.layouts._layout')

@section('content')
<div class="row justify-content-center">
    <div class="col m-3">
        @if (session('success'))
        <div class="alert alert-success" role="alert">
            {{ session('success') }}
            </div>
        @endif
        <div class="card" style="width: 100%;">
            @if(Auth::user()->hasRole('keyperson') || Auth::user()->hasRole('administrator') || Auth::user()->hasRole('boss'))
                <div class="card-header">              
                        <form class="form-inline" method="get" action="{{ route('deployment.target.index') }}">
                            @csrf
                            <div class="form-group mb-2">
                                    <input type="text" name="tahun" id="tahun" placeholder="tahun" style="width:20%;display:inline" value="{{old('tahun')}}" class="form-control" />
                                <select id="unit" name="unit" class="form-control" onchange="cari()" placeholder="nama unit">
                                    <option value="">Pilih nama unit</option>
                                    @foreach ($units as $unit)
                                        <option value="{{ $unit->kode }}-{{$unit->nama}}">{{ ucwords(strtolower($unit->nama)) }}</option>
                                    @endforeach
                                    
                                </select>
                            </div>
                        </form>
                </div>
            @endif
            <!-- @if(Auth::user()->hasRole('boss'))
                <div class="card-header">              
                    {{-- <form class="form-inline" method="get" > --}}
                        {{-- @csrf --}}
                        <div class="form-group mb-2">
                            <input type="text" name="tahun" id="tahun" placeholder="tahun" style="width:20%;display:inline" value="{{old('tahun')}}" class="form-control" />
                            <button class="btn mx-sm-3 btn-primary mb-2" onclick="carii()">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                        
                        
                    {{-- </form> --}}
                </div>
            @endif -->
            <div class="card-header">
              <b>Data Capaian</b>
            </div>
            <div class="card-body"> 
                <div style="width:100%;text-align:center;text-transform:uppercase">
                    <b>LAPORAN PENCAPAIAN <br>{{$nmunitnya}}<br> {{$tahun}}</b>
                </div><hr>
                <div class="table-responsive">
                    <div class="table-responsive" style="width:100%;overflow-x:scroll;min-height:300px">
                    <table border="1" width="140%">
                        <thead>
                            <tr> 
                                <th class="isidatath" rowspan="2" width="4%">Kode KPI</th>
                                <th class="isidatath" rowspan="2">Nama KPI</th>
                                <th class="isidatath" rowspan="2" width="5%">Bobot</th>
                                <th class="isidatath" rowspan="2" width="5%">Target</th>
                                <th class="isidatath" rowspan="2" width="5%">Satuan</th>
                                <th class="isidatath" rowspan="2" width="5%">Ket</th>
                                <th class="isidatath" colspan="12">Bulan</th>
                                <th class="isidatath" rowspan="2" width="5%">Akumulasi</th>
                                <th class="isidatath" rowspan="2" width="5%">Score</th>
                                
                            </tr>
                            <tr>
                                @for($i=1; $i<=12; $i++)
                                    <th class="isidatath" width="3%" style="text-align:center;">{{$i}}</th>
                                @endfor
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($deployments as $item)
                                <tr>
                                    <td  rowspan="3">
                                        {{$item->kode_kpi}}
                                    </td>
                                    <td rowspan="3">{{$item->kpi['kpi']}}</td>
                                    <td rowspan="3" class="isidata">{{$item->bobot_tahunan}}</td>
                                    <td rowspan="3" class="isidata">{{$item->target_tahunan}}</td>
                                    <td rowspan="3" class="isidata">{{$item->kpi['satuan']}}</td>
                                    <td class="isidata" style="text-align:left">Target</td>
                                    @for($i=1; $i<=12; $i++)
                                       
                                        <td align="center" style="font-size:12px"> 
                                                {{$isitarget->shift()['target']}}
                                        </td>
                                       
                                    @endfor
                                    <td class="isidata">{{$isitotaltarget->shift()['target']}}</td>
                                    <td class="isidata">{{$scoreisitotaltarget->shift()['score']}}</td>
                                </tr>
                                 <tr>
                                    <td class="isidata" style="text-align:left">Realisasi</td>
                                    @for($i=1; $i<=12; $i++)
                                       
                                        <td align="center" style="font-size:12px"> 
                                            
                                                {{$isirealisasi->shift()['realisasi']}}
                                            
                                        </td>
                                       
                                    @endfor
                                    <td class="isidata">{{$isitotalrealisasi->shift()['realisasi']}}</td>
                                    <td class="isidata">{{$scoreisitotalrealisasi->shift()['score']}}</td>
                                    
                                </tr>
                                 
                                <tr>
                                    <td class="isidata" style="text-align:left">Capaian</td>
                                    @for($i=1; $i<=12; $i++)
                                       
                                        <td align="center" style="font-size:12px"> 
                                            
                                                {{$capaian->shift()['capaian']}}%
                                            
                                        </td>
                                       
                                    @endfor
                                    <td class="isidata"></td>
                                    <td class="isidata"></td>
                                </tr>
                             @endforeach 
                                <tr>
                                    <td colspan="6" align="left"><b>Tanggal Validasi Pimpinan Unit</b></td> 
                                    @for($i=1; $i<=12; $i++)
                                        
                                            <td class="isidata" width="3%" style="text-align:center;">
                                                    {{Tglvalidasi($kode,$i,$tahunnya)}}
                                            </td>
                                        
                                    @endfor
                                    <td class="isidata"></td>
                                    <td class="isidata"></td>
                                </tr>
                                <tr>
                                    <td colspan="6" align="left"><b>Total Capaian</b></td> 
                                    @for($i=1; $i<=12; $i++)
                                        
                                            <td class="isidata" width="3%" style="text-align:center;">
                                                {{$nilaicapaian->shift()['capaian']}}
                                            </td>
                                        
                                    @endfor
                                    <td class="isidata"></td>
                                    <td class="isidata"></td>
                                </tr>
                                <tr>
                                    <td colspan="6" align="left"><b>Potongan Keterlambatan</b></td> 
                                    @for($i=1; $i<=12; $i++)
                                        
                                            <td class="isidata" width="3%" style="text-align:center;">
                                                {{$potongan->shift()['potongan']}}
                                                    
                                            </td>
                                       
                                    @endfor
                                    <td class="isidata"></td>
                                    <td class="isidata"></td>
                                </tr>
                                
                                <tr>
                                    <td colspan="6" align="left"><b>Capaian Akhir</b></td> 
                                    @for($i=1; $i<=12; $i++)
                                       
                                            <td class="isidata" width="3%" style="text-align:center;">
                                                
                                                   {{$finis->shift()['finis']}} 
                                            </td>
                                       
                                    @endfor
                                    <td class="isidata"></td>
                                    <td class="isidata"></td>
                                </tr> 
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- modal --}}
<div id="modalresponse" style="margin-top:2rem">
</div>
{{--  end modal --}}

@endsection
<style>
    table{font-size:12px;}
    .isidata{background: #fff;font-weight:bold;text-align: center;}
    .isidatath{background: aqua;font-weight:bold;text-align: center;}
</style>
<script>
    function cari(){
        var unit=$('#unit').val();
        var tahun=$('#tahun').val();
        var un = unit.split("-");
        location.assign("{{url('capaian/deployment/laporancapaianadmin')}}/"+un[0]+"/"+un[1]+"/"+tahun);
    }
    function carii(){
        var tahun=$('#tahun').val();
        location.assign("{{url('capaian/deployment/laporancapaianadmin')}}/all/all/"+tahun);
    }
</script>