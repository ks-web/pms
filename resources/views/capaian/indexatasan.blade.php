@extends('core-ui.layouts.app')

@push('style')
<link href="{{ asset('vendors/DataTables/datatables.min.css') }}" rel="stylesheet">
@endpush

@push('script')
<script src="{{ asset('vendors/DataTables/datatables.min.js') }}"></script> 
<script>
    $(document).ready(function() {
        $('#table').DataTable({
            responsive: true,
        });

        $('tbody').on('click','.modal-valid',function(e){
            var token = $("meta[name='csrf-token']").attr("content");
            var url = $(event.target).data('url');
            $.ajax({ 
                url: url,
                type: "GET",
                data: {"_token": token, },
                success: function (data, textStatus, jqXHR) {
                    $("#modalresponse").html(data.html);
                    $('#exampleModal').modal('show');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert("AJAX error: " + textStatus + ' : ' + errorThrown);
                },
            });
        });

        $('tbody').on('click','.show-detail',function(e){
            var token = $("meta[name='csrf-token']").attr("content");
            var url = $(event.target).data('url');
            $.ajax({ 
                url: url,
                type: "GET",
                data: {"_token": token, },
                success: function (data, textStatus, jqXHR) {
                    $("#modalresponse").html(data.html);
                    $('#exampleModal').modal('show');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert("AJAX error: " + textStatus + ' : ' + errorThrown);
                },
            });
        });
    });
</script>
@endpush

<?php
    function bulan($x) {
        $bulan = array (1=>'Januari',2=>'Februari',3=>'Maret',4=>'April',
                5=>'Mei',6=>'Juni',7=>'Juli',8=>'Agustus',
                9=>'September',10=>'Oktober',11=>'November',12=>'Desember');
        return $bulan[$x];
    }

?>

@include('core-ui.layouts._layout')

@section('content')
<div class="row justify-content-center">
    <div class="col m-3">
        @if (session('success'))
        <div class="alert alert-success" role="alert">
            {{ session('success') }}
            </div>
        @endif
        <div class="card" style="width: 100%;">
           
                <div class="card-header">              
                       
                        <form class="form-inline" method="get" action="{{ url('/capaian/deployment/atasan') }}">
                            @csrf
                            <div class="form-group mb-2">
                                <input type="text" name="tahun" id="tahun" placeholder="tahun" value="{{old('tahun')}}" class="form-control" />
                            </div>
                            <div class="form-group mb-2">
                                <select id="unit" name="unit" class="form-control" placeholder="nama unit">
                                    <option value="">Pilih nama unit</option>
                                    @foreach ($units as $unit)
                                        <option value="{{ $unit->kode }}">{{ ucwords(strtolower($unit->nama)) }}</option>
                                    @endforeach
                                    @role('administrator')
                                        <option value="all">All Data</option>
                                    @endrole
                                </select>
                            </div>
                            <button class="btn mx-sm-3 btn-primary mb-2">
                                <i class="fa fa-search"></i>
                            </button>
                        </form>
                </div>
            
            <div class="card-header">
              <b>Data Capaian</b>
            </div>
            <div class="card-body"> 
                <div style="width:100%;text-align:center;text-transform:uppercase">
                    <b>LAPORAN PENCAPAIAN <br>{{$nmunitnya}}<br> {{date('Y')}}</b>
                </div><hr>
                <div class="table-responsive">
                    <div class="table-responsive" style="width:100%;overflow-x:scroll;min-height:300px;padding-bottom:50px">
                    <table border="1" width="120%">
                        <thead>
                            <tr> 
                                <th class="isidata" rowspan="2" width="4%">Kode Kpi</th>
                                <th class="isidata" rowspan="2">Nama KPI</th>
                                <th class="isidata" rowspan="2" width="3%"></th>
                                <th class="isidata" rowspan="2" width="5%">Bobot</th>
                                <th class="isidata" rowspan="2" width="5%">Target</th>
                                <th class="isidata" rowspan="2" width="5%">Satuan</th>
                                <th class="isidata" rowspan="2" width="5%">Ket</th>
                                <th class="isidata" colspan="12">Bulan</th>
                                
                            </tr>
                            <tr>
                                @for($i=1; $i<=12; $i++)
                                    <th class="isidata" width="3%" style="text-align:center;">{{$i}}</th>
                                @endfor
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($deployments as $item)
                                <tr>
                                    <td rowspan="3" >
                                        {{$item->kode_kpi}}
                                    </td>
                                    <td rowspan="3">{{substr($item->kpi['kpi'],0,50)}}</td>
                                    <td class="isidata" rowspan="3"><a href="{{ route('deployment.target.validasitarget', $item->id) }}">
                                            <button class="btn btn-primary btn-sm ml-1 modal-valid"  title="Validasi">
                                                <i class="fa fa-search" ></i>
                                            </button>
                                        </a>
                                    </td>
                                    <td class="isidata" rowspan="3">{{$item->bobot_tahunan}}</td>
                                    <td class="isidata" rowspan="3">{{$item->target_tahunan}}</td>
                                    <td class="isidata" rowspan="3">{{$item->kpi['satuan']}}</td>
                                    <td class="isidata" >Target</td>
                                    @for($i=1; $i<=12; $i++)
                                       
                                            <td align="center" style="font-size:12px"> 
                                                {{$isitarget->shift()['target']}}
                                            </td>
                                       
                                    @endfor
                                </tr>
                                <tr>
                                    
                                    <td class="isidata">Realisasi</td>
                                    @for($i=1; $i<=12; $i++)
                                        
                                        <td align="center" style="font-size:12px"> 
                                            
                                                {{$isirealisasi->shift()['realisasi']}}
                                            
                                        </td>
                                        
                                    @endfor
                                </tr>
                                    
                                <tr>
                                    <td class="isidata">Capaian</td>
                                    @for($i=1; $i<=12; $i++)
                                        
                                        <td align="center" style="font-size:12px"> 
                                            
                                            {{$capaian->shift()['capaian']}}%
                                            
                                        </td>
                                        
                                    @endfor
                                </tr>
                             @endforeach 
                                <tr>
                                    <td colspan="7" align="right"><b>Validasi</b></td> 
                                    @for($i=1; $i<=12; $i++)
                                        
                                            <td class="isidata" width="3%" style="text-align:center;">
                                                    {!! $validasinya->shift()['validasi']!!}
                                                    
                                            </td>
                                       
                                    @endfor
                                </tr>
                                
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- modal --}}
<div id="modalresponse" style="margin-top:2rem">
</div>
{{--  end modal --}}

@endsection
<style>
    table{font-size:12px;}
    .isidata{background: aqua;font-weight:bold;text-align: center;}
</style>
<script>
    function validasiyah(a,b,c){
        $.ajax({
            type: 'GET',
            url: "{{url('realisasi/validasiperbulan/')}}",
            data: 'bulan='+a+'&kode='+b+'&tahun='+c,
            success: function(msg){
                alert('Sukses Tervalidasi');
                document.location.reload(); 
                //alert(msg)
            }
        });
        //alert(a+'='+b+'='+c);
    }
</script>