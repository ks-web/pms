@role('keyperson')
    <a href="{{ route('deployment.target.create', $item->id) }}" data-toggle="tooltip" data-placement="bottom" title="Input Realisasi">
        <button class="btn btn-outline-primary btn-sm ml-1">
            <i class="fa fa-plus"></i>
        </button>
    </a>

@endrole

@role('boss')
    <a href="{{ route('deployment.target.validasitarget', $item->id) }}">
        <button class="btn btn-primary btn-sm ml-1 modal-valid"  title="Validasi">
            <i class="fa fa-share-square" ></i>
        </button>
    </a>
@endrole

@role('administrator')
    <a href="{{ route('deployment.target.validasitarget', $item->id) }}">
        <button class="btn btn-outline-primary btn-sm ml-1 modal-valid"  title="Validasi">
            <i class="fa fa-edit" ></i>
        </button>
    </a>
@endrole
{{-- all role --}}
{{-- <a data-url="{{ route('deployment.target.show', $item->id) }}">
    <button class="btn btn-success btn-sm ml-1 show-detail" data-url="{{ route('deployment.realisasi.show', $item->id) }}" data-toggle="tooltip" data-placement="bottom" title="Detail data">
        <i class="fa fa-search-plus" data-url="{{ route('deployment.realisasi.show', $item->id) }}"></i>
    </button>
</a> --}}