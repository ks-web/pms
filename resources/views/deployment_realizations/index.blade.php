@extends('core-ui.layouts.app')

@push('style')
<link href="{{ asset('vendors/DataTables/datatables.min.css') }}" rel="stylesheet">
@endpush

@push('script')
<script src="{{ asset('vendors/DataTables/datatables.min.js') }}"></script> 
<script>
    $(document).ready(function() {
        $('#table').DataTable({
            responsive: true,
        });

        $('tbody').on('click','.modal-valid',function(e){
            var token = $("meta[name='csrf-token']").attr("content");
            var url = $(event.target).data('url');
            $.ajax({ 
                url: url,
                type: "GET",
                data: {"_token": token, },
                success: function (data, textStatus, jqXHR) {
                    $("#modalresponse").html(data.html);
                    $('#exampleModal').modal('show');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert("AJAX error: " + textStatus + ' : ' + errorThrown);
                },
            });
        });

        $('tbody').on('click','.show-detail',function(e){
            var token = $("meta[name='csrf-token']").attr("content");
            var url = $(event.target).data('url');
            $.ajax({ 
                url: url,
                type: "GET",
                data: {"_token": token, },
                success: function (data, textStatus, jqXHR) {
                    $("#modalresponse").html(data.html);
                    $('#exampleModal').modal('show');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert("AJAX error: " + textStatus + ' : ' + errorThrown);
                },
            });
        });
    });
</script>
@endpush

<?php
    function bulan($x) {
        $bulan = array (1=>'Januari',2=>'Februari',3=>'Maret',4=>'April',
                5=>'Mei',6=>'Juni',7=>'Juli',8=>'Agustus',
                9=>'September',10=>'Oktober',11=>'November',12=>'Desember');
        return $bulan[$x];
    }

?>

@include('core-ui.layouts._layout')

@section('content')
<div class="row justify-content-center">
    <div class="col m-3">
        @if (session('success'))
        <div class="alert alert-success" role="alert">
            {{ session('success') }}
            </div>
        @endif
        <div class="card" style="width: 100%;">
            
                <div class="card-header">
              
                    <form class="form-inline" method="get" action="{{ route('deployment.realisasi.index') }}">
                        @csrf
                        <div class="form-group mb-2">
                            <input type="text" name="tahun" id="tahun" placeholder="tahun" value="{{old('tahun')}}" class="form-control" />
                        </div>
                        <div class="form-group mb-2">
                            <select id="unit" name="unit" class="form-control" placeholder="nama unit">
                                <option value="">Pilih nama unit</option>
                                @foreach ($units as $unit)
                                    <option value="{{ $unit->kode }}">{{ ucwords(strtolower($unit->nama)) }}</option>
                                @endforeach
                                @role('administrator')
                                    <option value="all">All Data</option>
                                @endrole
                            </select>
                        </div>
                        <button class="btn mx-sm-3 btn-primary mb-2">
                            <i class="fa fa-search"></i>
                        </button>
                    </form>
                </div>
           
            <div class="card-header" style="text-transform:uppercase">
                <b>Data Realisasi "{{$namaunit}}"</b>
            </div>
            <div class="card-body">
                <div class="table-responsive" style="width:100%;overflow-x:scroll;min-height:300px">
                    <table  width="120%">
                        <thead>
                            <tr> 
                                
                                @role('administrator')
                                <th width="20%">Unit Kerja</th>
                                @endrole
                                <th >Nama Kpi</th>
                                <th width="6%">Tahun</th>
                                @for($i=1; $i<=12; $i++)
                                    <th width="4%">{{substr(bulan($i),0,3)}}</th>
                                @endfor
                                <th width="4%">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($deployments as $item)
                                <tr>
                                    
                                    @role('administrator')
                                    <td>
                                        [<b>{{$item->kode_unit }}</b>] <br>{{$item->unit['nama']}} 
                                    </td>
                                    @endrole
                                    <td>
                                        <b>[{{$item->kode_kpi}}]</b> {{$item->kpi['kpi']}}
                                    </td>
                                    <td>{{$item->tahun}}</td>
                                        @foreach ($item->deploymentTargets as $targetItem)
                                            <td  style="font-size:12px;background:#e2f599"> 
                                                @role('boss')
                                                    @if($targetItem->status_id == 3)                                        
                                                        <span class="badge badge-danger modal-valid" data-toggle="tooltip" data-placement="bottom" title="Menunggu validasi" data-url="{{ route('deployment.realisasi.formvalidasi', $targetItem->id_deployment) }}">{{ $targetItem->realisasi }}</span>
                                                    @else
                                                        {{ $targetItem->realisasi }}
                                                    @endif
                                                @endrole
                                                @role('administrator')
                                                    @if($targetItem->status_id == 2)                                        
                                                        <span class="badge badge-danger modal-valid" data-toggle="tooltip" data-placement="bottom" title="Menunggu validasi" data-url="{{ route('deployment.realisasi.formvalidasi', $targetItem->id_deployment) }}">{{ $targetItem->realisasi }}</span>
                                                    @else
                                                        {{ $targetItem->realisasi }}
                                                    @endif
                                                @endrole
                                                @role('keyperson')
                                                    {{ $targetItem->realisasi }}
                                                @endrole
                                               
                                            </td>
                                        @endforeach
                                        <td > 
                                            @include('deployment_realizations._action')
                                        </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- modal --}}
<div id="modalresponse" style="margin-top:2rem">
</div>
{{--  end modal --}}
<style>
    th{padding:5px;border:solid 1px #a5a5c7;background: #a5a5c7;font-size:12px;}
    td{padding:5px;border:solid 1px #a5a5c7;font-size:12px;}
</style>
@endsection
